/**********************************************************************
*	Author  : RI - CTS363601 
*	Purpose : Module to refer all class definitions
***********************************************************************/

/** Class definition - User/Employee **/
function Employee(){
  	this.empId						= 0;
  	this.employeeCode				= "0";
	this.firstName					= "--NA--";
	this.lastName					= "--NA--";
	this.email						= "";
  	this.countryCode				= "UK";
  	this.businessCode				= "R";
  	this.branchNumber				= "";  	
  
  	this.setEmployeeCode			= function(idVal){
      	this.employeeCode			= idVal;
    };
  
  	this.setEmployeeId				= function(idVal){
      	this.empId					= idVal;
    };
  
  	this.getEmployeeName			= function(){
      	/*var empName					= "";
      	empName						+= (!isEmpty(this.firstName)) ? this.firstName : "";
      	empName						+= " ";
      	empName						+= (!isEmpty(this.lastName)) ? this.lastName : "";
      	return empName;*/
      	return this.email;
    };
  	
}

/** Class definition - Attendance **/
function Attendance(){
	this.startDateStr				= "";
	this.startTimestamp				= 0;
  	this.startLocLatitude			= null;
  	this.startLocLongitude			= null;
  	
  	this.endDateStr					= "null";
	this.endTimestamp				= 0;
  	this.endLocLatitude				= null;
  	this.endLocLongitude			= null;
  	
  	this.isOverTime					= false;
  	this.startLocComment			= "";
  	this.endLocComment				= "";
  
  	this.setOverTimeFlag			= function(flagVal){
      	this.isOverTime				= flagVal;
    };
}

/** Class definition - Trace **/
function Trace(){
  	this.traceId					= 0;
  	this.employeeCode				= "";
  	this.email						= "";
    this.traceTimestamp				= 0;
    this.traceTimestampStr			= 0;
    this.traceLocLatitude			= null;
    this.traceLocLongitude			= null;  
    
    this.traceIsOpen				= true;
  	this.traceComment				= "";
    
    this.setTraceIsOpenFlag			= function(flagVal){
      	this.traceIsOpen			= flagVal;
    };
  	
  	this.setTraceId					= function(idVal){
      	this.attendanceId			= idVal;
    };
    	
  	this.setEmployeeCode			= function(idVal){
      	this.employeeCode			= idVal;
    };
}