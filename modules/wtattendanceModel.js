//****************Sync Version:MobileFabricInstaller-QA-7.2.0_v201610191639_r117*******************
// ****************Generated On Fri Nov 25 12:45:01 UTC 2016wtattendance*******************
// **********************************Start wtattendance's helper methods************************
if (typeof(kony) === "undefined") {
	kony = {};
}

if (typeof(kony.sync) === "undefined") {
	kony.sync = {};
}

if (typeof(kony.sync.log) === "undefined") {
	kony.sync.log = {};
}

if (typeof(sync) === "undefined") {
	sync = {};
}

if (typeof(sync.log) === "undefined") {
	sync.log = {};
}



if(typeof(WTSync)=== "undefined"){ WTSync = {}; }

/************************************************************************************
* Creates new wtattendance
*************************************************************************************/
WTSync.wtattendance = function(){
	this.attendanceId = null;
	this.employeeCode = null;
	this.email = null;
	this.startDateStr = null;
	this.startTimestamp = null;
	this.startLocLatitude = null;
	this.startLocLongitude = null;
	this.endDateStr = null;
	this.endTimestamp = null;
	this.endLocLatitude = null;
	this.endLocLongitude = null;
	this.isOverTime = null;
	this.startLocComment = null;
	this.endLocComment = null;
	this.isDeleted = null;
	this.lastModifiedDate = null;
	this.startTimestampStr = null;
	this.endTimestampStr = null;
	this.branchNumber = null;
	this.markForUpload = true;
};

WTSync.wtattendance.prototype = {
	get attendanceId(){
		return this._attendanceId;
	},
	set attendanceId(val){
		if(!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidNumberType(val)){
			sync.log.error("Invalid data type for the attribute attendanceId in wtattendance.\nExpected:\"integer\"\nActual:\"" + kony.type(val) + "\"");
		}
		this._attendanceId = val;
	},
	get employeeCode(){
		return this._employeeCode;
	},
	set employeeCode(val){
		this._employeeCode = val;
	},
	get email(){
		return this._email;
	},
	set email(val){
		this._email = val;
	},
	get startDateStr(){
		return this._startDateStr;
	},
	set startDateStr(val){
		this._startDateStr = val;
	},
	get startTimestamp(){
		return this._startTimestamp;
	},
	set startTimestamp(val){
		this._startTimestamp = val;
	},
	get startLocLatitude(){
		return this._startLocLatitude;
	},
	set startLocLatitude(val){
		if(!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidNumberType(val)){
			sync.log.error("Invalid data type for the attribute startLocLatitude in wtattendance.\nExpected:\"double\"\nActual:\"" + kony.type(val) + "\"");
		}
		this._startLocLatitude = val;
	},
	get startLocLongitude(){
		return this._startLocLongitude;
	},
	set startLocLongitude(val){
		if(!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidNumberType(val)){
			sync.log.error("Invalid data type for the attribute startLocLongitude in wtattendance.\nExpected:\"double\"\nActual:\"" + kony.type(val) + "\"");
		}
		this._startLocLongitude = val;
	},
	get endDateStr(){
		return this._endDateStr;
	},
	set endDateStr(val){
		this._endDateStr = val;
	},
	get endTimestamp(){
		return this._endTimestamp;
	},
	set endTimestamp(val){
		this._endTimestamp = val;
	},
	get endLocLatitude(){
		return this._endLocLatitude;
	},
	set endLocLatitude(val){
		if(!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidNumberType(val)){
			sync.log.error("Invalid data type for the attribute endLocLatitude in wtattendance.\nExpected:\"double\"\nActual:\"" + kony.type(val) + "\"");
		}
		this._endLocLatitude = val;
	},
	get endLocLongitude(){
		return this._endLocLongitude;
	},
	set endLocLongitude(val){
		if(!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidNumberType(val)){
			sync.log.error("Invalid data type for the attribute endLocLongitude in wtattendance.\nExpected:\"double\"\nActual:\"" + kony.type(val) + "\"");
		}
		this._endLocLongitude = val;
	},
	get isOverTime(){
		return kony.sync.getBoolean(this._isOverTime)+"";
	},
	set isOverTime(val){
		if(!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidBooleanType(val)){
			sync.log.error("Invalid data type for the attribute isOverTime in wtattendance.\nExpected:\"boolean\"\nActual:\"" + kony.type(val) + "\"");
		}
		this._isOverTime = val;
	},
	get startLocComment(){
		return this._startLocComment;
	},
	set startLocComment(val){
		this._startLocComment = val;
	},
	get endLocComment(){
		return this._endLocComment;
	},
	set endLocComment(val){
		this._endLocComment = val;
	},
	get isDeleted(){
		return kony.sync.getBoolean(this._isDeleted)+"";
	},
	set isDeleted(val){
		if(!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidBooleanType(val)){
			sync.log.error("Invalid data type for the attribute isDeleted in wtattendance.\nExpected:\"boolean\"\nActual:\"" + kony.type(val) + "\"");
		}
		this._isDeleted = val;
	},
	get lastModifiedDate(){
		return this._lastModifiedDate;
	},
	set lastModifiedDate(val){
		this._lastModifiedDate = val;
	},
	get startTimestampStr(){
		return this._startTimestampStr;
	},
	set startTimestampStr(val){
		this._startTimestampStr = val;
	},
	get endTimestampStr(){
		return this._endTimestampStr;
	},
	set endTimestampStr(val){
		this._endTimestampStr = val;
	},
	get branchNumber(){
		return this._branchNumber;
	},
	set branchNumber(val){
		this._branchNumber = val;
	},
};

/************************************************************************************
* Retrieves all instances of wtattendance SyncObject present in local database with
* given limit and offset where limit indicates the number of records to be retrieved
* and offset indicates number of rows to be ignored before returning the records.
* e.g. var orderByMap = []
* orderByMap[0] = {};
* orderByMap[0].key = "attendanceId";
* orderByMap[0].sortType ="desc";
* orderByMap[1] = {};
* orderByMap[1].key = "employeeCode";
* orderByMap[1].sortType ="asc";
* var limit = 20;
* var offset = 5;
* WTSync.wtattendance.getAll(successcallback,errorcallback, orderByMap, limit, offset)
*************************************************************************************/
WTSync.wtattendance.getAll = function(successcallback, errorcallback, orderByMap, limit, offset){
	sync.log.trace("Entering WTSync.wtattendance.getAll->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = WTSync.wtattendance.getTableName();
	orderByMap = kony.sync.formOrderByClause("wtattendance",orderByMap);
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);	
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_orderBy(query, orderByMap);
				kony.sync.qb_limitOffset(query,limit,offset);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];

	function mySuccCallback(res){
		sync.log.trace("Entering WTSync.wtattendance.getAll->successcallback");
		successcallback(WTSync.wtattendance.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}	
	kony.sync.single_select_execute(dbname,sql, params,mySuccCallback,errorcallback);
};

/************************************************************************************
* Returns number of wtattendance present in local database.
*************************************************************************************/
WTSync.wtattendance.getAllCount = function(successcallback,errorcallback){
	sync.log.trace("Entering WTSync.wtattendance.getAllCount function");
	WTSync.wtattendance.getCount("",successcallback,errorcallback);
};

/************************************************************************************
* Returns number of wtattendance using where clause in the local Database
*************************************************************************************/
WTSync.wtattendance.getCount = function(wcs,successcallback,errorcallback){
	sync.log.trace("Entering WTSync.wtattendance.getCount->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "WTSync.wtattendance.getCount" , "getCount", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = WTSync.wtattendance.getTableName();
	wcs = kony.sync.validateWhereClause(wcs);
	var sql = "select count(*) from \"" + tbname + "\" " + wcs;
	kony.sync.single_execute_sql(dbname,sql, null, mySuccCallback, errorcallback);
	function mySuccCallback(res) {
		sync.log.trace("Entering WTSync.wtattendance.getCount->successcallback");
		if(null!==res){
			var count = null;
			count = res["count(*)"];
			kony.sync.verifyAndCallClosure(successcallback, {count:count});
		}
		else{
			sync.log.error("Some error occured while getting the count");
		}
	}
};

/************************************************************************************
* Creates a new instance of wtattendance in the local Database. The new record will 
* be merged with the enterprise datasource in the next Sync.
*************************************************************************************/
WTSync.wtattendance.prototype.create = function(successcallback,errorcallback){
	sync.log.trace("Entering  WTSync.wtattendance.prototype.create function");
	var valuestable = this.getValuesTable(true);
	WTSync.wtattendance.create(valuestable, successcallback,errorcallback,this.markForUpload);
};
WTSync.wtattendance.create = function(valuestable, successcallback,errorcallback,markForUpload){
	sync.log.trace("Entering  WTSync.wtattendance.create->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	
	if(!kony.sync.validateInput(arguments, "WTSync.wtattendance.create" , "create", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = WTSync.wtattendance.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);

	if(kony.sync.attributeValidation(valuestable,"wtattendance",errorcallback,true)===false){
		return;
	}
	
	function executeSuccess(){
		sync.log.trace("Entering  WTSync.wtattendance.create->success callback");
		kony.sync.single_insert_execute(dbname,tbname,valuestable,successcallback,errorcallback,markForUpload);
	}

	if(kony.sync.enableORMValidations){
		var relationshipMap={};  
		relationshipMap = WTSync.wtattendance.getRelationshipMap(relationshipMap,valuestable);
		kony.sync.checkIntegrity(dbname,relationshipMap,executeSuccess,errorcallback);
	}
	else{
		kony.sync.single_insert_execute(dbname,tbname,valuestable,successcallback,errorcallback,markForUpload);
	}
};

/************************************************************************************
* Creates number of new instances of wtattendance in the local Database. These new 
* records will be merged with the enterprise datasource in the next Sync. Based upon 
* kony.sync.enableORMValidations flag, validations will be enabled/disabled.
* e.g.	var valuesArray = [];
*		valuesArray[0] = {};
*		valuesArray[0].employeeCode = "employeeCode_0";
*		valuesArray[0].email = "email_0";
*		valuesArray[0].startDateStr = "startDateStr_0";
*		valuesArray[0].startTimestamp = "startTimestamp_0";
*		valuesArray[0].startLocLatitude = 0;
*		valuesArray[0].startLocLongitude = 0;
*		valuesArray[0].endDateStr = "endDateStr_0";
*		valuesArray[0].endTimestamp = "endTimestamp_0";
*		valuesArray[0].endLocLatitude = 0;
*		valuesArray[0].endLocLongitude = 0;
*		valuesArray[0].isOverTime = true;
*		valuesArray[0].startLocComment = "startLocComment_0";
*		valuesArray[0].endLocComment = "endLocComment_0";
*		valuesArray[0].startTimestampStr = "startTimestampStr_0";
*		valuesArray[0].endTimestampStr = "endTimestampStr_0";
*		valuesArray[0].branchNumber = "branchNumber_0";
*		valuesArray[1] = {};
*		valuesArray[1].employeeCode = "employeeCode_1";
*		valuesArray[1].email = "email_1";
*		valuesArray[1].startDateStr = "startDateStr_1";
*		valuesArray[1].startTimestamp = "startTimestamp_1";
*		valuesArray[1].startLocLatitude = 1;
*		valuesArray[1].startLocLongitude = 1;
*		valuesArray[1].endDateStr = "endDateStr_1";
*		valuesArray[1].endTimestamp = "endTimestamp_1";
*		valuesArray[1].endLocLatitude = 1;
*		valuesArray[1].endLocLongitude = 1;
*		valuesArray[1].isOverTime = true;
*		valuesArray[1].startLocComment = "startLocComment_1";
*		valuesArray[1].endLocComment = "endLocComment_1";
*		valuesArray[1].startTimestampStr = "startTimestampStr_1";
*		valuesArray[1].endTimestampStr = "endTimestampStr_1";
*		valuesArray[1].branchNumber = "branchNumber_1";
*		valuesArray[2] = {};
*		valuesArray[2].employeeCode = "employeeCode_2";
*		valuesArray[2].email = "email_2";
*		valuesArray[2].startDateStr = "startDateStr_2";
*		valuesArray[2].startTimestamp = "startTimestamp_2";
*		valuesArray[2].startLocLatitude = 2;
*		valuesArray[2].startLocLongitude = 2;
*		valuesArray[2].endDateStr = "endDateStr_2";
*		valuesArray[2].endTimestamp = "endTimestamp_2";
*		valuesArray[2].endLocLatitude = 2;
*		valuesArray[2].endLocLongitude = 2;
*		valuesArray[2].isOverTime = true;
*		valuesArray[2].startLocComment = "startLocComment_2";
*		valuesArray[2].endLocComment = "endLocComment_2";
*		valuesArray[2].startTimestampStr = "startTimestampStr_2";
*		valuesArray[2].endTimestampStr = "endTimestampStr_2";
*		valuesArray[2].branchNumber = "branchNumber_2";
*		WTSync.wtattendance.createAll(valuesArray, successcallback, errorcallback, true);
*************************************************************************************/
WTSync.wtattendance.createAll = function(valuesArray, successcallback, errorcallback, markForUpload){
	sync.log.trace("Entering WTSync.wtattendance.createAll function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "WTSync.wtattendance.createAll" , "createAll", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = WTSync.wtattendance.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	var isProperData = true;
	var arrayLen = 0;
	var errorInfo = [];
	var arrayLength = valuesArray.length;
	var errObject = null;
	var isReferentialIntegrityFailure = false;
	var errMsg = null;
	if(kony.sync.enableORMValidations){
		var newValuesArray = [];

		//column level validations
		for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			var valuestable = valuesArray[i];
			if(kony.sync.attributeValidation(valuestable,"wtattendance",errorcallback,true)===false){
				return;
			}

			newValuesArray[i] = valuestable;
		}
		valuesArray = newValuesArray;
		var connection = kony.sync.getConnectionOnly(dbname, dbname);
		kony.sync.startTransaction(connection, checkIntegrity, transactionSuccessCallback, transactionErrorCallback);
		var isError = false;
	}
	else{
		//copying by value
		var newValuesArray = [];
		arrayLength = valuesArray.length;
		for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			newValuesArray[i] = kony.sync.CreateCopy(valuesArray[i]);
		}
		valuesArray = newValuesArray;
		kony.sync.massInsert(dbname, tbname, valuesArray, successcallback, errorcallback, markForUpload);
	}

	function transactionErrorCallback(){
		if(isError==true){
			//Statement error has occurred
				kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
		}
		else{
			//Transaction error has occurred
				kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeTransaction, kony.sync.getErrorMessage(kony.sync.errorCodeTransaction), null));
		}
	}

	function transactionSuccessCallback(){
		sync.log.trace("Entering  WTSync.wtattendance.createAll->transactionSuccessCallback");
		if(!isError){
			kony.sync.massInsert(dbname, tbname, valuesArray, successcallback, errorcallback, markForUpload);
		}
		else{
			if(isReferentialIntegrityFailure){
				kony.sync.verifyAndCallClosure(errorcallback, errObject);
			}
		}
	}
	
	//foreign key constraints validations
	function checkIntegrity(tx){
		sync.log.trace("Entering  WTSync.wtattendance.createAll->checkIntegrity");
		arrayLength = valuesArray.length;
		for (var i=0; valuesArray != null && i < arrayLength; i++ ){
			var relationshipMap={};  
			relationshipMap = WTSync.wtattendance.getRelationshipMap(relationshipMap,valuesArray[i]);
			errObject = kony.sync.checkIntegrityinTransaction(tx, relationshipMap, null);
			if(errObject===false){
				isError = true;
				return; 
			}
			if(errObject!==true){
				isError = true;
				isReferentialIntegrityFailure = true;
				return;
			}
		}
	}
};
/************************************************************************************
* Updates wtattendance using primary key in the local Database. The update will be
* merged with the enterprise datasource in the next Sync.
*************************************************************************************/
WTSync.wtattendance.prototype.updateByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering  WTSync.wtattendance.prototype.updateByPK function");
	var pks = this.getPKTable();
	var valuestable = this.getValuesTable(false);
	WTSync.wtattendance.updateByPK(pks,valuestable, successcallback,errorcallback,this.markForUpload);
};
WTSync.wtattendance.updateByPK = function(pks,valuestable, successcallback,errorcallback, markForUpload){
	sync.log.trace("Entering  WTSync.wtattendance.updateByPK-> main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "WTSync.wtattendance.updateByPK",  "updateByPk", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = WTSync.wtattendance.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	var wcs = [ ];

	if(WTSync.wtattendance.pkCheck(pks,wcs,errorcallback,"updating")===false){
		return;
	}

	if(kony.sync.attributeValidation(valuestable,"wtattendance",errorcallback,false)===false){
		return;
	}

	var relationshipMap={};  
	relationshipMap = WTSync.wtattendance.getRelationshipMap(relationshipMap,valuestable);

	kony.sync.updateByPK(tbname, dbname, relationshipMap, pks,valuestable, successcallback,errorcallback, markForUpload, wcs);
};

/************************************************************************************
* Updates wtattendance(s) using where clause in the local Database. The update(s)
* will be merged with the enterprise datasource in the next Sync.
*************************************************************************************/
WTSync.wtattendance.update = function(wcs, valuestable, successcallback,errorcallback,markForUpload){
	sync.log.trace("Entering WTSync.wtattendance.update function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "WTSync.wtattendance.update",  "update", errorcallback)){
		return;
	}

	var dbname = kony.sync.getDBName();
	var tbname = WTSync.wtattendance.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	wcs = kony.sync.validateWhereClause(wcs);

	if(kony.sync.attributeValidation(valuestable,"wtattendance",errorcallback,false)===false){
		return;
	}
	function executeSuccess(){
		sync.log.trace("Entering  WTSync.wtattendance.update-> success callback of Integrity Check");
		kony.sync.single_update_execute(dbname,tbname,valuestable,wcs,successcallback,errorcallback,true, markForUpload, WTSync.wtattendance.getPKTable());
	}

	if(kony.sync.enableORMValidations){
		var relationshipMap={};  
		relationshipMap = WTSync.wtattendance.getRelationshipMap(relationshipMap,valuestable);
		kony.sync.checkIntegrity(dbname,relationshipMap,executeSuccess,errorcallback);
	}
	else{
		kony.sync.single_update_execute(dbname,tbname,valuestable,wcs,successcallback,errorcallback,true, markForUpload, WTSync.wtattendance.getPKTable());
	}
};

/************************************************************************************
* Updates wtattendance(s) satisfying one or more where clauses in the local Database. 
* The update(s) will be merged with the enterprise datasource in the next Sync.
* Based upon kony.sync.enableORMValidations flag, validations will be enabled/disabled.
* e.g.	var inputArray = [];
*		inputArray[0] = {};
*		inputArray[0].changeSet = {};
*		inputArray[0].changeSet.employeeCode = "employeeCode_updated0";
*		inputArray[0].changeSet.email = "email_updated0";
*		inputArray[0].changeSet.startDateStr = "startDateStr_updated0";
*		inputArray[0].changeSet.startTimestamp = "startTimestamp_updated0";
*		inputArray[0].whereClause = "where attendanceId = 0";
*		inputArray[1] = {};
*		inputArray[1].changeSet = {};
*		inputArray[1].changeSet.employeeCode = "employeeCode_updated1";
*		inputArray[1].changeSet.email = "email_updated1";
*		inputArray[1].changeSet.startDateStr = "startDateStr_updated1";
*		inputArray[1].changeSet.startTimestamp = "startTimestamp_updated1";
*		inputArray[1].whereClause = "where attendanceId = 1";
*		inputArray[2] = {};
*		inputArray[2].changeSet = {};
*		inputArray[2].changeSet.employeeCode = "employeeCode_updated2";
*		inputArray[2].changeSet.email = "email_updated2";
*		inputArray[2].changeSet.startDateStr = "startDateStr_updated2";
*		inputArray[2].changeSet.startTimestamp = "startTimestamp_updated2";
*		inputArray[2].whereClause = "where attendanceId = 2";
*		WTSync.wtattendance.updateAll(inputArray,successcallback,errorcallback);
*************************************************************************************/
WTSync.wtattendance.updateAll = function(inputArray, successcallback, errorcallback, markForUpload) {
	sync.log.trace("Entering WTSync.wtattendance.updateAll function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "WTSync.wtattendance.updateAll",  "updateAll", errorcallback)){
		return;
	}
	var dbname = "100004898743b5a1e";
	var tbname = "wtattendance";
	var isError = false;
	var errObject = null;
	if(markForUpload == false || markForUpload == "false"){
		markForUpload="false"
	}
	else{
		markForUpload="true"
	}
	if((kony.sync.enableORMValidations)){

		var newInputArray = [];
		for (var i=0; ((inputArray) != null) && i < inputArray.length; i++ ){
			var v = inputArray[i];
			var valuestable = v.changeSet;
			var isEmpty = true;
			for(var key in valuestable){
				isEmpty = false;
				break;
			}
			if(isEmpty){
				errorcallback(kony.sync.getErrorTable(kony.sync.errorCodeNullValue,kony.sync.getErrorMessage(kony.sync.errorCodeNullValue)));
				return;
			}
			var wcs = v.whereClause;
			var twcs = wcs;
			if(kony.sync.attributeValidation(valuestable,"wtattendance",errorcallback,false)===false){
				return;
			}

			newInputArray[i] = [];
			newInputArray[i].changeSet = valuestable;
			newInputArray[i].whereClause = wcs;
		}
		inputArray = newInputArray;
		var connection = kony.sync.getConnectionOnly(dbname, dbname);
		kony.sync.startTransaction(connection, checkIntegrity, transactionSuccessCallback, transactionErrorCallback);

	}
	else{
		//copying by value
		var newInputArray = [];
		for (var i=0; ((inputArray) != null) && i < inputArray.length; i++ ){
		    var v = inputArray[i];
		    newInputArray[i] = kony.sync.CreateCopy(v);
		}
		inputArray = newInputArray;
		kony.sync.massUpdate(dbname, tbname,inputArray,successcallback,errorcallback,markForUpload, WTSync.wtattendance.getPKTable());
	}
	
		function transactionSuccessCallback(){
		sync.log.trace("Entering  WTSync.wtattendance.updateAll->transactionSuccessCallback");
		if(!isError){
			kony.sync.massUpdate(dbname, tbname,inputArray,successcallback,transactionErrorCallback,markForUpload, WTSync.wtattendance.getPKTable());
		}
	}

	function transactionErrorCallback(){
		if(errObject===false){
			//Sql statement error has occcurred
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
			
		}
		else if(errObject!==null){
			// Referential integrity error has occurred
			kony.sync.verifyAndCallClosure(errorcallback, errObject);
		}
		else{
			//Transaction error has occurred
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeTransaction, kony.sync.getErrorMessage(kony.sync.errorCodeTransaction), null));
		}
	}
	//foreign key constraints validations
	function checkIntegrity(tx){
		sync.log.trace("Entering  WTSync.wtattendance.updateAll->checkIntegrity");
		for (var i=0; ((inputArray) != null) && i < inputArray.length; i++ ){
			var relationshipMap={}; 
			relationshipMap = WTSync.wtattendance.getRelationshipMap(relationshipMap,inputArray[i].changeSet);
			sync.log.debug("Relationship Map for Integrity check created:", relationshipMap);
			errObject = kony.sync.checkIntegrityinTransaction(tx, relationshipMap, null);
			if(errObject===false){
				isError = true;
				return; 
			}
			if(errObject!==true){
				isError = true;
				kony.sync.rollbackTransaction(tx);
				return;
			}
		}
	}


}
/************************************************************************************
* Deletes wtattendance using primary key from the local Database. The record will be
* deleted from the enterprise datasource in the next Sync.
*************************************************************************************/
WTSync.wtattendance.prototype.deleteByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering WTSync.wtattendance.prototype.deleteByPK function");
	var pks = this.getPKTable();
	WTSync.wtattendance.deleteByPK(pks,successcallback,errorcallback,this.markForUpload);
};
WTSync.wtattendance.deleteByPK = function(pks, successcallback,errorcallback, markForUpload){
	sync.log.trace("Entering WTSync.wtattendance.deleteByPK-> main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "WTSync.wtattendance.deleteByPK",  "deleteByPK", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = WTSync.wtattendance.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	var wcs = [];
	var isError = false;
	var pkNotFound = false;
	var twcs = [];
	var deletedRows;
	var record = "";
	if(WTSync.wtattendance.pkCheck(pks,wcs,errorcallback,"deleting")===false){
		return;
	}	
	twcs = kony.sync.CreateCopy(wcs);
	function wtattendanceTransactionCallback(tx){
		sync.log.trace("Entering WTSync.wtattendance.deleteByPK->wtattendance_PKPresent successcallback");
		record = kony.sync.getOriginalRow(tx, tbname, wcs, errorcallback);
		if(record===false){
			isError = true;
			return;
		}
		if (null !== record) {
		}else{
			pkNotFound = true;
		}
		var deletedRows = kony.sync.remove(tx, tbname, wcs, false, markForUpload, null);
			if(deletedRows === false){
				isError = true;
			}
	}
	
	function wtattendanceErrorCallback(){
		sync.log.error("Entering WTSync.wtattendance.deleteByPK->relationship failure callback");
		if(isError === false){
			kony.sync.verifyAndCallClosure(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}

	}
	function wtattendanceSuccessCallback(){
		sync.log.trace("Entering WTSync.wtattendance.deleteByPK->relationship success callback");
		if(pkNotFound === true){
			kony.sync.verifyAndCallClosure(pkNotFoundErrCallback);
			return;
		}
		
		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, {rowsdeleted:1});
		}
	}
	
	function pkNotFoundErrCallback(){
		sync.log.error("Entering WTSync.wtattendance.deleteByPK->PK not found callback");
		kony.sync.pkNotFoundErrCallback(errorcallback,tbname);
	}
		
		var dbconnection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
		if(dbconnection != null){
			kony.sync.startTransaction(dbconnection, wtattendanceTransactionCallback, wtattendanceSuccessCallback, wtattendanceErrorCallback, "Single Execute");
		}

};

/************************************************************************************
* Deletes wtattendance(s) using where clause from the local Database. The record(s)
* will be deleted from the enterprise datasource in the next Sync.
* e.g. WTSync.wtattendance.remove("where employeeCode like 'A%'", successcallback,errorcallback, true);
*************************************************************************************/
WTSync.wtattendance.remove = function(wcs, successcallback,errorcallback, markForUpload){
	sync.log.trace("Entering WTSync.wtattendance.remove->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "WTSync.wtattendance.remove",  "remove", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = WTSync.wtattendance.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	wcs = kony.sync.validateWhereClause(wcs);
	var twcs = wcs;
	var isError = false;
	var rowsDeleted;

	function wtattendance_removeTransactioncallback(tx){
		wcs = " " + wcs;
		rowsDeleted = kony.sync.deleteBatch(tx, tbname, wcs, false, markForUpload, errorcallback)
		if(rowsDeleted === false){
			isError = true;
		}
	}
	function wtattendance_removeSuccess(){
		sync.log.trace("Entering WTSync.wtattendance.remove->wtattendance_removeSuccess function");

		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, rowsDeleted);
		}
	}
	function errorcallbackWrapper(){
		sync.log.trace("Entering WTSync.wtattendance.remove->error callback function");
		if(!isError){
			kony.sync.showTransactionError(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	function deleteEntity(){
		sync.log.trace("Entering WTSync.wtattendance.remove->delete Entity function");
		var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
		if(connection != null){
			kony.sync.startTransaction(connection, wtattendance_removeTransactioncallback, wtattendance_removeSuccess, errorcallbackWrapper);
		}
	}
	deleteEntity();
};

/************************************************************************************
* Deletes wtattendance using primary key from the local Database. This will
* not have any effect in enterprise datasource in subsequent sync cycles
*************************************************************************************/
WTSync.wtattendance.prototype.removeDeviceInstanceByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering WTSync.wtattendance.prototype.removeDeviceInstanceByPK function");
	var pks = this.getPKTable();
	WTSync.wtattendance.removeDeviceInstanceByPK(pks,successcallback,errorcallback);
};
WTSync.wtattendance.removeDeviceInstanceByPK = function(pks, successcallback,errorcallback){
	sync.log.trace("Entering WTSync.wtattendance.removeDeviceInstanceByPK function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "WTSync.wtattendance.removeDeviceInstanceByPK",  "removeDeviceInstanceByPK", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = WTSync.wtattendance.getTableName();
	var wcs = [];
	var isError = false;
	var pkNotFound = false;
	var deletedRows;
	if(WTSync.wtattendance.pkCheck(pks,wcs,errorcallback,"deleting")===false){
		return;
	}
	
	function wtattendanceTransactionCallback(tx){
		sync.log.trace("Entering WTSync.wtattendance.removeDeviceInstanceByPK -> wtattendanceTransactionCallback");
		var record = kony.sync.getOriginalRow(tx, tbname, wcs, errorcallback);
		if(null !== record && false !=record) {
			deletedRows = kony.sync.remove(tx, tbname, wcs, true, null, null);
			if(deletedRows === false){
				isError = true;
			}
		}else{
			pkNotFound = true;
		}
	}
	
	function wtattendanceErrorCallback(){
		sync.log.error("Entering WTSync.wtattendance.removeDeviceInstanceByPK -> wtattendanceErrorCallback");
		if(isError === false){
			kony.sync.verifyAndCallClosure(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	
	function wtattendanceSuccessCallback(){
		sync.log.trace("Entering WTSync.wtattendance.removeDeviceInstanceByPK -> wtattendanceSuccessCallback");
		if(pkNotFound === true){
			kony.sync.verifyAndCallClosure(pkNotFoundErrCallback);
			return;
		}
		
		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, {rowsdeleted:1});
		}
	}
	
	function pkNotFoundErrCallback(){
		sync.log.error("Entering WTSync.wtattendance.removeDeviceInstanceByPK -> PK not found callback");
		kony.sync.pkNotFoundErrCallback(errorcallback,tbname);
	}
	
	var dbconnection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
	if(dbconnection != null){
		kony.sync.startTransaction(dbconnection, wtattendanceTransactionCallback, wtattendanceSuccessCallback, wtattendanceErrorCallback, "Single Execute");
	}

};

/************************************************************************************
* Deletes wtattendance(s) using where clause from the local Database. This will
* not have any effect in enterprise datasource in subsequent sync cycles
*************************************************************************************/
WTSync.wtattendance.removeDeviceInstance = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering WTSync.wtattendance.removeDeviceInstance->main function");
	var dbname = kony.sync.getDBName();
	var tbname = WTSync.wtattendance.getTableName();
	wcs = kony.sync.validateWhereClause(wcs);
	var twcs = wcs;
	var isError = false;
	var rowsDeleted;

	function wtattendance_removeTransactioncallback(tx){
		wcs = " " + wcs;
		rowsDeleted = kony.sync.deleteBatch(tx, tbname, wcs, true, null, errorcallback)
		if(rowsDeleted === false){
			isError = true;
		}
	}
	function wtattendance_removeSuccess(){
		sync.log.trace("Entering WTSync.wtattendance.remove->wtattendance_removeSuccess function");

		if(!isError){
			kony.sync.verifyAndCallClosure(successcallback, rowsDeleted);
		}
	}
	function errorcallbackWrapper(){
		sync.log.trace("Entering WTSync.wtattendance.remove->error callback function");
		if(!isError){
			kony.sync.showTransactionError(errorcallback);
		}
		if(kony.sync.errorObject != null){
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	function deleteEntity(){
		sync.log.trace("Entering WTSync.wtattendance.remove->delete Entity function");
		var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
		if(connection != null){
			kony.sync.startTransaction(connection, wtattendance_removeTransactioncallback, wtattendance_removeSuccess, errorcallbackWrapper);
		}
	}
	deleteEntity();
};

/************************************************************************************
* Retrieves wtattendance using primary key from the local Database. 
*************************************************************************************/
WTSync.wtattendance.prototype.getAllDetailsByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering WTSync.wtattendance.prototype.getAllDetailsByPK function");
	var pks = this.getPKTable();
	WTSync.wtattendance.getAllDetailsByPK(pks,successcallback,errorcallback);
};
WTSync.wtattendance.getAllDetailsByPK = function(pks, successcallback,errorcallback){
	sync.log.trace("Entering WTSync.wtattendance.getAllDetailsByPK-> main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "WTSync.wtattendance.getAllDetailsByPK",  "getAllDetailsByPK", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = WTSync.wtattendance.getTableName();
	var wcs = [];
	if(WTSync.wtattendance.pkCheck(pks,wcs,errorcallback,"searching")===false){
		return;
	}
	twcs = kony.sync.CreateCopy(wcs);
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_where(query, wcs);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];
	function mySuccCallback(res){
		sync.log.trace("Entering WTSync.wtattendance.getAllDetailsByPK-> success callback function");
		successcallback(WTSync.wtattendance.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}	
	kony.sync.single_select_execute(dbname, sql, params, mySuccCallback, errorcallback);
};






/************************************************************************************
* Retrieves wtattendance(s) using where clause from the local Database. 
* e.g. WTSync.wtattendance.find("where employeeCode like 'A%'", successcallback,errorcallback);
*************************************************************************************/
WTSync.wtattendance.find = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering WTSync.wtattendance.find function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "WTSync.wtattendance.find",  "find", errorcallback)){
		return;
	}
	//wcs will be a string formed by the user.
	var dbname = kony.sync.getDBName();
	var tbname = WTSync.wtattendance.getTableName();
	wcs = kony.sync.validateWhereClause(wcs);
	var sql = "select * from \"" + tbname + "\" " + wcs;
	function mySuccCallback(res){
		kony.sync.verifyAndCallClosure(successcallback, WTSync.wtattendance.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}
	kony.sync.single_select_execute(dbname, sql, null, mySuccCallback, errorcallback);
};

/************************************************************************************
* Marks instance of wtattendance with given primary key for upload. This will 
* enable deferred records to merge with the enterprise datasource in the next Sync.
*************************************************************************************/
WTSync.wtattendance.prototype.markForUploadbyPK = function(successcallback, errorcallback){
	sync.log.trace("Entering WTSync.wtattendance.prototype.markForUploadbyPK function");
	var pks = this.getPKTable();
	WTSync.wtattendance.markForUploadbyPK(pks, successcallback, errorcallback);
};
WTSync.wtattendance.markForUploadbyPK = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering WTSync.wtattendance.markForUploadbyPK function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "WTSync.wtattendance.markForUploadbyPK",  "markForUploadbyPK", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = WTSync.wtattendance.getTableName();
	var isError = false;
	var recordsFound = false;
	var recordsMarkedForUpload = 0;
	var wcs = [];
	if(WTSync.wtattendance.pkCheck(pks, wcs, errorcallback, "marking for upload by PK")===false){
		return;
	}

	function markRecordForUpload(tx, record){
		var versionMapMain = [];
		versionMapMain[kony.sync.mainTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		versionMapMain[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname);
					kony.sync.qb_set(query,versionMapMain);
					kony.sync.qb_where(query, wcs);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0];
		var params = query_compile[1];
		return kony.sync.executeSql(tx, sql, params);		
	}
	
	function markRecordForUploadHistory(tx, record){
		var versionMap = [];
		versionMap[kony.sync.historyTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		var twcs = [];
		twcs = wcs;
		kony.table.insert(twcs,{key : kony.sync.historyTableChangeTypeColumn, value : record[kony.sync.historyTableChangeTypeColumn], optype : "EQ",comptype : "AND"});
		versionMap[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname + kony.sync.historyTableName);
					kony.sync.qb_set(query,versionMap);
					kony.sync.qb_where(query, twcs);
		kony.table.remove(twcs);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0];
		var params = query_compile[1];
		return kony.sync.executeSql(tx, sql, params);
	}
	
	function single_transaction_callback (tx){
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_select(query, [kony.sync.historyTableChangeTypeColumn]);
					kony.sync.qb_from(query, tbname);
					kony.sync.qb_where(query, wcs);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0];
		var params = query_compile[1];
		var resultSet = kony.sync.executeSql(tx, sql, params);
		if(resultSet === false){
			isError = true;
			return;
		}

		var num_records = resultSet.rows.length;
		if(num_records > 0){
			recordsFound = true;
			var record = kony.db.sqlResultsetRowItem(tx, resultSet, 0);
			var changeType = record[kony.sync.mainTableChangeTypeColumn];
			if(!kony.sync.isNullOrUndefined(changeType) && kony.string.startsWith(""+changeType,"9")){
				recordsMarkedForUpload = 1;
				if(markRecordForUpload(tx, record) === false){
					isError = true;
					return;
				}
			}
		}
					
				
		var query1 =kony.sync.qb_createQuery();
					kony.sync.qb_select(query1, [kony.sync.historyTableChangeTypeColumn]);
					kony.sync.qb_from(query1, tbname + kony.sync.historyTableName);
					kony.sync.qb_where(query1, wcs);
		var query1_compile = kony.sync.qb_compile(query1);
		var sql1 = query1_compile[0];
		var params1 = query1_compile[1];
		var resultSet1 = kony.sync.executeSql (tx, sql1, params1);
		if(resultSet1!==false){
			var num_records = resultSet1.rows.length;
			for(var i = 0; i <= num_records - 1; i++ ){
				var record = kony.db.sqlResultsetRowItem(tx, resultSet1, i);
				if(markRecordForUploadHistory(tx, record) === false){
					isError = true;
					return;
				}
				recordsFound = true;
			}
		}
		else{
			isError = true;
		}
	}
	function single_transaction_success_callback(){
		if(recordsFound === true){
			kony.sync.verifyAndCallClosure(successcallback , {count:recordsMarkedForUpload});
		}
		else{
			kony.sync.pkNotFoundErrCallback(errorcallback, tbname);
		}
	}
	
	function single_transaction_error_callback(res){
		if (!isError) {
			kony.sync.showTransactionError(errorcallback);
		}else{
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	
	var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
	if(connection != null){
		kony.sync.startTransaction(connection, single_transaction_callback, single_transaction_success_callback, single_transaction_error_callback);
	}
};

/************************************************************************************
* Marks instance(s) of wtattendance matching given where clause for upload. This will 
* enable deferred records to merge with the enterprise datasource in the next Sync.
*************************************************************************************/
WTSync.wtattendance.markForUpload = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering WTSync.wtattendance.markForUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "WTSync.wtattendance.markForUpload",  "markForUpload", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = WTSync.wtattendance.getTableName();
	var isError = false;
	var num_records_main = 0;
	wcs = kony.sync.validateWhereClause(wcs);
	if(!kony.sync.isNull(wcs) && !kony.sync.isEmptyString(wcs)) {
		wcs = wcs + " and " + kony.sync.historyTableChangeTypeColumn + " like '9%'";
	}else{	
		wcs = "where " + kony.sync.historyTableChangeTypeColumn + " like '9%'";
	}
	
	function markRecordForUpload(tx, record){
		var versionMapMain = [];
		versionMapMain[kony.sync.mainTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		versionMapMain[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname);
					kony.sync.qb_set(query,versionMapMain);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0] + " " + wcs;
		var params = query_compile[1];
		if(kony.sync.executeSql(tx, sql, params) === false){
			return false;
		}
	}
	
	function markRecordForUploadHistory(tx, record){
		var versionMap = [];
		versionMap[kony.sync.historyTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
		var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
		var versionNo = kony.sync.getseqnumber(tx, scopename);
		if(versionNo === false){
			return false;
		}
		var twcs = "";
		twcs = wcs;
		twcs = twcs + " AND " + kony.sync.historyTableChangeTypeColumn + " = " + record[kony.sync.historyTableChangeTypeColumn];
		versionMap[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
		
		var query = kony.sync.qb_createQuery();
					kony.sync.qb_update(query, tbname + kony.sync.historyTableName);
					kony.sync.qb_set(query,versionMap);
		var query_compile = kony.sync.qb_compile(query);
		var sql = query_compile[0]  + " " + twcs;
		var params = query_compile[1];
		if(kony.sync.executeSql(tx, sql, params) === false){
			return false;
		}
	}
	
	function single_transaction_callback (tx){
		sync.log.trace("Entering WTSync.wtattendance.markForUpload->single_transaction_callback");
		//updating main table
		var sql = "select " + kony.sync.historyTableChangeTypeColumn + " from \"" + tbname + "\" " + wcs ;
		var resultSet = kony.sync.executeSql (tx, sql, null);
		if(resultSet === false){
			isError = true;
			return;
		}
		
		num_records_main = resultSet.rows.length;
		for(var i = 0; i < num_records_main; i++ ){
			var record = kony.db.sqlResultsetRowItem(tx, resultSet, i);
			if(markRecordForUpload(tx, record) === false){
				isError = true;
				return;
			}
		}
		
		//updating history table
		var sql = "select " + kony.sync.historyTableChangeTypeColumn + " from " + tbname + kony.sync.historyTableName + " " + wcs;
		var resultSet = kony.sync.executeSql (tx, sql, null);
		if(resultSet === false){
			isError = true;
			return;
		}

		var num_records = resultSet.rows.length;
		for ( var i = 0; i <= num_records - 1; i++ ){
			var record = kony.db.sqlResultsetRowItem(tx, resultSet, i);
			if(markRecordForUploadHistory(tx, record)=== false){
				isError = true;
				return;
			}
		}
	}
	
	function single_transaction_success_callback(){
		sync.log.trace("Entering WTSync.wtattendance.markForUpload->single_transaction_success_callback");
		kony.sync.verifyAndCallClosure(successcallback, {count:num_records_main});
	}
	
	function single_transaction_error_callback(){
		sync.log.error("Entering WTSync.wtattendance.markForUpload->single_transaction_error_callback");
		if(!isError) {
			kony.sync.showTransactionError(errorcallback);
		}else{
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
			kony.sync.errorObject = null;
		}
	}
	
	var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
	if(connection != null){
		kony.sync.startTransaction(connection, single_transaction_callback, single_transaction_success_callback, single_transaction_error_callback);
	}
};

/************************************************************************************
* Retrieves instance(s) of wtattendance pending for upload. Records are marked for
* pending upload if they have been updated or created locally and the changes have
* not been merged with enterprise datasource.
*************************************************************************************/
WTSync.wtattendance.getPendingUpload = function(wcs, successcallback,errorcallback){
	sync.log.trace("Entering WTSync.wtattendance.getPendingUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = WTSync.wtattendance.getTableName();
	var currentversion = kony.sync.getCurrentVersionNumber(tbname);
	var sql;
	if(typeof(wcs) === "string" && wcs != null){
		wcs = kony.sync.validateWhereClause(wcs);
		sql = "select * from \"" + tbname + "\" "+ wcs + " and " + kony.sync.mainTableChangeTypeColumn + " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableSyncVersionColumn+" = "+currentversion+" AND "+kony.sync.mainTableChangeTypeColumn+" NOT LIKE '9%'"; 
	}else{
		errorcallback = successcallback;
		successcallback = wcs;
		sql = "select * from \"" + tbname + "\" WHERE " + kony.sync.mainTableChangeTypeColumn + " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableSyncVersionColumn+" = "+currentversion+" AND "+kony.sync.mainTableChangeTypeColumn+" NOT LIKE '9%'"; 
	}
	kony.sync.single_select_execute(dbname, sql, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering WTSync.wtattendance.getPendingUpload->successcallback function");
		kony.sync.verifyAndCallClosure(successcallback, WTSync.wtattendance.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}
};

/************************************************************************************
* Retrieves instance(s) of wtattendance pending for acknowledgement. This is relevant
* when the SyncObject is part of the SyncScope whose SyncStrategy is PersistentSync.
* In persistent Sync the  records in the local database are put into a pending 
* acknowledgement state after an upload.
*************************************************************************************/
WTSync.wtattendance.getPendingAcknowledgement = function(successcallback, errorcallback){
	sync.log.trace("Entering WTSync.wtattendance.getPendingAcknowledgement->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = WTSync.wtattendance.getTableName();
	var currentversion = kony.sync.getCurrentVersionNumber(tbname);
	var mysql="select * from \""+tbname+"\" WHERE "+kony.sync.mainTableChangeTypeColumn+ " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableSyncVersionColumn+" <> "+currentversion+" AND "+kony.sync.mainTableChangeTypeColumn+" NOT LIKE '9%'"; 
	kony.sync.single_select_execute(dbname, mysql, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering WTSync.wtattendance.getPendingAcknowledgement success callback function");
		kony.sync.verifyAndCallClosure(successcallback, WTSync.wtattendance.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}
};

/************************************************************************************
* Retrieves instance(s) of wtattendance deferred for upload.
*************************************************************************************/
WTSync.wtattendance.getDeferredUpload = function(wcs,successcallback,errorcallback){
	sync.log.trace("Entering WTSync.wtattendance.getDeferredUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = WTSync.wtattendance.getTableName();
	var sql;
	if(typeof(wcs) === "string" && wcs != null ){
		wcs = kony.sync.validateWhereClause(wcs);
		sql = "select * from \"" + tbname +  "\" " + wcs + " and " + kony.sync.mainTableChangeTypeColumn+ " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableChangeTypeColumn+" LIKE '9%'";
	}else{
		errorcallback = successcallback;
		successcallback = wcs;
		sql="select * from \""+tbname+"\" WHERE "+kony.sync.mainTableChangeTypeColumn+ " is not null AND "+kony.sync.mainTableChangeTypeColumn+" <> -1 AND "+kony.sync.mainTableChangeTypeColumn+" LIKE '9%'"; 
	}
	
	kony.sync.single_select_execute(dbname, sql, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering WTSync.wtattendance.getDeferredUpload->success callback function");
		kony.sync.verifyAndCallClosure(successcallback, WTSync.wtattendance.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
	}	
};

/************************************************************************************
* Rollbacks all changes to wtattendance in local database to last synced state
*************************************************************************************/
WTSync.wtattendance.rollbackPendingLocalChanges = function(successcallback, errorcallback){
	sync.log.trace("Entering WTSync.wtattendance.rollbackPendingLocalChanges->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = WTSync.wtattendance.getTableName();
	kony.sync.konySyncRollBackPendingChanges(tbname, dbname, null, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering WTSync.wtattendance.rollbackPendingLocalChanges->main function");
		kony.sync.verifyAndCallClosure(successcallback, res);
	}		
};

/************************************************************************************
* Rollbacks changes to wtattendance's record with given primary key in local 
* database to last synced state
*************************************************************************************/
WTSync.wtattendance.prototype.rollbackPendingLocalChangesByPK = function(successcallback,errorcallback){
	sync.log.trace("Entering WTSync.wtattendance.prototype.rollbackPendingLocalChangesByPK function");
	var pks = this.getPKTable();
	WTSync.wtattendance.rollbackPendingLocalChangesByPK(pks,successcallback,errorcallback);
};
WTSync.wtattendance.rollbackPendingLocalChangesByPK = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering WTSync.wtattendance.rollbackPendingLocalChangesByPK->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "WTSync.wtattendance.rollbackPendingLocalChangesByPK",  "rollbackPendingLocalChangesByPK", errorcallback)){
		return;
	}	
	var dbname = kony.sync.getDBName();
	var tbname = WTSync.wtattendance.getTableName();
	var wcs = [];
	if(WTSync.wtattendance.pkCheck(pks,wcs,errorcallback,"rollbacking")===false){
		return;
	}	
	kony.sync.konySyncRollBackPendingChanges(tbname, dbname, wcs, mySuccesscallback, pkNotFoundErrCallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering WTSync.wtattendance.rollbackPendingLocalChangesByPK->success callback function");
		kony.sync.verifyAndCallClosure(successcallback, res);
	}	
	function pkNotFoundErrCallback(){
		sync.log.error("Entering WTSync.wtattendance.rollbackPendingLocalChangesByPK->PK not found callback");
		kony.sync.pkNotFoundErrCallback(errorcallback,tbname);
	}
};

/************************************************************************************
* isRecordDeferredForUpload returns true or false depending on whether wtattendance's record  
* with given primary key got deferred in last sync
*************************************************************************************/
WTSync.wtattendance.prototype.isRecordDeferredForUpload = function(successcallback,errorcallback){
	sync.log.trace("Entering  WTSync.wtattendance.prototype.isRecordDeferredForUpload function");
	var pks = this.getPKTable();
	WTSync.wtattendance.isRecordDeferredForUpload(pks,successcallback,errorcallback);
};
WTSync.wtattendance.isRecordDeferredForUpload = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering WTSync.wtattendance.isRecordDeferredForUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "WTSync.wtattendance.isRecordDeferredForUpload",  "isRecordDeferredForUpload", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = WTSync.wtattendance.getTableName();
	var wcs = [] ;
	var flag;
	if(WTSync.wtattendance.pkCheck(pks,wcs,errorcallback,"selecting")===false){
		return;
	}
	var twcs = [];
	twcs = kony.sync.CreateCopy(wcs);
	kony.table.insert(twcs, {
			key : kony.sync.mainTableChangeTypeColumn,
			value : "9%",
			optype : "LIKE",
			comptype : "AND"
		});
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_where(query, twcs);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];
	kony.sync.single_select_execute(dbname, sql, params, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering WTSync.wtattendance.isRecordDeferredForUpload->successcallback function");
		if(res.length === 1){
			flag = true;
		}
		else{
			flag = false;
		}
		kony.sync.verifyAndCallClosure(successcallback, {deferred:flag});
	}
};

/************************************************************************************
* isRecordPendingForUpload returns true or false depending on whether wtattendance's record  
* with given primary key is pending for upload
*************************************************************************************/
WTSync.wtattendance.prototype.isRecordPendingForUpload = function(successcallback,errorcallback){
	sync.log.trace("Entering  WTSync.wtattendance.prototype.isRecordPendingForUpload function");
	var pks = this.getPKTable();
	WTSync.wtattendance.isRecordPendingForUpload(pks,successcallback,errorcallback);
};
WTSync.wtattendance.isRecordPendingForUpload = function(pks, successcallback, errorcallback){
	sync.log.trace("Entering WTSync.wtattendance.isRecordPendingForUpload->main function");
	if(!kony.sync.isSyncInitialized(errorcallback)){
		return;
	}
	if(!kony.sync.validateInput(arguments, "WTSync.wtattendance.isRecordPendingForUpload",  "isRecordPendingForUpload", errorcallback)){
		return;
	}
	var dbname = kony.sync.getDBName();
	var tbname = WTSync.wtattendance.getTableName();
	var wcs = [] ;
	var flag;
	if(WTSync.wtattendance.pkCheck(pks,wcs,errorcallback,"selecting")===false){
		return;
	}
	var twcs = [];
	twcs = kony.sync.CreateCopy(wcs);
	kony.table.insert(twcs, {
			key : kony.sync.mainTableChangeTypeColumn,
			value : "9%",
			optype : "NOT LIKE",
			comptype : "AND"
		});
	var query = kony.sync.qb_createQuery();
				kony.sync.qb_select(query, null);
				kony.sync.qb_from(query, tbname);
				kony.sync.qb_where(query, twcs);
	var query_compile = kony.sync.qb_compile(query);
	var sql = query_compile[0];
	var params = query_compile[1];
	kony.sync.single_select_execute(dbname, sql, params, mySuccesscallback, errorcallback);
	function mySuccesscallback(res){
		sync.log.trace("Entering WTSync.wtattendance.isRecordPendingForUpload->successcallback function");
		if(res.length === 1){
			flag = true;
		}
		else{
			flag = false;
		}
		kony.sync.verifyAndCallClosure(successcallback, {pending:flag});
	}
};




/************************************************************************************
* Start of helper functions used internally, not to be used as ORMs
*************************************************************************************/

//Deletes all the dependant tables in the relationship tables.Need to pass transaction handler as input
WTSync.wtattendance.removeCascade = function(tx, wcs, errorcallback, markForUpload, isCascade, parentTable, isLocal){
	sync.log.trace("Entering WTSync.wtattendance.removeCascade function");
	var tbname = WTSync.wtattendance.getTableName();
	markForUpload = kony.sync.getUploadStatus(markForUpload);
	function removeCascadeChildren(){
	}
	if(isCascade){
		if(removeCascadeChildren()===false){
			return false;
		}
		if(kony.sync.deleteBatch(tx, tbname, wcs, isLocal,markForUpload, null)===false){
			return false;
		}
		return true;
	}else{
		var sql = "select * from \"" + tbname + "\" " + wcs;
		var resultSet = kony.sync.executeSql(tx, sql, null);
		if(resultSet===false){
			return false;
		}	
		var num_records = resultSet.rows.length;
		if(num_records === 0){
			return true;
		}else{
			sync.log.error(kony.sync.getReferetialIntegrityDeleteErrMessg(tbname,tbname,tbname,parentTable));
			errorcallback(kony.sync.getErrorTable(kony.sync.errorCodeReferentialIntegrity,kony.sync.getReferetialIntegrityDeleteErrMessg(tbname,tbname,tbname,parentTable)));
			return false;
		}
	}
};


WTSync.wtattendance.convertTableToObject = function(res){
	sync.log.trace("Entering WTSync.wtattendance.convertTableToObject function");
	objMap = [];
	if(res!==null){
		for(var i in res){
			var obj = new WTSync.wtattendance();
			obj.attendanceId = res[i].attendanceId;
			obj.employeeCode = res[i].employeeCode;
			obj.email = res[i].email;
			obj.startDateStr = res[i].startDateStr;
			obj.startTimestamp = res[i].startTimestamp;
			obj.startLocLatitude = res[i].startLocLatitude;
			obj.startLocLongitude = res[i].startLocLongitude;
			obj.endDateStr = res[i].endDateStr;
			obj.endTimestamp = res[i].endTimestamp;
			obj.endLocLatitude = res[i].endLocLatitude;
			obj.endLocLongitude = res[i].endLocLongitude;
			obj.isOverTime = res[i].isOverTime;
			obj.startLocComment = res[i].startLocComment;
			obj.endLocComment = res[i].endLocComment;
			obj.isDeleted = res[i].isDeleted;
			obj.lastModifiedDate = res[i].lastModifiedDate;
			obj.startTimestampStr = res[i].startTimestampStr;
			obj.endTimestampStr = res[i].endTimestampStr;
			obj.branchNumber = res[i].branchNumber;
			obj.markForUpload = (Math.floor(res[i].konysyncchangetype/10)==9)? false:true;
			objMap[i] = obj;
		}
	}
	return objMap;
};

WTSync.wtattendance.filterAttributes = function(valuestable, insert){
	sync.log.trace("Entering WTSync.wtattendance.filterAttributes function");
	var attributeTable = {};
	attributeTable.attendanceId = "attendanceId";
	attributeTable.employeeCode = "employeeCode";
	attributeTable.email = "email";
	attributeTable.startDateStr = "startDateStr";
	attributeTable.startTimestamp = "startTimestamp";
	attributeTable.startLocLatitude = "startLocLatitude";
	attributeTable.startLocLongitude = "startLocLongitude";
	attributeTable.endDateStr = "endDateStr";
	attributeTable.endTimestamp = "endTimestamp";
	attributeTable.endLocLatitude = "endLocLatitude";
	attributeTable.endLocLongitude = "endLocLongitude";
	attributeTable.isOverTime = "isOverTime";
	attributeTable.startLocComment = "startLocComment";
	attributeTable.endLocComment = "endLocComment";
	attributeTable.startTimestampStr = "startTimestampStr";
	attributeTable.endTimestampStr = "endTimestampStr";
	attributeTable.branchNumber = "branchNumber";

	var PKTable = {};
	PKTable.attendanceId = {}
	PKTable.attendanceId.name = "attendanceId";
	PKTable.attendanceId.isAutoGen = true;
	var newvaluestable = {};
	for (var k in valuestable){
		var v = valuestable[k];
		if(kony.sync.isNull(attributeTable[k])) { 
			sync.log.warn("Ignoring the attribute " + k + " for the SyncObject wtattendance. "  + k + " not defined as an attribute in SyncConfiguration.");
		}else if(!kony.sync.isNull(PKTable[k])) {
			if(insert===false){
				sync.log.warn("Ignoring the primary key " + k + " for the SyncObject wtattendance. Primary Key should not be the part of the attributes to be updated in the local device database.");
			}else if(PKTable[k].isAutoGen){
				sync.log.warn("Ignoring the auto-generated primary key " + k + " for the SyncObject wtattendance. Auto-generated Primary Key should not be the part of the attributes to be inserted in the local device database.");
			}else{
				newvaluestable[k] = v;
			}
		}
		else{
			newvaluestable[k] = v;
		}
	}
	return newvaluestable;
};

WTSync.wtattendance.formOrderByClause = function(orderByMap){
	sync.log.trace("Entering WTSync.wtattendance.formOrderByClause function");
	if(!kony.sync.isNull(orderByMap)){
		var valuestable = kony.sync.convertOrderByMapToValuesTable(orderByMap);
		//var filteredValuestable = WTSync.wtattendance.filterAttributes(valuestable, true);
		return kony.sync.convertToValuesTableOrderByMap(orderByMap,valuestable);
	}
	return null;
};

WTSync.wtattendance.prototype.getValuesTable = function(isInsert){
	sync.log.trace("Entering WTSync.wtattendance.prototype.getValuesTable function");
	var valuesTable = {};
	if(isInsert===true){
		valuesTable.attendanceId = this.attendanceId;
	}
	valuesTable.employeeCode = this.employeeCode;
	valuesTable.email = this.email;
	valuesTable.startDateStr = this.startDateStr;
	valuesTable.startTimestamp = this.startTimestamp;
	valuesTable.startLocLatitude = this.startLocLatitude;
	valuesTable.startLocLongitude = this.startLocLongitude;
	valuesTable.endDateStr = this.endDateStr;
	valuesTable.endTimestamp = this.endTimestamp;
	valuesTable.endLocLatitude = this.endLocLatitude;
	valuesTable.endLocLongitude = this.endLocLongitude;
	valuesTable.isOverTime = this.isOverTime;
	valuesTable.startLocComment = this.startLocComment;
	valuesTable.endLocComment = this.endLocComment;
	valuesTable.startTimestampStr = this.startTimestampStr;
	valuesTable.endTimestampStr = this.endTimestampStr;
	valuesTable.branchNumber = this.branchNumber;
	return valuesTable;
};

WTSync.wtattendance.prototype.getPKTable = function(){
	sync.log.trace("Entering WTSync.wtattendance.prototype.getPKTable function");
	var pkTable = {};
	pkTable.attendanceId = {key:"attendanceId",value:this.attendanceId};
	return pkTable;
};

WTSync.wtattendance.getPKTable = function(){
	sync.log.trace("Entering WTSync.wtattendance.getPKTable function");
	var pkTable = [];
	pkTable.push("attendanceId");
	return pkTable;
};

WTSync.wtattendance.pkCheck = function(pks,wcs,errorcallback,opName){
	sync.log.trace("Entering WTSync.wtattendance.pkCheck function");
	var wc = [];
	if(kony.sync.isNull(pks)){
		sync.log.error("Primary Key attendanceId not specified in  " + opName + "  an item in wtattendance");
		kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified,kony.sync.getPrimaryKeyNotSpecifiedMsg("attendanceId",opName,"wtattendance")));
		return false;	
	}
	else if(kony.sync.isValidJSTable(pks)){
		if(!kony.sync.isNull(pks.attendanceId)){
			if(!kony.sync.isNull(pks.attendanceId.value)){
				wc.key = "attendanceId";
				wc.value = pks.attendanceId.value;
			}
			else{
				wc.key = "attendanceId";
				wc.value = pks.attendanceId;
			}
		}else{
			sync.log.error("Primary Key attendanceId not specified in  " + opName + "  an item in wtattendance");
			kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified,kony.sync.getPrimaryKeyNotSpecifiedMsg("attendanceId",opName,"wtattendance")));
			return false;
		}
	}
	else{
		wc.key = "attendanceId";
		wc.value = pks;
	}	
	kony.table.insert(wcs,wc);
	return true;
};

WTSync.wtattendance.validateNull = function (valuestable,errorcallback){
	sync.log.trace("Entering WTSync.wtattendance.validateNull function");
	return true;
};

WTSync.wtattendance.validateNullInsert = function (valuestable,errorcallback){
	sync.log.trace("Entering WTSync.wtattendance.validateNullInsert function");
	return true;
};

WTSync.wtattendance.getRelationshipMap = function(relationshipMap,valuestable){
	sync.log.trace("Entering WTSync.wtattendance.getRelationshipMap function");
	var r1 = {};

	return relationshipMap;
};


WTSync.wtattendance.checkPKValueTables = function (valuetables)	{
	var checkPksNotNullFlag = true;
	for(var i = 0; i < valuetables.length; i++)	{
		if(kony.sync.isNull(valuetables[i])){
			checkPksNotNullFlag = false;
			break;
		}
	}
	return checkPksNotNullFlag;
};

WTSync.wtattendance.getTableName = function(){
	return "wtattendance";
};




// **********************************End wtattendance's helper methods************************