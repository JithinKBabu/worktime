/**********************************************************************
*	Author  : RI - CTS363601 
*	Purpose : Module to refer sync based methods
***********************************************************************/

/** Start sync session **/
function initDataSync(){
  	if (gblShowCustomProgress === true) {
      	showProgressPopup("common.message.syncStarting", "common.label.dataSync");
    } else {
      	Home.lblProgress.text		= getI18nString("common.message.syncStarting");
    }
  	gblIsSyncInProgress				= true;
    var config 						= {};
  	config.userid					= WTConstant.SYNC_USER_ID;
	config.password					= WTConstant.SYNC_PASSWORD;
	config.appid 					= WTConstant.SYNC_APP_ID;
	config.serverhost 				= WTConstant.SYNC_SERVER_HOST;
    config.serverport               = WTConstant.SYNC_SERVER_PORT;
  	config.issecure 				= WTConstant.SYNC_IS_SECURE;
  	config.sessiontasks 			= {"WTSync":{doupload:WTConstant.SYNC_DO_UPLOAD, dodownload:WTConstant.SYNC_DO_DOWNLOAD, uploaderrorpolicy:WTConstant.SYNC_UPLOAD_ERROR_POLICY}};
	config.removeafterupload 		= {"WTSync":[]};
	
  	//sync life cycle callbacks
	config.onsyncstart 				= onSyncStartCallback;
	config.onscopestart 			= onScopeStartCallback;
	config.onscopecerror 			= onScopeErrorCallback;
	config.onscopesuccess 			= onScopeSuccessCallback;
	config.onuploadstart 			= onUploadStartCallback;
	config.onuploadsuccess 			= onUploadSuccessCallback;
	config.onsyncsuccess 			= onSyncSuccessCallback;
	config.onsyncerror 				= onSyncErrorCallBack;
  	sync.startSession(config);
}

/** Stop sync session **/
function stopSyncSession(){
  	sync.stopSession(onSyncStopSessionCallback);
}

/** Reset sync **/
function resetSync(){
  	showLoading("common.message.loading");
  	sync.reset(onSyncResetSuccessCallback, onSyncResetFailureCallback);
}

/** Sync start callback **/
function onSyncStartCallback(response){
  	
}

/** Sync scope start callback **/
function onScopeStartCallback(response){
  	if (gblShowCustomProgress === true) {
      	updateProgressMessage("common.message.syncInProgress");
    } else {
      	Home.lblProgress.text		= getI18nString("common.message.syncInProgress");
    }
}

/** Sync scope error callback **/
function onScopeErrorCallback(response){

}

/** Sync scope success callback **/
function onScopeSuccessCallback(response){
  	if (gblShowCustomProgress === true) {
      	updateProgressMessage("common.message.syncDone");
    } else {
      	Home.lblProgress.text		= getI18nString("common.message.syncDone");
    }
}

/** Sync upload start callback **/
function onUploadStartCallback(response){

}

/** Sync upload success callback **/
function onUploadSuccessCallback(response){
}

/** Sync success callback **/
function onSyncSuccessCallback(response){
  	var today 				= new Date();
	var dateStr 			= today.toLocaleDateString();
	var timeStr 			= today.toLocaleTimeString();
  	var syncDateStr			= dateStr + " " + timeStr;
  	kony.store.setItem(WTConstant.LAST_SYNC_DATE, syncDateStr);
  	gblIsSyncInProgress		= false;
  	resetSync();
  	
  	if (gblShowCustomProgress === true) {
      	dismissProgressMessage();
    } else {
      	setFlxOverlayMsgOkButtonStyle(true);
      	dismissLoading();
    }
}

/** Sync error callback **/
function onSyncErrorCallBack(response){
  	gblIsSyncInProgress			= false;
  	if (gblShowCustomProgress === true) {
      	dismissProgressMessage();
      	showMessagePopup("common.message.syncError", ActionMessagePopupDismiss);
    } else {
      	Home.lblProgress.text	= getI18nString("common.message.syncError");
      	setFlxOverlayMsgOkButtonStyle(true);
      	dismissLoading();
    }
}

/** Sync stop session callback **/
function onSyncStopSessionCallback(response){
  	gblIsSyncInProgress		= false;
  	dismissProgressMessage();
  	setFlxOverlayMsgOkButtonStyle(true);
  	dismissLoading();
}

/** Sync reset success callback **/
function onSyncResetSuccessCallback(response){
  	dismissLoading();
}

/** Sync reset failure callback **/
function onSyncResetFailureCallback(response){
  	dismissLoading();
}