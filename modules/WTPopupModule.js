/**********************************************************************
*	Author  : RI - CTS363601 
*	Purpose : Module to refer Popup related methods
***********************************************************************/

/** Popup for displaying warnings and messages **/
function showMessagePopup(msgKey, buttonHandlerAction){
  	PopupMessage.lblTitle.text		= getI18nString("common.app.title");
  	PopupMessage.lblMessage.text	= getI18nString(msgKey);
    PopupMessage.btnOk.text			= getI18nString("common.label.ok");
  	if (buttonHandlerAction !== null){
      	PopupMessage.btnOk.onClick	= buttonHandlerAction;
    } else {
      	PopupMessage.btnOk.onClick	= ActionPopupMessageOk;
    }
    PopupMessage.show();  
}

/** Popup - Ok button handler **/
function dismissMessagePopup(){
  	PopupMessage.dismiss(); 
}

/** Confirm popup - show **/
function showConfirmPopup(msgKey, yesButtonHandlerAction, noButtonHandlerAction){
  	PopupConfirmation.lblTitle.text		= getI18nString("common.app.title");
  	PopupConfirmation.lblMessage.text	= getI18nString(msgKey);
    PopupConfirmation.btnLeft.text		= getI18nString("common.label.no");
  	PopupConfirmation.btnRight.text		= getI18nString("common.label.yes");
    PopupConfirmation.btnLeft.onClick	= noButtonHandlerAction;
    PopupConfirmation.btnRight.onClick	= yesButtonHandlerAction;
    PopupConfirmation.show();  
}

/** Confirm popup - Yes btn action **/
function onClickConfirmPopupYesBtn(){
  	dismissConfirmPopup();
  	exitApplication();
}

/** Confirm popup - No btn action **/
function onClickConfirmPopupNoBtn(){
  	dismissConfirmPopup();
}

/** Confirm logout popup - Yes btn action **/
function onClickLogoutConfirmPopupYesBtn(){
  	showLoading("common.message.loading");
  	kony.store.removeItem(WTConstant.WT_RECORD_IN_STORE);
  	kony.store.removeItem(WTConstant.LAST_SYNC_DATE);
  	dismissConfirmPopup(); 
  	resetGlobals();
  	deleteEmployeeDetailsTransaction();
}

/** Success callback - SQL query execution - delete employee record **/
function success_deleteEmployeeDetails(){
	dismissLoading();
  	exitApplication();
}

/** Error callback - SQL query execution - delete employee record **/
function error_deleteEmployeeDetails(error) {
	dismissLoading();
  	exitApplication();
}

/** Confirm popup - dismiss **/
function dismissConfirmPopup(){
  	PopupConfirmation.dismiss(); 
}

/** Progress popup - show **/
function showProgressPopup(msgKey, titleKey){
  	PopupProgress.lblTitle.text		= getI18nString(titleKey);
    PopupProgress.lblMessage.text	= getI18nString(msgKey);
  	PopupProgress.show();
}

/** Progress popup - update message **/
function updateProgressMessage(msgKey){
  	 PopupProgress.lblMessage.text	= getI18nString(msgKey);
}

/** Progress popup - dismiss **/
function dismissProgressMessage(){
  	 PopupProgress.dismiss();
}

/** Confirm sync stop confirm - Yes btn action **/
function onClickSyncStopConfirmYesBtn(){
  	showLoading("common.message.loading");
  	stopSyncSession();
  	dismissConfirmPopup(); 
}

/** On click message popup Ok button action - This will exit application **/
function onClickMessagePopupOkButton(){
  	showLoading("common.message.loading");
  	dismissMessagePopup();
  	dismissLoading();
  	exitApplication();
}

/** On click message popup Ok button action - This will just dismiss message popup **/
function hideMessagePopup(){
  	dismissMessagePopup();
  	dismissLoading();
}