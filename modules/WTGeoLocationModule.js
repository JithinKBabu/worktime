/**********************************************************************
*	Author  : RI - CTS363601 
*	Purpose : Module to refer the geo location based methods
***********************************************************************/

/** To capture current location **/
/** timeout = 5 seconds and cached location age = 10 minutes **/
function captureCurrentLocation(successCallbackHandler, errorCallbackHandler){
  	var positionOptions 						= {timeout: 5000, maximumAge: 600000};   	
  	var successCallbackMethod					= (!isEmpty(successCallbackHandler)) ? successCallbackHandler : success_geoLocCallback;
  	var failureCallbackMethod					= (!isEmpty(errorCallbackHandler)) ? errorCallbackHandler : error_geoLocCallback;
  	kony.location.getCurrentPosition(successCallbackMethod, failureCallbackMethod, positionOptions);
}

/** Success callback locaion capture - At start work **/
function success_geoLocCallbackStartWork(position) {
  	gblAttendanceObject.startLocLatitude		= position.coords.latitude;
  	gblAttendanceObject.startLocLongitude		= position.coords.longitude;  
  	if (!isEmpty(gblPrevAttendanceObject)){
      	gblPrevAttendanceObject.endDateStr		= gblAttendanceObject.startDateStr;
        gblPrevAttendanceObject.endTimestamp	= gblAttendanceObject.startTimestamp;
        gblPrevAttendanceObject.endTimestampStr	= getTimeInGMT(gblAttendanceObject.startTimestamp);
        gblPrevAttendanceObject.endLocLatitude	= position.coords.latitude;
        gblPrevAttendanceObject.endLocLongitude	= position.coords.longitude;
      	createAttendanceRecordInDB(gblPrevAttendanceObject);
    }
  	saveWorkTimeRecordInStore(gblAttendanceObject);
  	createTraceRecordInDB(gblAttendanceObject, true);
}

/** Failure callback locaion capture - At start work **/
function error_geoLocCallbackStartWork(positionerror){
  	gblAttendanceObject.startLocLatitude		= "";
  	gblAttendanceObject.startLocLongitude		= "";
  	gblAttendanceObject.startLocComment			= positionerror.message;  
  	if (!isEmpty(gblPrevAttendanceObject)){
      	gblPrevAttendanceObject.endDateStr		= gblAttendanceObject.startDateStr;
        gblPrevAttendanceObject.endTimestamp	= gblAttendanceObject.startTimestamp;
        gblPrevAttendanceObject.endTimestampStr	= getTimeInGMT(gblAttendanceObject.startTimestamp);
        gblPrevAttendanceObject.endLocLatitude	= "";
        gblPrevAttendanceObject.endLocLongitude	= "";
        gblPrevAttendanceObject.endLocComment	= positionerror.message;
      	createAttendanceRecordInDB(gblPrevAttendanceObject);
    }
  	saveWorkTimeRecordInStore(gblAttendanceObject);
  	createTraceRecordInDB(gblAttendanceObject, true); 
  	if (positionerror.message == WTConstant.LOCATION_UNAVAILABLE || positionerror.message == WTConstant.LOCATION_TIMEOUT || positionerror.message == WTConstant.LOCATION_PERMSN_DENIED) {
      	showMessagePopup("common.message.turnOnLocation", ActionMessagePopupDismiss);
    }
}

/** Success callback locaion capture - At stop work **/
function success_geoLocCallbackStopWork(position) {
  	gblAttendanceObject.endLocLatitude			= position.coords.latitude;
  	gblAttendanceObject.endLocLongitude			= position.coords.longitude;  	  
  	gblTransactionFlags							= {};
  	gblTransactionFlags.trace					= false;
  	gblTransactionFlags.attendance				= false;
  	gblTransactionFlags.todaysAttendace			= false;
  	gblTransactionFlags.getAllTrace				= false;
  	gblTransactionFlags.moveAllTrace			= false;
  	gblTransactionFlags.deleteAllTemp			= false;
  	startTransactionWaitTimer();
  	createTraceRecordInDB(gblAttendanceObject, false);
}

/** Failure callback locaion capture - At stop work **/
function error_geoLocCallbackStopWork(positionerror){
  	gblAttendanceObject.endLocLatitude			= "";
  	gblAttendanceObject.endLocLongitude			= "";
  	gblAttendanceObject.endLocComment			= positionerror.message;  
  	gblTransactionFlags							= {};
  	gblTransactionFlags.trace					= false;
  	gblTransactionFlags.attendance				= false;
  	gblTransactionFlags.todaysAttendace			= false;
  	gblTransactionFlags.getAllTrace				= false;
  	gblTransactionFlags.moveAllTrace			= false;
  	gblTransactionFlags.deleteAllTemp			= false;
  	startTransactionWaitTimer();
  	createTraceRecordInDB(gblAttendanceObject, false);
}