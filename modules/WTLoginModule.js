/**********************************************************************
*	Author  : RI - CTS363601 
*	Purpose : UI Screen - Login - Methods
***********************************************************************/

/** Login screen - PreShow **/
function onPreShowLoginScreen(){
  	printMessage("#***# Inside Login - Pre Show #***#");
  	gblEmployeeObject					= null;
	Login.flxPopupOverlayContainer.setVisibility(false);  
  	Login.lblUsername.text				= getI18nString("common.message.welcome");
  	Login.lblPopupTitle.text			= getI18nString("common.label.chooseUser");
  	Login.btnCancel.text				= getI18nString("common.label.cancel");
  	Login.btnOk.text					= getI18nString("common.label.ok");
  	Login.segEmailList.setData([]); 
    Login.listBusiness.masterData = businessCodes;
}

/** Login screen - PostShow **/
function onPostShowLoginScreen(){	
  	printMessage("#***# Inside Login - Post Show #***#");
  	showLoading("common.message.loading");
  	initSyncModule();
}

/** Sync initialization - Success callback  **/
function success_initSyncModule(response){
	printMessage("#***# Inside Init Sync - Success #***#");
  	getEmployeeDetailsTransaction();
}				

/** Sync initialization - Failure callback  **/
function failure_initSyncModule(response){
	printMessage("#***# Inside Init Sync - Failure #***#");
  	dismissLoading();
}

/** Success callback - SQL query execution - get employee records **/
function success_getEmployeeDetails(result){
  	printMessage("#***# Inside success_getEmployeeDetails #***#");
	if(result !== null && result.length > 0) {
      	var rowItem 					= null;	
      	for (var i = 0; i < result.length; i++) {
  			rowItem						= result[i];
          	showHomeScreen(rowItem, false);
        }
    } else {
      	getDeviceConfiguredEmails();
    }
}

/** Error callback - SQL query execution - get employee records **/
function error_getEmployeeDetails(error) {
  	printMessage("#***# Inside error_getEmployeeDetails #***#");
  	getDeviceConfiguredEmails();
}

/** Validate configured emails in device **/
function getDeviceConfiguredEmails(){
    var configuredEmails 				= EmailChecker.getEmail(); 
  	var emailsCount						= configuredEmails.length;
  	var segMasterData					= [];  	
  	if (emailsCount > 0){
      	if (emailsCount > 1){          	
          	var aRecord					= null;
          	var anItem					= null;
          	for (var i = 0; i < emailsCount; i++){
              	aRecord					= configuredEmails[i];
              	anItem					= {};
               	anItem.lblItemName		= aRecord;
              	anItem.imgSelection 	= (i === 0) ? WTConstant.RADIO_BTN_ON : WTConstant.RADIO_BTN_OFF;
              	segMasterData.push(anItem);           
            }
          	Login.segEmailList.setData(segMasterData);
          	gblSegEmailLastSelIndex		= 0;
          	Login.flxPopupOverlayContainer.setVisibility(true);
          	dismissLoading();
        } else {
          	//if (isNetworkAvailable()){
              	/*** callAuthorizeEmail(configuredEmails[0]); ***/
              	var anInputObj			= {};
      			anInputObj.email		= configuredEmails[0];
      	      Login.flxPopupOverlayContainer.setVisibility(true);
                 Login.flxPopupContainer.setVisibility(true);
                 Login.flexPopupBranch.setVisibility(false);
          
                selectedEmail	    = configuredEmails[0];
       			Login.flexPopupBranch.setVisibility(true);
       			Login.listBusiness.selectedKey      = WTConstant.SELECT;
       			Login.listbranchnumbers.masterData  = [[WTConstant.SELECT,WTConstant.SELECT]];
       			Login.listbranchnumbers.selectedKey = WTConstant.SELECT;
       			Login.flxPopupContainer.setVisibility(false);
       			Login.listbranchnumbers.setVisibility(false);
                dismissLoading();
  		
    }
    }else {
      	showMessagePopup("common.message.noRentokilEmailConfigured", ActionOnMessagePopupOkBtn);
      	dismissLoading();
    }  
}

/** To update welcome label text **/
function updateWelcomeLabel(){
  	/*** Login.lblUsername.text				= getI18nString("common.message.welcome") + " " + gblEmployeeObject.getEmployeeName(); ***/
  	Login.lblUsername.text				= getI18nString("common.message.welcome");
}

/** Integration service call for user authentication  **/
function callAuthorizeEmail(selectedEmail){
  	var inputParams						= {};
  	inputParams.serviceName				= "AuthorizeUserSIT"; 	//[ AuthorizeUserSIT or AuthorizeUser ]
  	inputParams.operationName			= "getEmployeeSIT";		//[ getEmployeeSIT or getEmployee ]
  	inputParams.inputs					= {"countryCode":WTConstant.DEF_COUNTRY_CODE, "businessCode":WTConstant.DEF_BUSINESS_CODE, "email":selectedEmail, "languageCode": WTConstant.DEF_LANGUAGE_CODE};
  	gblMFInputs.email					= selectedEmail;
  	callAuthenticateUserIntService(inputParams);
}

/** Success callback - Integration service - Autorize Email  **/
function success_authorizeEmailCallback(response){
	if (response.httpStatusCode === WTConstant.SERVICE_HTTP_STATUS){
      	if (response.opstatus === WTConstant.SERVICE_OPSTATUS){
          	if (response.message === WTConstant.SERVICE_MSG_SUCCESS){
              	showHomeScreen(response, true);
            } else {
              	showMessagePopup("common.message.userNotRecognized", ActionOnMessagePopupOkBtn);
      			dismissLoading();
            }
        } else {
          	showMessagePopup("common.message.serviceUnavailable", ActionOnMessagePopupOkBtn);
          	dismissLoading();
        }
    } else {
      	showMessagePopup("common.message.serviceUnavailable", ActionOnMessagePopupOkBtn);
        dismissLoading();
    }
}

/** Failure callback - Integration service - Autorize Email  **/
function failure_authorizeEmailCallback(response){
  	showMessagePopup("common.message.serviceUnavailable", ActionOnMessagePopupOkBtn);
  	dismissLoading();
}


/** On click Emails popup Ok button action **/
function onClickEmailsPopupOkButton(){
  	//showLoading("common.message.loading");
  	
	var selectedItem	= Login.segEmailList.data[gblSegEmailLastSelIndex];
   	 selectedEmail	= selectedItem.lblItemName;
     
  	if (isNetworkAvailable()){
  		/*** callAuthorizeEmail(selectedEmail); ***/
      	var anInputObj					= {};
      	anInputObj.email				= selectedEmail;
      //showHomeScreen(anInputObj, true);
      //calling the popup
      // showBranchPopup(selectedEmail);
       Login.flexPopupBranch.setVisibility(true);
       Login.listBusiness.selectedKey      = WTConstant.SELECT;
       Login.listbranchnumbers.masterData  = [[WTConstant.SELECT,WTConstant.SELECT]];
       Login.listbranchnumbers.selectedKey = WTConstant.SELECT;
       Login.listbranchnumbers.setVisibility(false);      
       Login.flxPopupContainer.setVisibility(false);
       
    } else {
      	showMessagePopup("common.message.noNetwork", ActionOnMessagePopupOkBtn);
      	dismissLoading();
    }
}

/** On click Emails popup Cancel button action **/
function onClickEmailsPopupCancelButton(){
  	showLoading("common.message.loading");
	Login.flxPopupOverlayContainer.setVisibility(false);
  	dismissLoading();
    exitApplication();
}


function onClickBranchPopupOkButton(){
  	showLoading("common.message.loading"); 
//  var selectedItem	= Login.segEmailList.data[gblSegEmailLastSelIndex];
  // 	var selectedEmail	= selectedItem.lblItemName;
  printMessage("selectedEmail----------->>>>"+selectedEmail);
  //validate business and brnach entered
  if((Login.listbranchnumbers.selectedKey  != WTConstant.SELECT) && (Login.listBusiness.selectedKey  != WTConstant.SELECT)){
  
  
  	Login.flxPopupContainer.setVisibility(false);
    Login.flexPopupBranch.setVisibility(false);
	//var selectedItem	= Login.segEmailList.data[gblSegEmailLastSelIndex];
  //	var selectedEmail	= selectedItem.lblItemName;
//	if (isNetworkAvailable()){
  		/*** callAuthorizeEmail(selectedEmail); ***/
      	var anInputObj					= {};
      	anInputObj.email				= selectedEmail;
        anInputObj.businessCode         = Login.listBusiness.selectedKey;
        anInputObj.branchNumber         = Login.listbranchnumbers.selectedKey;
        printMessage("anInputObj  : "+JSON.stringify(anInputObj) );
        showHomeScreen(anInputObj, true);
      
       
 /*   } else {
      	showMessagePopup("common.message.noNetwork", ActionOnMessagePopupOkBtn);
      	dismissLoading();
    }*/
    
  }else{
    

		Login.lblWarningMsg.text			= "Business and Branch are mandatory";
     	Login.btnYes.setVisibility(false);
        Login.flxBottomMsgContainer.setVisibility(true);
      	dismissLoading();
  
    
  }
}


function onClickBranchOverlayLayer(){
  	Login.flxBottomMsgContainer.setVisibility(false);
}



function updateBranchObj(widgetRef){
  
  
  
  printMessage("updateBranchObj --->>>"+JSON.stringify(widgetRef));
  //  var comboRef											= widgetRef.id.slice(-1);
    Login.listbranchnumbers.masterData =[];
    var selectedKey	= Login.listBusiness.selectedKey;
        
    if (selectedKey == WTConstant.PEST) {
      Login.listbranchnumbers.setVisibility(true);
      printMessage("updateBranchObj --->>>selectedKey"+selectedKey);
      Login.listbranchnumbers.masterData =pestbranches;
      Login.listbranchnumbers.selectedKey = WTConstant.SELECT;
    } else if(selectedKey == WTConstant.HYGIENE) {
      Login.listbranchnumbers.setVisibility(true);
       printMessage("updateBranchObj --->>>selectedKey"+selectedKey);
      Login.listbranchnumbers.masterData =hygienebranches;	
      Login.listbranchnumbers.selectedKey = WTConstant.SELECT;
    } else if(selectedKey == WTConstant.SELECT){
      Login.listbranchnumbers.setVisibility(false);
      printMessage("updateBranchObj --->>>selectedKey"+selectedKey);
      }
   
}



/** Selecting an email from segment list **/
function onClickSegEmailRow(){
  	var selectedRowIndex				= Login.segEmailList.selectedRowIndex[1];
  	var selectedSectionIndex			= Login.segEmailList.selectedRowIndex[0];
  	if (selectedRowIndex !== gblSegEmailLastSelIndex) {
      	var preSelectedRow				= Login.segEmailList.data[gblSegEmailLastSelIndex];
      	var curSelectedRow				= Login.segEmailList.data[selectedRowIndex];
      
      	preSelectedRow.imgSelection 	= WTConstant.RADIO_BTN_OFF;
      	curSelectedRow.imgSelection 	= WTConstant.RADIO_BTN_ON;
      
      	Login.segEmailList.setDataAt(preSelectedRow, gblSegEmailLastSelIndex, selectedSectionIndex);
      	Login.segEmailList.setDataAt(curSelectedRow, selectedRowIndex, selectedSectionIndex);
      
      	gblSegEmailLastSelIndex			= selectedRowIndex;
    }
}