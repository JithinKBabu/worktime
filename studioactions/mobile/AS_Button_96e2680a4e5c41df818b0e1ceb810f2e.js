function AS_Button_96e2680a4e5c41df818b0e1ceb810f2e(eventobject) {
    function SHOW_ALERT__520ccc051d92410fb1ca374852202759_True() {
        onClickLogout.call(this);
    }

    function SHOW_ALERT__520ccc051d92410fb1ca374852202759_False() {
        dismissMessagePopup.call(this);
    }

    function SHOW_ALERT__520ccc051d92410fb1ca374852202759_Callback(response) {
        if (response == true) {
            SHOW_ALERT__520ccc051d92410fb1ca374852202759_True()
        } else {
            SHOW_ALERT__520ccc051d92410fb1ca374852202759_False()
        };
    }
    kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_CONFIRMATION,
        "alertTitle": null,
        "yesLabel": "Yes",
        "noLabel": "No",
        "alertIcon": "logout_icon.png",
        "message": "Are you sure want to log out the application?",
        "alertHandler": SHOW_ALERT__520ccc051d92410fb1ca374852202759_Callback
    }, {
        "iconPosition": constants.ALERT_ICON_POSITION_LEFT
    })
}