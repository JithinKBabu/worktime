//startup.js file
var globalhttpheaders = {};
var appConfig = {
    appId: "WorkTime",
    appName: "Work Time",
    appVersion: "1.0",
    platformVersion: null,
    serverIp: "10.10.12.125",
    serverPort: "80",
    secureServerPort: "443",
    isDebug: false,
    middlewareContext: "WorkTime",
    isturlbase: "https://rentokil-initial.konycloud.com/services",
    isMFApp: true,
    appKey: "ee25144ef5d2e318da8841dbd8af73ba",
    appSecret: "e5b14dc6e5702de4690b34137d329ea2",
    serviceUrl: "https://100004898.auth.konycloud.com/appconfig",
    svcDoc: {
        "appId": "a7b662dc-1b60-4880-befb-1f496415944e",
        "baseId": "743b5a1e-39e6-4948-953b-f406f7c0b1a5",
        "name": "WorkTimeProd",
        "selflink": "https://100004898.auth.konycloud.com/appconfig",
        "sync": {
            "appId": "100004898743b5a1e",
            "url": "https://rentokil-initial.sync.konycloud.com/syncservice/api/v1/100004898743b5a1e"
        },
        "reportingsvc": {
            "custom": "https://rentokil-initial.konycloud.com/services/CMS",
            "session": "https://rentokil-initial.konycloud.com/services/IST"
        },
        "services_meta": {}
    },
    svcDocRefresh: false,
    svcDocRefreshTimeSecs: -1,
    eventTypes: ["FormEntry", "FormExit", "Touch", "Gesture", "Orientation", "ServiceRequest", "ServiceResponse", "Error", "Crash"],
    url: "https://rentokil-initial.konycloud.com/WorkTime/MWServlet",
    secureurl: "https://rentokil-initial.konycloud.com/WorkTime/MWServlet"
};
sessionID = "";

function appInit(params) {
    skinsInit();
    initializeflxRowSelectionContainer02c4cb792302d43();
    initializetplRowSegSelectionList();
    initializetplTempSegEmp();
    initializetplTempSegOthers();
    ContentGlobals();
    HomeGlobals();
    LoginGlobals();
    PopupConfirmationGlobals();
    PopupMessageGlobals();
    PopupProgressGlobals();
    setAppBehaviors();
};

function setAppBehaviors() {
    kony.application.setApplicationBehaviors({
        applyMarginPaddingInBCGMode: false,
        adherePercentageStrictly: true,
        retainSpaceOnHide: true,
        marginsIncludedInWidgetContainerWeight: true,
        APILevel: 7000
    })
};

function themeCallBack() {
    callAppMenu();
    initializeGlobalVariables();
    kony.application.setApplicationInitializationEvents({
        preappinit: AS_AppEvents_d7c985570eec418990647017250e5a1a,
        init: appInit,
        postappinit: AS_AppEvents_24da5bfb113946e699199d551f892dbc,
        showstartupform: function() {
            Login.show();
        }
    });
};

function loadResources() {
    globalhttpheaders = {};
    kony.os.loadLibrary({
        "javaclassname": "com.konylabs.ffi.N_EmailChecker"
    });
    kony.os.loadLibrary({
        "javaclassname": "com.konylabs.ffi.N_Trace"
    });
    sdkInitConfig = {
        "appConfig": appConfig,
        "isMFApp": appConfig.isMFApp,
        "appKey": appConfig.appKey,
        "appSecret": appConfig.appSecret,
        "eventTypes": appConfig.eventTypes,
        "serviceUrl": appConfig.serviceUrl
    }
    kony.setupsdks(sdkInitConfig, onSuccessSDKCallBack, onSuccessSDKCallBack);
};

function onSuccessSDKCallBack() {
    //This is to define sync global variable if application has sync
    var mbaasSdkObj = kony.sdk && kony.sdk.getCurrentInstance();
    if (mbaasSdkObj.getSyncService()) {
        sync = mbaasSdkObj.getSyncService();
    }
    kony.theme.setCurrentTheme("default", themeCallBack, themeCallBack);
}

function onSuccess(oldlocalname, newlocalename, info) {
    loadResources();
};

function onFailure(errorcode, errormsg, info) {
    loadResources();
};
kony.application.setApplicationMode(constants.APPLICATION_MODE_NATIVE);
//If default locale is specified. This is set even before any other app life cycle event is called.
kony.i18n.setDefaultLocaleAsync("en_GB", onSuccess, onFailure, null);