/**********************************************************************
 *	Author  : RI - CTS363601 
 *	Purpose : UI Screen - Settings(Burger Menu) - Methods
 ***********************************************************************/
/** Settings Menu (Burger Menu)- UI refreshing like i18n updates, default visibility and all **/
function refreshSettingsUI() {
    var frmObj = kony.application.getCurrentForm();
    var usernameStr = (!isEmpty(gblEmployeeObject)) ? (gblEmployeeObject.getEmployeeName()) : "";
    frmObj.flxSettingsContainer.centerX = "-50%";
    frmObj.lblVersion.text = getI18nString("common.label.version") + " " + WTConstant.APP_VERSION;
    /*** frmObj.lblUsername.text					= getI18nString("common.message.welcome") + " " + usernameStr; ***/
    frmObj.lblUsername.text = usernameStr;
    frmObj.lblAbout.text = getI18nString("common.label.about");
    frmObj.lblLogout.text = getI18nString("common.label.logout");
    frmObj.lblSync.text = getI18nString("common.label.sync");
    displayLastSyncDate();
}
/** Settings Menu (Burger Menu)- Open menu button action **/
function onClickOpenMenuButton() {
    var frmObj = kony.application.getCurrentForm();
    displayLastSyncDate();

    function openMenu_Callback() {}
    frmObj.flxSettingsContainer.animate(kony.ui.createAnimation({
        "100": {
            "centerX": "50%",
            "stepConfig": {
                "timingFunction": kony.anim.LINEAR
            }
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.25
    }, {
        "animationEnd": openMenu_Callback
    });
}
/** Settings Menu (Burger Menu)- Close menu button action **/
function closeSettingsMenu() {
    var frmObj = kony.application.getCurrentForm();

    function closeMenu_Callback() {}
    frmObj.flxSettingsContainer.animate(kony.ui.createAnimation({
        "100": {
            "centerX": "-50%",
            "stepConfig": {
                "timingFunction": kony.anim.LINEAR
            }
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.25
    }, {
        "animationEnd": closeMenu_Callback
    });
}
/** Settings Menu (Burger Menu)- About button action **/
function onClickAbout() {
    closeSettingsMenu();
    showContentScreen();
}
/** Settings Menu (Burger Menu)- Sync button action **/
function onClickSync() {
    closeSettingsMenu();
    var isActive = isWorkTimeActive();
    if (isActive === true) {
        showMessagePopup("common.message.cannotSync", ActionMessagePopupDismiss);
    } else {
        gblShowCustomProgress = true;
        initDataSync();
    }
}
/** Settings Menu (Burger Menu)- Logout button action **/
function onClickLogout() {
    closeSettingsMenu();
    var isActive = isWorkTimeActive();
    if (isActive === true) {
        //showConfirmPopup("common.message.confirmLogoutWithActiveTracking", ActionConfirmLogoutPopupYesBtn, ActionConfirmPopupNoBtn);
        showMessagePopup("common.message.cannotLogout", ActionMessagePopupDismiss);
    } else {
        showConfirmPopup("common.message.confirmLogout", ActionConfirmLogoutPopupYesBtn, ActionConfirmPopupNoBtn);
    }
}
/** The below function will handle touch start on settings screen **/
function handleTouchStartOnSettings(widgetRef, x, y) {
    gblTouchMovePrevX = x;
}
/** The below function will handle touch move on settings screen **/
function handleTouchMoveOnSettings(widgetRef, x, y) {
    var frmObj = kony.application.getCurrentForm();
    var curCenterXPercentageVal = frmObj.flxSettingsContainer.centerX;
    var curCenterXIntVal = parseInt(curCenterXPercentageVal.replace("%", ""));
    var deviceWidthVal = gblDeviceInfo.deviceWidth;
    var xDisplacement = (gblTouchMovePrevX > x) ? (gblTouchMovePrevX - x) : (x - gblTouchMovePrevX);
    var xDisplacementInPercent = (xDisplacement * 100) / deviceWidthVal;
    var newCenterXInPercent = (gblTouchMovePrevX > x) ? (curCenterXIntVal - xDisplacementInPercent) : (curCenterXIntVal + xDisplacementInPercent);
    if (newCenterXInPercent < 50 && newCenterXInPercent > -50) {
        frmObj.flxSettingsContainer.centerX = newCenterXInPercent.toFixed(2) + "%";
        frmObj.flxSettingsContainer.forceLayout();
    }
    gblTouchMovePrevX = x;
}
/** The below function will handle touch end on settings screen **/
function handleTouchEndOnSettings(widgetRef, x, y) {
    var frmObj = kony.application.getCurrentForm();
    var deviceWidthVal = gblDeviceInfo.deviceWidth;
    var flxMoveMinX = (deviceWidthVal * 30) / 100;
    if (x === gblTouchMovePrevX) {
        closeSettingsMenu();
    } else if (x < flxMoveMinX) {
        closeSettingsMenu();
    } else {
        onClickOpenMenuButton();
    }
}
/** Void action **/
function voidClick() {
    //printMessage("Void click - Nothing to do");
}
/** Last sync date displya method **/
function displayLastSyncDate() {
    var frmObj = kony.application.getCurrentForm();
    var lastSyncDate = kony.store.getItem(WTConstant.LAST_SYNC_DATE);
    if (!isEmpty(lastSyncDate)) {
        frmObj.lblLastSync.text = getI18nString("common.label.lastSyncDate");
        frmObj.lblLastSyncDate.text = (!isEmpty(lastSyncDate)) ? lastSyncDate : "";
    } else {
        frmObj.lblLastSync.text = "";
        frmObj.lblLastSyncDate.text = "";
    }
}