/**********************************************************************
 *	Author  : RI - CTS363601 
 *	Purpose : UI Screen - Home - Methods
 ***********************************************************************/
/** Show home screen **/
function showHomeScreen(userDataObj, isNewUser) {
    printMessage("#***# Inside showHomeScreen #***#");
    setGblUserObject(userDataObj);
    updateWelcomeLabel();
    if (isNewUser === true) {
        insertEmployeeDetailsTransaction();
    }
    Home.show();
}
/** Home screen - PreShow **/
function onPreShowHomeScreen() {
    printMessage("#***# Inside onPreShowHomeScreen #***#");
    dismissLoading();
    Home.lblStartWork.text = getI18nString("common.label.startWork");
    Home.lblStopWork.text = getI18nString("common.label.stopWork");
    Home.lblStartOvertime.text = getI18nString("common.label.startOvertime");
    Home.lblStopOvertime.text = getI18nString("common.label.endOvertime");
    Home.lblPopupTitle.text = getI18nString("popup.label.workTimeSummary");
    Home.lblProgress.text = "";
    Home.btnOk.text = getI18nString("common.label.ok");
    Home.lblTimer.text = "";
    Home.flxStartOverTimeInnerContainer.setEnabled(true);
    Home.flxStopOverTimeInnerContainer.setEnabled(false);
    Home.flxStartOverTimeInnerContainer.setEnabled(true);
    Home.flxStopOverTimeInnerContainer.setEnabled(false);
    Home.flxTimerContainer.setVisibility(false);
    Home.flxPopupOverlay.setVisibility(false);
    gblAttendanceObject = getWorkTimeRecordFromStore();
}
/** Home screen - PostShow **/
function onPostShowHomeScreen() {
    printMessage("#***# Inside onPostShowHomeScreen #***#");
    showLoading("common.message.loading");
    applyWTRunningStatusOnScreen(gblAttendanceObject);
    refreshSettingsUI();
}
/** To configure home screen based on current running mode **/
function applyWTRunningStatusOnScreen(record) {
    printMessage("#***# Inside applyWTRunningStatusOnScreen #***#");
    if (!isEmpty(record)) {
        var workStartedTimeStamp = getTimeFromTimestamp(record.startTimestamp);
        var workStoppedTimeStamp = 0;
        var isOvertimeRecord = record.isOverTime;
        Home.lblStartTimeValue.text = (isOvertimeRecord === false) ? workStartedTimeStamp.h + ":" + workStartedTimeStamp.m + ":" + workStartedTimeStamp.s : Home.lblStartTimeValue.text;
        Home.lblStartOvertimeTimeValue.text = (isOvertimeRecord === true) ? workStartedTimeStamp.h + ":" + workStartedTimeStamp.m + ":" + workStartedTimeStamp.s : Home.lblStartOvertimeTimeValue.text;
        switchButtonAccessibility(!isOvertimeRecord, false);
        if (record.endTimestamp === 0) {
            switchButtonAccessibility(isOvertimeRecord, true);
            startTimer();
        } else {
            switchButtonAccessibility(isOvertimeRecord, false);
            workStoppedTimeStamp = getTimeFromTimestamp(record.endTimestamp);
            Home.lblStopTimeValue.text = (isOvertimeRecord === false) ? workStoppedTimeStamp.h + ":" + workStoppedTimeStamp.m + ":" + workStoppedTimeStamp.s : Home.lblStopTimeValue.text;
            Home.lblStopOverTimeValue.text = (isOvertimeRecord === true) ? workStoppedTimeStamp.h + ":" + workStoppedTimeStamp.m + ":" + workStoppedTimeStamp.s : Home.lblStopOverTimeValue.text;
        }
    }
    dismissLoading();
}
/** Start day button click **/
function onClickStartDay() {
    showLoading("common.message.loading");
    switchButtonAccessibility(false, true);
    switchButtonAccessibility(true, false);
    var currentTimeStamp = getCurrentTimestamp();
    var currentTimeSplits = getTimeFromTimestamp(currentTimeStamp);
    Home.lblStartTimeValue.text = currentTimeSplits.h + ":" + currentTimeSplits.m + ":" + currentTimeSplits.s;
    Home.lblStopTimeValue.text = "";
    var isPreWorkTimeStillActive = isWorkTimeActive();
    if (isPreWorkTimeStillActive === true) {
        gblPrevAttendanceObject = clone(gblAttendanceObject);
        Home.lblStopOverTimeValue.text = currentTimeSplits.h + ":" + currentTimeSplits.m + ":" + currentTimeSplits.s;
    }
    gblAttendanceObject = null;
    gblAttendanceObject = new Attendance();
    gblAttendanceObject.setOverTimeFlag(false);
    gblAttendanceObject.startDateStr = getToday();
    gblAttendanceObject.startTimestamp = currentTimeStamp;
    gblAttendanceObject.startTimestampStr = getTimeInGMT();
    startTimer();
    captureCurrentLocation(success_geoLocCallbackStartWork, error_geoLocCallbackStartWork);
}
/** Start overtime button click **/
function onClickStartOvertime() {
    showLoading("common.message.loading");
    switchButtonAccessibility(true, true);
    switchButtonAccessibility(false, false);
    var currentTimeStamp = getCurrentTimestamp();
    var currentTimeSplits = getTimeFromTimestamp(currentTimeStamp);
    Home.lblStartOvertimeTimeValue.text = currentTimeSplits.h + ":" + currentTimeSplits.m + ":" + currentTimeSplits.s;
    Home.lblStopOverTimeValue.text = "";
    var isPreWorkTimeStillActive = isWorkTimeActive();
    if (isPreWorkTimeStillActive === true) {
        gblPrevAttendanceObject = clone(gblAttendanceObject);
        Home.lblStopTimeValue.text = currentTimeSplits.h + ":" + currentTimeSplits.m + ":" + currentTimeSplits.s;
    }
    gblAttendanceObject = null;
    gblAttendanceObject = new Attendance();
    gblAttendanceObject.setOverTimeFlag(true);
    gblAttendanceObject.startDateStr = getToday();
    gblAttendanceObject.startTimestamp = currentTimeStamp;
    gblAttendanceObject.startTimestampStr = getTimeInGMT();
    startTimer();
    captureCurrentLocation(success_geoLocCallbackStartWork, error_geoLocCallbackStartWork);
}
/** Save work time global object in store **/
function saveWorkTimeRecordInStore(wtRecord) {
    printMessage("#***# Inside saveWorkTimeRecordInStore #***#");
    var wtRecordStr = JSON.stringify(wtRecord);
    kony.store.setItem(WTConstant.WT_RECORD_IN_STORE, wtRecordStr);
}
/** Get work time global object from store **/
function getWorkTimeRecordFromStore() {
    printMessage("#***# Inside getWorkTimeRecordFromStore #***#");
    var wtRecordStr = kony.store.getItem(WTConstant.WT_RECORD_IN_STORE);
    var wtRecord = JSON.parse(wtRecordStr);
    return wtRecord;
}
/** End day button click **/
function onClickEndDay() {
    if (!isEmpty(gblAttendanceObject)) {
        showLoading("common.message.loading");
        switchButtonAccessibility(false, false);
        switchButtonAccessibility(true, false);
        executeWorkTimeLogic(false);
    }
}
/** End overtime button click **/
function onClickEndOvertime() {
    if (!isEmpty(gblAttendanceObject)) {
        showLoading("common.message.loading");
        switchButtonAccessibility(true, false);
        switchButtonAccessibility(false, false);
        executeWorkTimeLogic(true);
    }
}
/** Work time logic implemented here **/
function executeWorkTimeLogic(isOvertime) {
    stopTimer();
    Trace.stopService();
    var currentTimeStamp = getCurrentTimestamp();
    var currentTimeSplits = getTimeFromTimestamp(currentTimeStamp);
    gblAttendanceObject.endDateStr = getToday();
    gblAttendanceObject.endTimestamp = currentTimeStamp;
    gblAttendanceObject.endTimestampStr = getTimeInGMT(currentTimeStamp);
    captureCurrentLocation(success_geoLocCallbackStopWork, error_geoLocCallbackStopWork);
    if (isOvertime === true) {
        Home.lblStopOverTimeValue.text = currentTimeSplits.h + ":" + currentTimeSplits.m + ":" + currentTimeSplits.s;
    } else {
        Home.lblStopTimeValue.text = currentTimeSplits.h + ":" + currentTimeSplits.m + ":" + currentTimeSplits.s;
    }
}
/** Success callback - SQL query execution for fetch todays attendance records **/
function success_getTodaysWorkTimeRecords(resultset) {
    if (!isEmpty(gblTransactionFlags)) {
        gblTransactionFlags.todaysAttendace = true;
    }
    gblAttendanceObject = null;
    gblPrevAttendanceObject = null;
    var normalWorkTime = 0;
    var overtimeWorkTime = 0;
    var activeTime = 0;
    /** Base time means - the day starting time mid night 12'0 clk [Since it showing todays statistics, no need to include previous day work time] **/
    var todayDate = new Date();
    var todaysBaseDate = new Date(todayDate.getFullYear(), todayDate.getMonth(), todayDate.getDate(), 0, 0, 0, 1);
    var todaysBaseTime = todaysBaseDate.getTime();
    var startTimeVal = 0;
    if (!isEmpty(resultset) && resultset.length > 0) {
        var rowItem = null;
        for (var i = 0; i < resultset.length; i++) {
            rowItem = resultset[i];
            startTimeVal = (parseInt(rowItem.startTimestamp) < todaysBaseTime) ? todaysBaseTime : rowItem.startTimestamp;
            activeTime = timeDifferenceBtwTimes(rowItem.endTimestamp, startTimeVal);
            if (rowItem.isOverTime == "true") {
                overtimeWorkTime = overtimeWorkTime + activeTime;
            } else {
                normalWorkTime = normalWorkTime + activeTime;
            }
        }
    }
    var totalOverTime = (overtimeWorkTime > 0) ? getActualTime(overtimeWorkTime) : "00:00:00";
    var totalWorkTime = (normalWorkTime > 0) ? getActualTime(normalWorkTime) : "00:00:00";
    var currentTimeStamp = getCurrentTimestamp();
    var theDay = getDateFromTimestamp(currentTimeStamp);
    var monthI18nStr = getMonthNamei18n(theDay.mh);
    Home.lblDate.text = getI18nString("common.label.date") + " " + theDay.dy + "-" + getI18nString(monthI18nStr) + "-" + theDay.yr;
    Home.lblTotalWorkTime.text = getI18nString("popup.label.totalWorkTime") + " " + totalWorkTime;
    Home.lblTotalOvertime.text = getI18nString("popup.label.totalOvertime") + " " + totalOverTime;
    setFlxOverlayMsgOkButtonStyle(false);
    Home.flxPopupOverlay.setVisibility(true);
    kony.store.removeItem(WTConstant.WT_RECORD_IN_STORE);
}
/** Error callback - SQL query execution for fetch todays attendance records **/
function error_getTodaysWorkTimeRecords(error) {
    if (!isEmpty(gblTransactionFlags)) {
        gblTransactionFlags.todaysAttendace = true;
    }
    gblAttendanceObject = null;
    gblPrevAttendanceObject = null;
    kony.store.removeItem(WTConstant.WT_RECORD_IN_STORE);
    dismissLoading();
}
/** Enabling/Disabling overlay message popup ok button **/
function setFlxOverlayMsgOkButtonStyle(enabled) {
    Home.btnOk.setEnabled(enabled);
    Home.btnOk.skin = (enabled === true) ? btnNormalSkin : btnNormalDisabledSkin;
    Home.btnOk.focusSkin = (enabled === true) ? btnNormalSkin : btnNormalDisabledSkin;
}
/** To check any work time is currently active **/
function isWorkTimeActive() {
    var isActive = (!isEmpty(gblAttendanceObject) && gblAttendanceObject.endDateStr === "null") ? true : false;
    return isActive;
}
/** On click OK button action from work summary overlay popup **/
function onClickPopupOk() {
    Home.flxStartOverTimeInnerContainer.setEnabled(true);
    Home.flxStartOverTimeInnerContainer.skin = flxItemEnabledSkin;
    Home.flxStopOverTimeInnerContainer.setEnabled(false);
    Home.flxStopOverTimeInnerContainer.skin = flxItemDisabledSkin;
    Home.flxStartOverTimeInnerContainer.setEnabled(true);
    Home.flxStartOverTimeInnerContainer.skin = flxItemEnabledSkin;
    Home.flxStopOverTimeInnerContainer.setEnabled(false);
    Home.flxStopOverTimeInnerContainer.skin = flxItemDisabledSkin;
    Home.flxPopupOverlay.setVisibility(false);
}
/** To switch button accessibility based on current status **/
function switchButtonAccessibility(isOvertime, isActive) {
    if (isOvertime === false) {
        Home.flxStartWorkInnerContainer.setEnabled(!isActive);
        Home.flxStartWorkInnerContainer.skin = (isActive === true) ? flxItemDisabledSkin : flxItemEnabledSkin;
        Home.flxStopWorkInnerContainer.setEnabled(isActive);
        Home.flxStopWorkInnerContainer.skin = (isActive === true) ? flxItemEnabledSkin : flxItemDisabledSkin;
    } else {
        Home.flxStartOverTimeInnerContainer.setEnabled(!isActive);
        Home.flxStartOverTimeInnerContainer.skin = (isActive === true) ? flxItemDisabledSkin : flxItemEnabledSkin;
        Home.flxStopOverTimeInnerContainer.setEnabled(isActive);
        Home.flxStopOverTimeInnerContainer.skin = (isActive === true) ? flxItemEnabledSkin : flxItemDisabledSkin;
    }
}
/** Timer start call - Work time runner **/
function startTimer() {
    try {
        stopTimer();
        var isPreWorkTimeStillActive = isWorkTimeActive();
        if (isPreWorkTimeStillActive === true) {
            var isOvertimeVal = gblAttendanceObject.isOverTime;
            Home.flxTimerContainer.skin = (isOvertimeVal === true) ? flxRedBgSkin : flxGreenBgSkin;
            kony.timer.schedule("WorkTimerScheduler", showWorkTimerUpdates, 1, true);
            Home.flxTimerContainer.setVisibility(true);
        }
    } catch (error) {
        printMessage("Error in start timer: " + error);
    }
}
/** Timer stop call - Work time runner **/
function stopTimer() {
    try {
        kony.timer.cancel("WorkTimerScheduler");
        Home.flxTimerContainer.setVisibility(false);
        Home.lblTimer.text = "";
    } catch (error) {
        printMessage("Error in stop timer: " + error);
    }
}
/** Timer updates - Work timer refresher **/
function showWorkTimerUpdates() {
    var timeDiff = timeDifferenceWithCurrentTime(gblAttendanceObject.startTimestamp);
    Home.lblTimer.text = getActualTime(timeDiff);
}
/** Timer start call - To wait the transactions to be completed **/
function startTransactionWaitTimer() {
    try {
        stopTransactionWaitTimer();
        kony.timer.schedule("TransactionWaitScheduler", checkTransactionStatus, 0.1, true);
    } catch (error) {
        printMessage("Error in startTransactionWaitTimer: " + error);
    }
}
/** Timer stop call - To wait the transactions to be completed **/
function stopTransactionWaitTimer() {
    try {
        kony.timer.cancel("TransactionWaitScheduler");
    } catch (error) {
        printMessage("Error in stopTransactionWaitTimer: " + error);
    }
}
/** Timer method - To check whether transactions completed  **/
function checkTransactionStatus() {
    if (!isEmpty(gblTransactionFlags)) {
        if (gblTransactionFlags.trace === true) {
            gblTransactionFlags.trace = null;
            createAttendanceRecordInDB(gblAttendanceObject);
        }
        if (gblTransactionFlags.attendance === true) {
            gblTransactionFlags.attendance = null;
            getTodaysWorkTimeRecords();
        }
        if (gblTransactionFlags.todaysAttendace === true) {
            gblTransactionFlags.todaysAttendace = null;
            getAllTempTracesTransaction();
        }
        if (gblTransactionFlags.getAllTrace === true) {
            gblTransactionFlags.getAllTrace = null;
            moveTempTracesRecords();
        }
        if (gblTransactionFlags.moveAllTrace === true) {
            gblTransactionFlags.moveAllTrace = null;
            deleteAllTempTracesTransaction();
        }
        if (gblTransactionFlags.deleteAllTemp === true) {
            gblTransactionFlags.deleteAllTemp = null;
            gblShowCustomProgress = false;
            initDataSync();
        }
        if (gblTransactionFlags.trace === null && gblTransactionFlags.attendance === null && gblTransactionFlags.todaysAttendace === null && gblTransactionFlags.getAllTrace === null && gblTransactionFlags.moveAllTrace === null && gblTransactionFlags.deleteAllTemp === null) {
            gblTransactionFlags = null;
        }
    } else {
        dismissLoading();
        stopTransactionWaitTimer();
    }
}
/** To find out the time difference between given time and current time **/
function timeDifferenceWithCurrentTime(pastTimeStamp) {
    var currentTimeStamp = getCurrentTimestamp();
    var timeDiff = timeDifferenceBtwTimes(currentTimeStamp, parseInt(pastTimeStamp));
    return timeDiff;
}
/** To find out the time difference between two given times **/
function timeDifferenceBtwTimes(newTimeStamp, oldTimeStamp) {
    var timeDiff = parseInt(newTimeStamp) - parseInt(oldTimeStamp);
    return timeDiff;
}
/** To format time in hour minutes and seconds style **/
function getActualTime(timeStampValue) {
    var timeDiffHours = Math.floor(parseInt(timeStampValue) / 1000 / 60 / 60);
    var timeDiff1 = parseInt(timeStampValue) - (timeDiffHours * 1000 * 60 * 60);
    var timeDiffMinutes = Math.floor(timeDiff1 / 1000 / 60);
    var timeDiff2 = timeDiff1 - (timeDiffMinutes * 1000 * 60);
    var timeDiffSeconds = Math.floor(timeDiff2 / 1000);
    timeDiffHours = (timeDiffHours < 10) ? ("0" + timeDiffHours) : timeDiffHours;
    timeDiffMinutes = (timeDiffMinutes < 10) ? ("0" + timeDiffMinutes) : timeDiffMinutes;
    timeDiffSeconds = (timeDiffSeconds < 10) ? ("0" + timeDiffSeconds) : timeDiffSeconds;
    var totalTimeDiffInFormat = timeDiffHours + ":" + timeDiffMinutes + ":" + timeDiffSeconds;
    return totalTimeDiffInFormat;
}
/** To find out current timestamp **/
function getCurrentTimestamp() {
    var currentDate = new Date();
    var currentTimestamp = currentDate.getTime();
    return currentTimestamp;
}
/** To find out the time from current timestamp **/
function getTimeFromTimestamp(timeInMs) {
    var givenDate = new Date(parseInt(timeInMs));
    var timeSplits = {};
    var hrs = givenDate.getHours();
    var mnts = givenDate.getMinutes();
    var scnds = givenDate.getSeconds();
    hrs = (hrs < 10) ? ("0" + hrs.toString()) : hrs;
    mnts = (mnts < 10) ? ("0" + mnts.toString()) : mnts;
    scnds = (scnds < 10) ? ("0" + scnds.toString()) : scnds;
    timeSplits.h = hrs;
    timeSplits.m = mnts;
    timeSplits.s = scnds;
    return timeSplits;
}
/** To find out the date from current timestamp **/
function getDateFromTimestamp(timeInMs) {
    var givenDate = new Date(parseInt(timeInMs));
    var dateSplits = {};
    var d = givenDate.getDate();
    var m = givenDate.getMonth() + 1;
    var y = givenDate.getFullYear();
    d = (d < 10) ? ("0" + d) : d;
    m = (m < 10) ? ("0" + m) : m;
    dateSplits.yr = y;
    dateSplits.mh = m;
    dateSplits.dy = d;
    return dateSplits;
}
/** To find out current date as string **/
function getToday() {
    var today = new Date();
    var d = today.getDate();
    var m = today.getMonth() + 1;
    var y = today.getFullYear();
    d = (d < 10) ? ("0" + d) : d;
    m = (m < 10) ? ("0" + m) : m;
    var todayStr = d + "-" + m + "-" + y;
    return todayStr;
}
/** change from UTC format to HH:MM:SS GMT format*/
function getTimeInGMT() {
    var mDateObj = new Date();
    // var timeStr 	= mDateObj.getUTCHours()+":"+mDateObj.getUTCMinutes()+":"+mDateObj.getUTCSeconds();
    var gmthrs = (mDateObj.getUTCHours() < 10) ? ("0" + mDateObj.getUTCHours().toString()) : mDateObj.getUTCHours();
    var gmtmnts = (mDateObj.getUTCMinutes() < 10) ? ("0" + mDateObj.getUTCMinutes()) : mDateObj.getUTCMinutes();
    var gmtscnds = (mDateObj.getUTCSeconds() < 10) ? ("0" + mDateObj.getUTCSeconds().toString()) : mDateObj.getUTCSeconds();
    var timeStr = gmthrs + ":" + gmtmnts + ":" + gmtscnds;
    return timeStr;
}