/**********************************************************************
 *	Author  : RI - CTS363601 
 *	Purpose : Module to refer all global variables and methods
 ***********************************************************************/
/** Application - Pre app init call **/
function onPreAppInit() {
    getCurrentDevLocale();
    setApplicationBehaviors();
}
/** Application - Post app init call **/
function onPostAppInit() {
    printMessage("#***# Inside Post App Init #***#");
    initializeGlobals();
    gblDeviceInfo = kony.os.deviceInfo();
}
/** Initializing global variables **/
function initializeGlobals() {
    printMessage("#***# Inside InitializeGlobals #***#");
    gblEmployeeObject = null;
    gblAttendanceObject = null;
    gblPrevAttendanceObject = null;
    gblSegEmailLastSelIndex = 0;
    gblMFInputs = {};
    gblTouchMovePrevX = 0;
    gblIsSyncInProgress = false;
    gblIsTraceOpen = false;
    gblTraceDataArray = null;
    gblShowCustomProgress = false;
    gblTempTraces = [];
    gblTransactionFlags = null;
    selectedEmail = "";
}
/** Reset global variables **/
function resetGlobals() {
    gblEmployeeObject = null;
    gblAttendanceObject = null;
    gblPrevAttendanceObject = null;
    gblSegEmailLastSelIndex = 0;
    gblMFInputs = {};
    gblIsSyncInProgress = false;
    gblIsTraceOpen = false;
    gblTouchMovePrevX = 0;
    gblTraceDataArray = null;
    gblShowCustomProgress = false;
    gblTempTraces = [];
    gblTransactionFlags = null;
}
/** Device back button action **/
function onDeviceBackButtonCall() {
    var frmObject = kony.application.getCurrentForm();
    if (gblIsSyncInProgress === true) {
        showConfirmPopup("common.message.stopSync", ActionConfirmSyncStopYesBtn, ActionConfirmPopupNoBtn);
    } else if (frmObject.id == WTConstant.FORM_LOGIN) {
        exitApplication();
    } else if (frmObject.id == WTConstant.FORM_HOME) {
        var isActive = isWorkTimeActive();
        if (isActive === true) {
            showConfirmPopup("common.message.confirmExitWithActiveTracking", ActionConfirmPopupYesBtn, ActionConfirmPopupNoBtn);
        } else {
            showConfirmPopup("common.message.confirmExit", ActionConfirmPopupYesBtn, ActionConfirmPopupNoBtn);
        }
    }
}
// storing business codes and branches
var businessCodes = [
    ["Select", "Select Business"],
    ["Pest", "Pest"],
    ["Hygiene", "Specialist Hygiene"]
];
var pestbranches = [
    ["Select", "Select Branch"],
    ["002 E-London", "002 E-London"],
    ["003 Essex", "003 Essex"],
    ["004 Home Counties", "004 Home Counties"],
    ["006 Fumigation", "006 Fumigation"],
    ["012 Watford", "012 Watford"],
    ["016 Uxbridge", "016 Uxbridge"],
    ["044 East Anglia", "044 East Anglia"],
    ["045 MK & Cambridge", "045 MK & Cambridge"],
    ["014 South Coast", "014 South Coast"],
    ["017 Oxford", "017 Oxford"],
    ["027 Devon Cornwall & Channel", "027 Devon Cornwall & Channel"],
    ["028 Three Counties", "028 Three Counties"],
    ["029 Bristol and Wiltshire", "029 Bristol and Wiltshire"],
    ["033 S&W-Wales", "033 S&W-Wales"],
    ["008 Kent", "008 Kent"],
    ["009 Sussex", "009 Sussex"],
    ["010 SW-Surrey London", "010 SW-Surrey London"],
    ["015 Surrey", "015 Surrey"],
    ["020 London Underground", "020 London Underground"],
    ["021 Mayfair", "021 Mayfair"],
    ["022 London West End", "022 London West End"],
    ["023 London The City", "023 London The City"],
    ["024 Fulham and Kensington", "024 Fulham and Kensington"],
    ["046 Leicester & Peterborough", "046 Leicester & Peterborough"],
    ["058 Hull", "058 Hull"],
    ["059 Leeds & Bradford", "059 Leeds & Bradford"],
    ["060 S&W-Yorkshire", "060 S&W-Yorkshire"],
    ["061 Teesside", "061 Teesside"],
    ["062 Newcastle", "062 Newcastle"],
    ["063 Leeds CC/Yorks Water", "063 Leeds CC/Yorks Water"],
    ["037 W-Midlands Central", "037 W-Midlands Central"],
    ["038 W-Midlands North", "038 W-Midlands North"],
    ["040 W-Midlands South", "040 W-Midlands South"],
    ["051 N-Wales", " Wirral & IoM", "051 N-Wales", " Wirral & IoM"],
    ["052 Liverpool", "052 Liverpool"],
    ["053 Manchester", "053 Manchester"],
    ["055 Cumbria", "055 Cumbria"],
    ["065 N-Glasgow", "065 N-Glasgow"],
    ["072 Edinburgh", "072 Edinburgh"],
    ["073 Aberdeen", "073 Aberdeen"],
    ["067 Belfast", "067 Belfast"],
    ["097 Hit Squad", "097 Hit Squad"]
];
var hygienebranches = [
    ["Select", "Select Branch"],
    ["180 Birmingham", "180 Birmingham"],
    ["181 Bristol", "181 Bristol"],
    ["182 East Kilbride", "182 East Kilbride"],
    ["183 Harlow", "183 Harlow"],
    ["184 Haydock", "184 Haydock"],
    ["185 Leeds", "185 Leeds"],
    ["186 Newcastle", "186 Newcastle"]
];
/** To set some of the application behaviors **/
function setApplicationBehaviors() {
    printMessage("#***# Inside setApplicationBehaviors #***#");
    var inputParamTable = {};
    inputParamTable.defaultIndicatorColor = "#ffe6e6";
    kony.application.setApplicationBehaviors(inputParamTable);
    var callbackParamTable = {};
    callbackParamTable.onbackground = ActionApplicationWentBackground;
    callbackParamTable.onforeground = ActionApplicationBecomesForeground;
    kony.application.setApplicationCallbacks(callbackParamTable);
}
/** Callback - App running in foreground **/
function onAppBecomesForeground() {
    printMessage("#***# Inside onAppBecomesForeground #***#");
    var frmObj = kony.application.getCurrentForm();
    if (frmObj.id == WTConstant.FORM_HOME) {
        startTimer();
        refreshSettingsUI();
    } else if (frmObj.id == WTConstant.FORM_CONTENT) {
        refreshSettingsUI();
    }
}
/** Callback - App running in background **/
function onAppWentBackground() {
    printMessage("#***# Inside onAppWentBackground #***#");
    stopTimer();
}
/** To fetch current device locale  **/
function getCurrentDevLocale() {
    var curDevLocale = kony.i18n.getCurrentDeviceLocale();
    var language = curDevLocale.language;
    var country = curDevLocale.country;
    var devLocale = (!isEmpty(language) && !isEmpty(country)) ? (language + "_" + toUpperCase(country)) : WTConstant.DEF_LOCALE;
}
/** To change current device locale  **/
function changeDeviceLocale(selLocale) {
    if (kony.i18n.isLocaleSupportedByDevice(selLocale) && kony.i18n.isResourceBundlePresent(selLocale)) {
        kony.i18n.setCurrentLocaleAsync(selLocale, success_setLocaleCallback, failure_setLocaleCallback, {});
    }
}
/** Success callback - change device locale  **/
function success_setLocaleCallback() {
    printMessage("Set locale action succeeded");
}
/** Failure callback - change device locale  **/
function failure_setLocaleCallback() {
    printMessage("Set locale action failed");
}
/** To set global employee object **/
function setGblUserObject(employeeInfo) {
    gblEmployeeObject = new Employee();
    gblEmployeeObject.email = (isEmpty(employeeInfo.email) || employeeInfo.email == "null") ? gblMFInputs.email : employeeInfo.email;
    gblEmployeeObject.firstName = (!isEmpty(employeeInfo.firstName)) ? employeeInfo.firstName : gblEmployeeObject.firstName;
    gblEmployeeObject.lastName = (!isEmpty(employeeInfo.lastName)) ? employeeInfo.lastName : gblEmployeeObject.lastName;
    gblEmployeeObject.countryCode = (!isEmpty(employeeInfo.countryCode)) ? employeeInfo.countryCode : gblEmployeeObject.countryCode;
    gblEmployeeObject.businessCode = (!isEmpty(employeeInfo.businessCode)) ? employeeInfo.businessCode : gblEmployeeObject.businessCode;
    gblEmployeeObject.branchNumber = (!isEmpty(employeeInfo.branchNumber)) ? employeeInfo.branchNumber : gblEmployeeObject.branchNumber;
    if (!isEmpty(employeeInfo.branchNumber)) {
        gblEmployeeObject.setEmployeeCode(employeeInfo.employeeCode);
    }
}

function getMonthNamei18n(monthIndex) {
    var monthShortNamesi18n = {
        "01": "common.month.jan",
        "02": "common.month.feb",
        "03": "common.month.mar",
        "04": "common.month.apr",
        "05": "common.month.may",
        "06": "common.month.jun",
        "07": "common.month.jul",
        "08": "common.month.aug",
        "09": "common.month.sep",
        "10": "common.month.oct",
        "11": "common.month.nov",
        "12": "common.month.dec"
    };
    return monthShortNamesi18n[monthIndex];
}