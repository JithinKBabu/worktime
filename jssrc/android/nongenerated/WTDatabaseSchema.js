/**********************************************************************
 *	Author  : RI - CTS363601 
 *	Purpose : Module to refer all the offline database and sync methods
 ***********************************************************************/
// kony.db.changeVersion("1.0","1.1",transactioCall,errorCall,sucessCall);
/** Method - Create attendance and trace records in database tables **/
function createTraceRecordInDB(recordObj, isTraceOpen) {
    gblTraceDataArray = [];
    var traceTimestamp = (isTraceOpen === true) ? recordObj.startTimestamp : recordObj.endTimestamp;
    var traceTimestampStr = (isTraceOpen === true) ? recordObj.startDateStr + " " + recordObj.startTimestampStr : recordObj.endDateStr + " " + recordObj.endTimestampStr;
    var traceLocLatitude = (isTraceOpen === true) ? recordObj.startLocLatitude : recordObj.endLocLatitude;
    var traceLocLongitude = (isTraceOpen === true) ? recordObj.startLocLongitude : recordObj.endLocLongitude;
    var traceComment = (isTraceOpen === true) ? recordObj.startLocComment : recordObj.endLocComment;
    gblTraceDataArray.push(gblEmployeeObject.employeeCode);
    gblTraceDataArray.push(gblEmployeeObject.email);
    gblTraceDataArray.push(gblEmployeeObject.branchNumber);
    gblTraceDataArray.push(traceTimestamp);
    gblTraceDataArray.push(traceTimestampStr);
    gblTraceDataArray.push(traceLocLatitude);
    gblTraceDataArray.push(traceLocLongitude);
    gblTraceDataArray.push(isTraceOpen);
    gblTraceDataArray.push(traceComment);
    kony.print("gblTraceDataArray::" + JSON.stringify(gblTraceDataArray));
    gblIsTraceOpen = isTraceOpen;
    insertTempTraceTransaction();
}
/** Method - Create attendance record in database table **/
function createAttendanceRecordInDB(recordObj) {
    var attendanceDetails = {};
    attendanceDetails.employeeCode = gblEmployeeObject.employeeCode;
    attendanceDetails.email = gblEmployeeObject.email;
    attendanceDetails.branchNumber = gblEmployeeObject.branchNumber;
    attendanceDetails.startDateStr = recordObj.startDateStr;
    attendanceDetails.startTimestamp = recordObj.startTimestamp;
    attendanceDetails.startLocLatitude = recordObj.startLocLatitude;
    attendanceDetails.startLocLongitude = recordObj.startLocLongitude;
    attendanceDetails.endDateStr = recordObj.endDateStr;
    attendanceDetails.endTimestamp = recordObj.endTimestamp;
    attendanceDetails.endLocLatitude = recordObj.endLocLatitude;
    attendanceDetails.endLocLongitude = recordObj.endLocLongitude;
    attendanceDetails.isOverTime = recordObj.isOverTime;
    attendanceDetails.startLocComment = recordObj.startLocComment;
    attendanceDetails.endLocComment = recordObj.endLocComment;
    attendanceDetails.startTimestampStr = recordObj.startTimestampStr;
    attendanceDetails.endTimestampStr = recordObj.endTimestampStr;
    kony.print("attendanceDetails::" + JSON.stringify(attendanceDetails));
    WTSync.wtattendance.create(attendanceDetails, success_createAttendanceRecordInDB, error_createAttendanceRecordInDB, true);
}
/** Method - Fetch todays work time records **/
function getTodaysWorkTimeRecords() {
    var todayStr = getToday();
    var conditionalStmt = "WHERE startDateStr = '" + todayStr + "' OR endDateStr = '" + todayStr + "'";
    WTSync.wtattendance.find(conditionalStmt, success_getTodaysWorkTimeRecords, error_getTodaysWorkTimeRecords);
}
/** Method - Create attendance and trace records in database tables **/
function moveTempTracesRecords() {
    if (!isEmpty(gblTempTraces) && gblTempTraces.length > 0) {
        WTSync.wtemployeetrace.createAll(gblTempTraces, success_moveTempTracesRecords, error_moveTempTracesRecords, true);
    } else {
        if (!isEmpty(gblTransactionFlags)) {
            gblTransactionFlags.moveAllTrace = true;
        }
    }
}
/********************************************* Custom DB operations *************************************************/
/** To open DB handler **/
function openLocalDatabase() {
    var dbname = kony.sync.getDBName();
    var dbObjectId = kony.sync.getConnectionOnly(dbname, dbname);
    return dbObjectId;
}
/** Get details from employee details table - Transaction **/
function getEmployeeDetailsTransaction() {
    printMessage("#***# Inside Transaction - Get Employee Details #***#");
    var dbname = kony.sync.getDBName();
    var sqlStatement = "SELECT employeeCode, email,branchNumber FROM employeedetails LIMIT 0,1 ";
    kony.sync.single_select_execute(dbname, sqlStatement, null, success_getEmployeeDetails, error_getEmployeeDetails);
}
/** Insert details in employee details table - Transaction **/
function insertEmployeeDetailsTransaction() {
    var dbObjectId = openLocalDatabase();
    kony.db.transaction(dbObjectId, insertEmployeeDetails, error_commonCallback, success_commonCallback);
}
/** Delete all employee details from temp employee details table - Transaction **/
function deleteEmployeeDetailsTransaction() {
    var dbObjectId = openLocalDatabase();
    kony.db.transaction(dbObjectId, deleteEmployeeDetails, error_deleteEmployeeDetails, success_deleteEmployeeDetails);
}
/** Insert trace record in temp trace table - Transaction **/
function insertTempTraceTransaction() {
    var dbObjectId = openLocalDatabase();
    kony.db.transaction(dbObjectId, insertTempTrace, error_createTraceRecord, success_createTraceRecord);
}
/** Get trace records from temp trace table - Transaction **/
function getAllTempTracesTransaction() {
    var dbname = kony.sync.getDBName();
    var sqlStatement = "SELECT employeeCode, email,branchNumber, traceTimestamp,traceTimestampStr, traceLocLatitude, traceLocLongitude, traceIsOpen, traceComment FROM emptracetemp ";
    kony.sync.single_select_execute(dbname, sqlStatement, null, success_getAllTracesRecord, error_getAllTracesRecord);
}
/** Delete all trace records from temp trace table - Transaction **/
function deleteAllTempTracesTransaction() {
    var dbObjectId = openLocalDatabase();
    kony.db.transaction(dbObjectId, deleteAllTempTrace, error_deleteAllTempTraces, success_deleteAllTempTraces);
}
/** Insert employee details in employeedetails Table **/
function insertEmployeeDetails(dbId) {
    var sqlStatement = "INSERT INTO employeedetails ( employeeCode, email, firstName, lastName, countryCode, businessCode, branchNumber) VALUES (?, ?, ?, ?, ?, ?, ?)";
    var inputData = [];
    inputData.push(gblEmployeeObject.employeeCode);
    inputData.push(gblEmployeeObject.email);
    inputData.push(gblEmployeeObject.firstName);
    inputData.push(gblEmployeeObject.lastName);
    inputData.push(gblEmployeeObject.countryCode);
    inputData.push(gblEmployeeObject.businessCode);
    inputData.push(gblEmployeeObject.branchNumber);
    kony.db.executeSql(dbId, sqlStatement, inputData, success_transCommonCallback, error_transCommonCallback);
}
/** Delete employee details from employeedetails Table **/
function deleteEmployeeDetails(dbId) {
    var sqlStatement = "DELETE FROM employeedetails";
    kony.db.executeSql(dbId, sqlStatement, null, success_transCommonCallback, error_transCommonCallback);
}
/** Insert Trace details in TraceTemp Table **/
function insertTempTrace(dbId) {
    var sqlStatement = "INSERT INTO emptracetemp (employeeCode, email, branchNumber, traceTimestamp, traceTimestampStr, traceLocLatitude, traceLocLongitude, traceIsOpen, traceComment) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
    kony.db.executeSql(dbId, sqlStatement, gblTraceDataArray, success_transCommonCallback, error_transCommonCallback);
}
/** Delete Trace details from TraceTemp Table **/
function deleteAllTempTrace(dbId) {
    var sqlStatement = "DELETE FROM emptracetemp";
    kony.db.executeSql(dbId, sqlStatement, null, success_transCommonCallback, error_transCommonCallback);
}
/********************************************* Transaction Callbacks *************************************************/
/** Success callback - SQL query execution - insert attendance records **/
function success_createAttendanceRecordInDB() {
    if (!isEmpty(gblTransactionFlags)) {
        gblTransactionFlags.attendance = true;
    }
}
/** Error callback - SQL query execution - insert attendance records **/
function error_createAttendanceRecordInDB(error) {
    if (!isEmpty(gblTransactionFlags)) {
        gblTransactionFlags.attendance = true;
    }
}
/** Success callback - inserting records in wtemployeetrace **/
function success_moveTempTracesRecords() {
    if (!isEmpty(gblTransactionFlags)) {
        gblTransactionFlags.moveAllTrace = true;
    }
}
/** Error callback -  inserting records in wtemployeetrace **/
function error_moveTempTracesRecords(error) {
    if (!isEmpty(gblTransactionFlags)) {
        gblTransactionFlags.moveAllTrace = true;
    }
}
/** Success callback - SQL query execution - insert trace records **/
function success_createTraceRecord() {
    if (gblIsTraceOpen === true) {
        Trace.getLocation(); // To start tracing service (FFI invocation) 
        dismissLoading();
    } else {
        if (!isEmpty(gblTransactionFlags)) {
            gblTransactionFlags.trace = true;
        }
    }
}
/** Error callback - SQL query execution - insert trace records **/
function error_createTraceRecord(error) {
    if (gblIsTraceOpen === false) {
        if (!isEmpty(gblTransactionFlags)) {
            gblTransactionFlags.trace = true;
        }
    }
    dismissLoading();
}
/** Success callback - SQL query execution - get all trace records **/
function success_getAllTracesRecord(result) {
    gblTempTraces = [];
    if (result !== null && result.length > 0) {
        var rowItem = null;
        var traceDetails = null;
        for (var i = 0; i < result.length; i++) {
            rowItem = result[i];
            traceDetails = {};
            traceDetails.employeeCode = rowItem.employeeCode;
            traceDetails.email = rowItem.email;
            traceDetails.branchNumber = rowItem.branchNumber;
            traceDetails.traceTimestamp = rowItem.traceTimestamp;
            traceDetails.traceLocLatitude = rowItem.traceLocLatitude;
            traceDetails.traceLocLongitude = rowItem.traceLocLongitude;
            traceDetails.traceIsOpen = rowItem.traceIsOpen;
            traceDetails.traceComment = rowItem.traceComment;
            traceDetails.traceTimestampStr = rowItem.traceTimestampStr;
            gblTempTraces.push(traceDetails);
        }
    }
    if (!isEmpty(gblTransactionFlags)) {
        gblTransactionFlags.getAllTrace = true;
    }
}
/** Error callback - SQL query execution - get all trace records **/
function error_getAllTracesRecord(error) {
    if (!isEmpty(gblTransactionFlags)) {
        gblTransactionFlags.getAllTrace = true;
    }
}
/** Success callback - SQL query execution - delete trace records **/
function success_deleteAllTempTraces() {
    if (!isEmpty(gblTransactionFlags)) {
        gblTransactionFlags.deleteAllTemp = true;
    }
}
/** Error callback - SQL query execution - delete trace records **/
function error_deleteAllTempTraces(error) {
    if (!isEmpty(gblTransactionFlags)) {
        gblTransactionFlags.deleteAllTemp = true;
    }
}
/** Transaction common success callback - For sql statement execution **/
function success_transCommonCallback(transactionId, result) {}
/** Transaction common error callback - For sql statement execution **/
function error_transCommonCallback(error) {}
/** Common success callback - For sql statement execution **/
function success_commonCallback() {}
/** Common error callback - For sql statement execution **/
function error_commonCallback(error) {}