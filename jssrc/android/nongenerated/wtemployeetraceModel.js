//****************Sync Version:MobileFabricInstaller-QA-7.2.0_v201610191639_r117*******************
// ****************Generated On Fri Nov 25 12:45:01 UTC 2016wtemployeetrace*******************
// **********************************Start wtemployeetrace's helper methods************************
if (typeof(kony) === "undefined") {
    kony = {};
}
if (typeof(kony.sync) === "undefined") {
    kony.sync = {};
}
if (typeof(kony.sync.log) === "undefined") {
    kony.sync.log = {};
}
if (typeof(sync) === "undefined") {
    sync = {};
}
if (typeof(sync.log) === "undefined") {
    sync.log = {};
}
if (typeof(WTSync) === "undefined") {
    WTSync = {};
}
/************************************************************************************
 * Creates new wtemployeetrace
 *************************************************************************************/
WTSync.wtemployeetrace = function() {
    this.traceId = null;
    this.employeeCode = null;
    this.email = null;
    this.traceTimestamp = null;
    this.traceLocLatitude = null;
    this.traceLocLongitude = null;
    this.traceIsOpen = null;
    this.traceComment = null;
    this.isDeleted = null;
    this.lastModifiedDate = null;
    this.traceTimestampStr = null;
    this.branchNumber = null;
    this.markForUpload = true;
};
WTSync.wtemployeetrace.prototype = {
    get traceId() {
        return this._traceId;
    },
    set traceId(val) {
        if (!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidNumberType(val)) {
            sync.log.error("Invalid data type for the attribute traceId in wtemployeetrace.\nExpected:\"integer\"\nActual:\"" + kony.type(val) + "\"");
        }
        this._traceId = val;
    },
    get employeeCode() {
        return this._employeeCode;
    },
    set employeeCode(val) {
        this._employeeCode = val;
    },
    get email() {
        return this._email;
    },
    set email(val) {
        this._email = val;
    },
    get traceTimestamp() {
        return this._traceTimestamp;
    },
    set traceTimestamp(val) {
        this._traceTimestamp = val;
    },
    get traceLocLatitude() {
        return this._traceLocLatitude;
    },
    set traceLocLatitude(val) {
        if (!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidNumberType(val)) {
            sync.log.error("Invalid data type for the attribute traceLocLatitude in wtemployeetrace.\nExpected:\"double\"\nActual:\"" + kony.type(val) + "\"");
        }
        this._traceLocLatitude = val;
    },
    get traceLocLongitude() {
        return this._traceLocLongitude;
    },
    set traceLocLongitude(val) {
        if (!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidNumberType(val)) {
            sync.log.error("Invalid data type for the attribute traceLocLongitude in wtemployeetrace.\nExpected:\"double\"\nActual:\"" + kony.type(val) + "\"");
        }
        this._traceLocLongitude = val;
    },
    get traceIsOpen() {
        return kony.sync.getBoolean(this._traceIsOpen) + "";
    },
    set traceIsOpen(val) {
        if (!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidBooleanType(val)) {
            sync.log.error("Invalid data type for the attribute traceIsOpen in wtemployeetrace.\nExpected:\"boolean\"\nActual:\"" + kony.type(val) + "\"");
        }
        this._traceIsOpen = val;
    },
    get traceComment() {
        return this._traceComment;
    },
    set traceComment(val) {
        this._traceComment = val;
    },
    get isDeleted() {
        return kony.sync.getBoolean(this._isDeleted) + "";
    },
    set isDeleted(val) {
        if (!kony.sync.isEmptyString(val) && !kony.sync.isNull(val) && !kony.sync.isValidBooleanType(val)) {
            sync.log.error("Invalid data type for the attribute isDeleted in wtemployeetrace.\nExpected:\"boolean\"\nActual:\"" + kony.type(val) + "\"");
        }
        this._isDeleted = val;
    },
    get lastModifiedDate() {
        return this._lastModifiedDate;
    },
    set lastModifiedDate(val) {
        this._lastModifiedDate = val;
    },
    get traceTimestampStr() {
        return this._traceTimestampStr;
    },
    set traceTimestampStr(val) {
        this._traceTimestampStr = val;
    },
    get branchNumber() {
        return this._branchNumber;
    },
    set branchNumber(val) {
        this._branchNumber = val;
    },
};
/************************************************************************************
 * Retrieves all instances of wtemployeetrace SyncObject present in local database with
 * given limit and offset where limit indicates the number of records to be retrieved
 * and offset indicates number of rows to be ignored before returning the records.
 * e.g. var orderByMap = []
 * orderByMap[0] = {};
 * orderByMap[0].key = "traceId";
 * orderByMap[0].sortType ="desc";
 * orderByMap[1] = {};
 * orderByMap[1].key = "employeeCode";
 * orderByMap[1].sortType ="asc";
 * var limit = 20;
 * var offset = 5;
 * WTSync.wtemployeetrace.getAll(successcallback,errorcallback, orderByMap, limit, offset)
 *************************************************************************************/
WTSync.wtemployeetrace.getAll = function(successcallback, errorcallback, orderByMap, limit, offset) {
    sync.log.trace("Entering WTSync.wtemployeetrace.getAll->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = WTSync.wtemployeetrace.getTableName();
    orderByMap = kony.sync.formOrderByClause("wtemployeetrace", orderByMap);
    var query = kony.sync.qb_createQuery();
    kony.sync.qb_select(query, null);
    kony.sync.qb_from(query, tbname);
    kony.sync.qb_orderBy(query, orderByMap);
    kony.sync.qb_limitOffset(query, limit, offset);
    var query_compile = kony.sync.qb_compile(query);
    var sql = query_compile[0];
    var params = query_compile[1];

    function mySuccCallback(res) {
        sync.log.trace("Entering WTSync.wtemployeetrace.getAll->successcallback");
        successcallback(WTSync.wtemployeetrace.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
    }
    kony.sync.single_select_execute(dbname, sql, params, mySuccCallback, errorcallback);
};
/************************************************************************************
 * Returns number of wtemployeetrace present in local database.
 *************************************************************************************/
WTSync.wtemployeetrace.getAllCount = function(successcallback, errorcallback) {
    sync.log.trace("Entering WTSync.wtemployeetrace.getAllCount function");
    WTSync.wtemployeetrace.getCount("", successcallback, errorcallback);
};
/************************************************************************************
 * Returns number of wtemployeetrace using where clause in the local Database
 *************************************************************************************/
WTSync.wtemployeetrace.getCount = function(wcs, successcallback, errorcallback) {
    sync.log.trace("Entering WTSync.wtemployeetrace.getCount->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "WTSync.wtemployeetrace.getCount", "getCount", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = WTSync.wtemployeetrace.getTableName();
    wcs = kony.sync.validateWhereClause(wcs);
    var sql = "select count(*) from \"" + tbname + "\" " + wcs;
    kony.sync.single_execute_sql(dbname, sql, null, mySuccCallback, errorcallback);

    function mySuccCallback(res) {
        sync.log.trace("Entering WTSync.wtemployeetrace.getCount->successcallback");
        if (null !== res) {
            var count = null;
            count = res["count(*)"];
            kony.sync.verifyAndCallClosure(successcallback, {
                count: count
            });
        } else {
            sync.log.error("Some error occured while getting the count");
        }
    }
};
/************************************************************************************
 * Creates a new instance of wtemployeetrace in the local Database. The new record will 
 * be merged with the enterprise datasource in the next Sync.
 *************************************************************************************/
WTSync.wtemployeetrace.prototype.create = function(successcallback, errorcallback) {
    sync.log.trace("Entering  WTSync.wtemployeetrace.prototype.create function");
    var valuestable = this.getValuesTable(true);
    WTSync.wtemployeetrace.create(valuestable, successcallback, errorcallback, this.markForUpload);
};
WTSync.wtemployeetrace.create = function(valuestable, successcallback, errorcallback, markForUpload) {
    sync.log.trace("Entering  WTSync.wtemployeetrace.create->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "WTSync.wtemployeetrace.create", "create", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = WTSync.wtemployeetrace.getTableName();
    markForUpload = kony.sync.getUploadStatus(markForUpload);
    if (kony.sync.attributeValidation(valuestable, "wtemployeetrace", errorcallback, true) === false) {
        return;
    }

    function executeSuccess() {
        sync.log.trace("Entering  WTSync.wtemployeetrace.create->success callback");
        kony.sync.single_insert_execute(dbname, tbname, valuestable, successcallback, errorcallback, markForUpload);
    }
    if (kony.sync.enableORMValidations) {
        var relationshipMap = {};
        relationshipMap = WTSync.wtemployeetrace.getRelationshipMap(relationshipMap, valuestable);
        kony.sync.checkIntegrity(dbname, relationshipMap, executeSuccess, errorcallback);
    } else {
        kony.sync.single_insert_execute(dbname, tbname, valuestable, successcallback, errorcallback, markForUpload);
    }
};
/************************************************************************************
 * Creates number of new instances of wtemployeetrace in the local Database. These new 
 * records will be merged with the enterprise datasource in the next Sync. Based upon 
 * kony.sync.enableORMValidations flag, validations will be enabled/disabled.
 * e.g.	var valuesArray = [];
 *		valuesArray[0] = {};
 *		valuesArray[0].employeeCode = "employeeCode_0";
 *		valuesArray[0].email = "email_0";
 *		valuesArray[0].traceTimestamp = "traceTimestamp_0";
 *		valuesArray[0].traceLocLatitude = 0;
 *		valuesArray[0].traceLocLongitude = 0;
 *		valuesArray[0].traceIsOpen = true;
 *		valuesArray[0].traceComment = "traceComment_0";
 *		valuesArray[0].traceTimestampStr = "traceTimestampStr_0";
 *		valuesArray[0].branchNumber = "branchNumber_0";
 *		valuesArray[1] = {};
 *		valuesArray[1].employeeCode = "employeeCode_1";
 *		valuesArray[1].email = "email_1";
 *		valuesArray[1].traceTimestamp = "traceTimestamp_1";
 *		valuesArray[1].traceLocLatitude = 1;
 *		valuesArray[1].traceLocLongitude = 1;
 *		valuesArray[1].traceIsOpen = true;
 *		valuesArray[1].traceComment = "traceComment_1";
 *		valuesArray[1].traceTimestampStr = "traceTimestampStr_1";
 *		valuesArray[1].branchNumber = "branchNumber_1";
 *		valuesArray[2] = {};
 *		valuesArray[2].employeeCode = "employeeCode_2";
 *		valuesArray[2].email = "email_2";
 *		valuesArray[2].traceTimestamp = "traceTimestamp_2";
 *		valuesArray[2].traceLocLatitude = 2;
 *		valuesArray[2].traceLocLongitude = 2;
 *		valuesArray[2].traceIsOpen = true;
 *		valuesArray[2].traceComment = "traceComment_2";
 *		valuesArray[2].traceTimestampStr = "traceTimestampStr_2";
 *		valuesArray[2].branchNumber = "branchNumber_2";
 *		WTSync.wtemployeetrace.createAll(valuesArray, successcallback, errorcallback, true);
 *************************************************************************************/
WTSync.wtemployeetrace.createAll = function(valuesArray, successcallback, errorcallback, markForUpload) {
    sync.log.trace("Entering WTSync.wtemployeetrace.createAll function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "WTSync.wtemployeetrace.createAll", "createAll", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = WTSync.wtemployeetrace.getTableName();
    markForUpload = kony.sync.getUploadStatus(markForUpload);
    var isProperData = true;
    var arrayLen = 0;
    var errorInfo = [];
    var arrayLength = valuesArray.length;
    var errObject = null;
    var isReferentialIntegrityFailure = false;
    var errMsg = null;
    if (kony.sync.enableORMValidations) {
        var newValuesArray = [];
        //column level validations
        for (var i = 0; valuesArray != null && i < arrayLength; i++) {
            var valuestable = valuesArray[i];
            if (kony.sync.attributeValidation(valuestable, "wtemployeetrace", errorcallback, true) === false) {
                return;
            }
            newValuesArray[i] = valuestable;
        }
        valuesArray = newValuesArray;
        var connection = kony.sync.getConnectionOnly(dbname, dbname);
        kony.sync.startTransaction(connection, checkIntegrity, transactionSuccessCallback, transactionErrorCallback);
        var isError = false;
    } else {
        //copying by value
        var newValuesArray = [];
        arrayLength = valuesArray.length;
        for (var i = 0; valuesArray != null && i < arrayLength; i++) {
            newValuesArray[i] = kony.sync.CreateCopy(valuesArray[i]);
        }
        valuesArray = newValuesArray;
        kony.sync.massInsert(dbname, tbname, valuesArray, successcallback, errorcallback, markForUpload);
    }

    function transactionErrorCallback() {
        if (isError == true) {
            //Statement error has occurred
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
        } else {
            //Transaction error has occurred
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeTransaction, kony.sync.getErrorMessage(kony.sync.errorCodeTransaction), null));
        }
    }

    function transactionSuccessCallback() {
        sync.log.trace("Entering  WTSync.wtemployeetrace.createAll->transactionSuccessCallback");
        if (!isError) {
            kony.sync.massInsert(dbname, tbname, valuesArray, successcallback, errorcallback, markForUpload);
        } else {
            if (isReferentialIntegrityFailure) {
                kony.sync.verifyAndCallClosure(errorcallback, errObject);
            }
        }
    }
    //foreign key constraints validations
    function checkIntegrity(tx) {
        sync.log.trace("Entering  WTSync.wtemployeetrace.createAll->checkIntegrity");
        arrayLength = valuesArray.length;
        for (var i = 0; valuesArray != null && i < arrayLength; i++) {
            var relationshipMap = {};
            relationshipMap = WTSync.wtemployeetrace.getRelationshipMap(relationshipMap, valuesArray[i]);
            errObject = kony.sync.checkIntegrityinTransaction(tx, relationshipMap, null);
            if (errObject === false) {
                isError = true;
                return;
            }
            if (errObject !== true) {
                isError = true;
                isReferentialIntegrityFailure = true;
                return;
            }
        }
    }
};
/************************************************************************************
 * Updates wtemployeetrace using primary key in the local Database. The update will be
 * merged with the enterprise datasource in the next Sync.
 *************************************************************************************/
WTSync.wtemployeetrace.prototype.updateByPK = function(successcallback, errorcallback) {
    sync.log.trace("Entering  WTSync.wtemployeetrace.prototype.updateByPK function");
    var pks = this.getPKTable();
    var valuestable = this.getValuesTable(false);
    WTSync.wtemployeetrace.updateByPK(pks, valuestable, successcallback, errorcallback, this.markForUpload);
};
WTSync.wtemployeetrace.updateByPK = function(pks, valuestable, successcallback, errorcallback, markForUpload) {
    sync.log.trace("Entering  WTSync.wtemployeetrace.updateByPK-> main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "WTSync.wtemployeetrace.updateByPK", "updateByPk", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = WTSync.wtemployeetrace.getTableName();
    markForUpload = kony.sync.getUploadStatus(markForUpload);
    var wcs = [];
    if (WTSync.wtemployeetrace.pkCheck(pks, wcs, errorcallback, "updating") === false) {
        return;
    }
    if (kony.sync.attributeValidation(valuestable, "wtemployeetrace", errorcallback, false) === false) {
        return;
    }
    var relationshipMap = {};
    relationshipMap = WTSync.wtemployeetrace.getRelationshipMap(relationshipMap, valuestable);
    kony.sync.updateByPK(tbname, dbname, relationshipMap, pks, valuestable, successcallback, errorcallback, markForUpload, wcs);
};
/************************************************************************************
 * Updates wtemployeetrace(s) using where clause in the local Database. The update(s)
 * will be merged with the enterprise datasource in the next Sync.
 *************************************************************************************/
WTSync.wtemployeetrace.update = function(wcs, valuestable, successcallback, errorcallback, markForUpload) {
    sync.log.trace("Entering WTSync.wtemployeetrace.update function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "WTSync.wtemployeetrace.update", "update", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = WTSync.wtemployeetrace.getTableName();
    markForUpload = kony.sync.getUploadStatus(markForUpload);
    wcs = kony.sync.validateWhereClause(wcs);
    if (kony.sync.attributeValidation(valuestable, "wtemployeetrace", errorcallback, false) === false) {
        return;
    }

    function executeSuccess() {
        sync.log.trace("Entering  WTSync.wtemployeetrace.update-> success callback of Integrity Check");
        kony.sync.single_update_execute(dbname, tbname, valuestable, wcs, successcallback, errorcallback, true, markForUpload, WTSync.wtemployeetrace.getPKTable());
    }
    if (kony.sync.enableORMValidations) {
        var relationshipMap = {};
        relationshipMap = WTSync.wtemployeetrace.getRelationshipMap(relationshipMap, valuestable);
        kony.sync.checkIntegrity(dbname, relationshipMap, executeSuccess, errorcallback);
    } else {
        kony.sync.single_update_execute(dbname, tbname, valuestable, wcs, successcallback, errorcallback, true, markForUpload, WTSync.wtemployeetrace.getPKTable());
    }
};
/************************************************************************************
 * Updates wtemployeetrace(s) satisfying one or more where clauses in the local Database. 
 * The update(s) will be merged with the enterprise datasource in the next Sync.
 * Based upon kony.sync.enableORMValidations flag, validations will be enabled/disabled.
 * e.g.	var inputArray = [];
 *		inputArray[0] = {};
 *		inputArray[0].changeSet = {};
 *		inputArray[0].changeSet.employeeCode = "employeeCode_updated0";
 *		inputArray[0].changeSet.email = "email_updated0";
 *		inputArray[0].changeSet.traceTimestamp = "traceTimestamp_updated0";
 *		inputArray[0].changeSet.traceLocLatitude = 0;
 *		inputArray[0].whereClause = "where traceId = 0";
 *		inputArray[1] = {};
 *		inputArray[1].changeSet = {};
 *		inputArray[1].changeSet.employeeCode = "employeeCode_updated1";
 *		inputArray[1].changeSet.email = "email_updated1";
 *		inputArray[1].changeSet.traceTimestamp = "traceTimestamp_updated1";
 *		inputArray[1].changeSet.traceLocLatitude = 1;
 *		inputArray[1].whereClause = "where traceId = 1";
 *		inputArray[2] = {};
 *		inputArray[2].changeSet = {};
 *		inputArray[2].changeSet.employeeCode = "employeeCode_updated2";
 *		inputArray[2].changeSet.email = "email_updated2";
 *		inputArray[2].changeSet.traceTimestamp = "traceTimestamp_updated2";
 *		inputArray[2].changeSet.traceLocLatitude = 2;
 *		inputArray[2].whereClause = "where traceId = 2";
 *		WTSync.wtemployeetrace.updateAll(inputArray,successcallback,errorcallback);
 *************************************************************************************/
WTSync.wtemployeetrace.updateAll = function(inputArray, successcallback, errorcallback, markForUpload) {
        sync.log.trace("Entering WTSync.wtemployeetrace.updateAll function");
        if (!kony.sync.isSyncInitialized(errorcallback)) {
            return;
        }
        if (!kony.sync.validateInput(arguments, "WTSync.wtemployeetrace.updateAll", "updateAll", errorcallback)) {
            return;
        }
        var dbname = "100004898743b5a1e";
        var tbname = "wtemployeetrace";
        var isError = false;
        var errObject = null;
        if (markForUpload == false || markForUpload == "false") {
            markForUpload = "false"
        } else {
            markForUpload = "true"
        }
        if ((kony.sync.enableORMValidations)) {
            var newInputArray = [];
            for (var i = 0;
                ((inputArray) != null) && i < inputArray.length; i++) {
                var v = inputArray[i];
                var valuestable = v.changeSet;
                var isEmpty = true;
                for (var key in valuestable) {
                    isEmpty = false;
                    break;
                }
                if (isEmpty) {
                    errorcallback(kony.sync.getErrorTable(kony.sync.errorCodeNullValue, kony.sync.getErrorMessage(kony.sync.errorCodeNullValue)));
                    return;
                }
                var wcs = v.whereClause;
                var twcs = wcs;
                if (kony.sync.attributeValidation(valuestable, "wtemployeetrace", errorcallback, false) === false) {
                    return;
                }
                newInputArray[i] = [];
                newInputArray[i].changeSet = valuestable;
                newInputArray[i].whereClause = wcs;
            }
            inputArray = newInputArray;
            var connection = kony.sync.getConnectionOnly(dbname, dbname);
            kony.sync.startTransaction(connection, checkIntegrity, transactionSuccessCallback, transactionErrorCallback);
        } else {
            //copying by value
            var newInputArray = [];
            for (var i = 0;
                ((inputArray) != null) && i < inputArray.length; i++) {
                var v = inputArray[i];
                newInputArray[i] = kony.sync.CreateCopy(v);
            }
            inputArray = newInputArray;
            kony.sync.massUpdate(dbname, tbname, inputArray, successcallback, errorcallback, markForUpload, WTSync.wtemployeetrace.getPKTable());
        }

        function transactionSuccessCallback() {
            sync.log.trace("Entering  WTSync.wtemployeetrace.updateAll->transactionSuccessCallback");
            if (!isError) {
                kony.sync.massUpdate(dbname, tbname, inputArray, successcallback, transactionErrorCallback, markForUpload, WTSync.wtemployeetrace.getPKTable());
            }
        }

        function transactionErrorCallback() {
            if (errObject === false) {
                //Sql statement error has occcurred
                kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
                kony.sync.errorObject = null;
            } else if (errObject !== null) {
                // Referential integrity error has occurred
                kony.sync.verifyAndCallClosure(errorcallback, errObject);
            } else {
                //Transaction error has occurred
                kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodeTransaction, kony.sync.getErrorMessage(kony.sync.errorCodeTransaction), null));
            }
        }
        //foreign key constraints validations
        function checkIntegrity(tx) {
            sync.log.trace("Entering  WTSync.wtemployeetrace.updateAll->checkIntegrity");
            for (var i = 0;
                ((inputArray) != null) && i < inputArray.length; i++) {
                var relationshipMap = {};
                relationshipMap = WTSync.wtemployeetrace.getRelationshipMap(relationshipMap, inputArray[i].changeSet);
                sync.log.debug("Relationship Map for Integrity check created:", relationshipMap);
                errObject = kony.sync.checkIntegrityinTransaction(tx, relationshipMap, null);
                if (errObject === false) {
                    isError = true;
                    return;
                }
                if (errObject !== true) {
                    isError = true;
                    kony.sync.rollbackTransaction(tx);
                    return;
                }
            }
        }
    }
    /************************************************************************************
     * Deletes wtemployeetrace using primary key from the local Database. The record will be
     * deleted from the enterprise datasource in the next Sync.
     *************************************************************************************/
WTSync.wtemployeetrace.prototype.deleteByPK = function(successcallback, errorcallback) {
    sync.log.trace("Entering WTSync.wtemployeetrace.prototype.deleteByPK function");
    var pks = this.getPKTable();
    WTSync.wtemployeetrace.deleteByPK(pks, successcallback, errorcallback, this.markForUpload);
};
WTSync.wtemployeetrace.deleteByPK = function(pks, successcallback, errorcallback, markForUpload) {
    sync.log.trace("Entering WTSync.wtemployeetrace.deleteByPK-> main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "WTSync.wtemployeetrace.deleteByPK", "deleteByPK", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = WTSync.wtemployeetrace.getTableName();
    markForUpload = kony.sync.getUploadStatus(markForUpload);
    var wcs = [];
    var isError = false;
    var pkNotFound = false;
    var twcs = [];
    var deletedRows;
    var record = "";
    if (WTSync.wtemployeetrace.pkCheck(pks, wcs, errorcallback, "deleting") === false) {
        return;
    }
    twcs = kony.sync.CreateCopy(wcs);

    function wtemployeetraceTransactionCallback(tx) {
        sync.log.trace("Entering WTSync.wtemployeetrace.deleteByPK->wtemployeetrace_PKPresent successcallback");
        record = kony.sync.getOriginalRow(tx, tbname, wcs, errorcallback);
        if (record === false) {
            isError = true;
            return;
        }
        if (null !== record) {} else {
            pkNotFound = true;
        }
        var deletedRows = kony.sync.remove(tx, tbname, wcs, false, markForUpload, null);
        if (deletedRows === false) {
            isError = true;
        }
    }

    function wtemployeetraceErrorCallback() {
        sync.log.error("Entering WTSync.wtemployeetrace.deleteByPK->relationship failure callback");
        if (isError === false) {
            kony.sync.verifyAndCallClosure(errorcallback);
        }
        if (kony.sync.errorObject != null) {
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
            kony.sync.errorObject = null;
        }
    }

    function wtemployeetraceSuccessCallback() {
        sync.log.trace("Entering WTSync.wtemployeetrace.deleteByPK->relationship success callback");
        if (pkNotFound === true) {
            kony.sync.verifyAndCallClosure(pkNotFoundErrCallback);
            return;
        }
        if (!isError) {
            kony.sync.verifyAndCallClosure(successcallback, {
                rowsdeleted: 1
            });
        }
    }

    function pkNotFoundErrCallback() {
        sync.log.error("Entering WTSync.wtemployeetrace.deleteByPK->PK not found callback");
        kony.sync.pkNotFoundErrCallback(errorcallback, tbname);
    }
    var dbconnection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
    if (dbconnection != null) {
        kony.sync.startTransaction(dbconnection, wtemployeetraceTransactionCallback, wtemployeetraceSuccessCallback, wtemployeetraceErrorCallback, "Single Execute");
    }
};
/************************************************************************************
 * Deletes wtemployeetrace(s) using where clause from the local Database. The record(s)
 * will be deleted from the enterprise datasource in the next Sync.
 * e.g. WTSync.wtemployeetrace.remove("where employeeCode like 'A%'", successcallback,errorcallback, true);
 *************************************************************************************/
WTSync.wtemployeetrace.remove = function(wcs, successcallback, errorcallback, markForUpload) {
    sync.log.trace("Entering WTSync.wtemployeetrace.remove->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "WTSync.wtemployeetrace.remove", "remove", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = WTSync.wtemployeetrace.getTableName();
    markForUpload = kony.sync.getUploadStatus(markForUpload);
    wcs = kony.sync.validateWhereClause(wcs);
    var twcs = wcs;
    var isError = false;
    var rowsDeleted;

    function wtemployeetrace_removeTransactioncallback(tx) {
        wcs = " " + wcs;
        rowsDeleted = kony.sync.deleteBatch(tx, tbname, wcs, false, markForUpload, errorcallback)
        if (rowsDeleted === false) {
            isError = true;
        }
    }

    function wtemployeetrace_removeSuccess() {
        sync.log.trace("Entering WTSync.wtemployeetrace.remove->wtemployeetrace_removeSuccess function");
        if (!isError) {
            kony.sync.verifyAndCallClosure(successcallback, rowsDeleted);
        }
    }

    function errorcallbackWrapper() {
        sync.log.trace("Entering WTSync.wtemployeetrace.remove->error callback function");
        if (!isError) {
            kony.sync.showTransactionError(errorcallback);
        }
        if (kony.sync.errorObject != null) {
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
            kony.sync.errorObject = null;
        }
    }

    function deleteEntity() {
        sync.log.trace("Entering WTSync.wtemployeetrace.remove->delete Entity function");
        var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
        if (connection != null) {
            kony.sync.startTransaction(connection, wtemployeetrace_removeTransactioncallback, wtemployeetrace_removeSuccess, errorcallbackWrapper);
        }
    }
    deleteEntity();
};
/************************************************************************************
 * Deletes wtemployeetrace using primary key from the local Database. This will
 * not have any effect in enterprise datasource in subsequent sync cycles
 *************************************************************************************/
WTSync.wtemployeetrace.prototype.removeDeviceInstanceByPK = function(successcallback, errorcallback) {
    sync.log.trace("Entering WTSync.wtemployeetrace.prototype.removeDeviceInstanceByPK function");
    var pks = this.getPKTable();
    WTSync.wtemployeetrace.removeDeviceInstanceByPK(pks, successcallback, errorcallback);
};
WTSync.wtemployeetrace.removeDeviceInstanceByPK = function(pks, successcallback, errorcallback) {
    sync.log.trace("Entering WTSync.wtemployeetrace.removeDeviceInstanceByPK function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "WTSync.wtemployeetrace.removeDeviceInstanceByPK", "removeDeviceInstanceByPK", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = WTSync.wtemployeetrace.getTableName();
    var wcs = [];
    var isError = false;
    var pkNotFound = false;
    var deletedRows;
    if (WTSync.wtemployeetrace.pkCheck(pks, wcs, errorcallback, "deleting") === false) {
        return;
    }

    function wtemployeetraceTransactionCallback(tx) {
        sync.log.trace("Entering WTSync.wtemployeetrace.removeDeviceInstanceByPK -> wtemployeetraceTransactionCallback");
        var record = kony.sync.getOriginalRow(tx, tbname, wcs, errorcallback);
        if (null !== record && false != record) {
            deletedRows = kony.sync.remove(tx, tbname, wcs, true, null, null);
            if (deletedRows === false) {
                isError = true;
            }
        } else {
            pkNotFound = true;
        }
    }

    function wtemployeetraceErrorCallback() {
        sync.log.error("Entering WTSync.wtemployeetrace.removeDeviceInstanceByPK -> wtemployeetraceErrorCallback");
        if (isError === false) {
            kony.sync.verifyAndCallClosure(errorcallback);
        }
        if (kony.sync.errorObject != null) {
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
            kony.sync.errorObject = null;
        }
    }

    function wtemployeetraceSuccessCallback() {
        sync.log.trace("Entering WTSync.wtemployeetrace.removeDeviceInstanceByPK -> wtemployeetraceSuccessCallback");
        if (pkNotFound === true) {
            kony.sync.verifyAndCallClosure(pkNotFoundErrCallback);
            return;
        }
        if (!isError) {
            kony.sync.verifyAndCallClosure(successcallback, {
                rowsdeleted: 1
            });
        }
    }

    function pkNotFoundErrCallback() {
        sync.log.error("Entering WTSync.wtemployeetrace.removeDeviceInstanceByPK -> PK not found callback");
        kony.sync.pkNotFoundErrCallback(errorcallback, tbname);
    }
    var dbconnection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
    if (dbconnection != null) {
        kony.sync.startTransaction(dbconnection, wtemployeetraceTransactionCallback, wtemployeetraceSuccessCallback, wtemployeetraceErrorCallback, "Single Execute");
    }
};
/************************************************************************************
 * Deletes wtemployeetrace(s) using where clause from the local Database. This will
 * not have any effect in enterprise datasource in subsequent sync cycles
 *************************************************************************************/
WTSync.wtemployeetrace.removeDeviceInstance = function(wcs, successcallback, errorcallback) {
    sync.log.trace("Entering WTSync.wtemployeetrace.removeDeviceInstance->main function");
    var dbname = kony.sync.getDBName();
    var tbname = WTSync.wtemployeetrace.getTableName();
    wcs = kony.sync.validateWhereClause(wcs);
    var twcs = wcs;
    var isError = false;
    var rowsDeleted;

    function wtemployeetrace_removeTransactioncallback(tx) {
        wcs = " " + wcs;
        rowsDeleted = kony.sync.deleteBatch(tx, tbname, wcs, true, null, errorcallback)
        if (rowsDeleted === false) {
            isError = true;
        }
    }

    function wtemployeetrace_removeSuccess() {
        sync.log.trace("Entering WTSync.wtemployeetrace.remove->wtemployeetrace_removeSuccess function");
        if (!isError) {
            kony.sync.verifyAndCallClosure(successcallback, rowsDeleted);
        }
    }

    function errorcallbackWrapper() {
        sync.log.trace("Entering WTSync.wtemployeetrace.remove->error callback function");
        if (!isError) {
            kony.sync.showTransactionError(errorcallback);
        }
        if (kony.sync.errorObject != null) {
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
            kony.sync.errorObject = null;
        }
    }

    function deleteEntity() {
        sync.log.trace("Entering WTSync.wtemployeetrace.remove->delete Entity function");
        var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
        if (connection != null) {
            kony.sync.startTransaction(connection, wtemployeetrace_removeTransactioncallback, wtemployeetrace_removeSuccess, errorcallbackWrapper);
        }
    }
    deleteEntity();
};
/************************************************************************************
 * Retrieves wtemployeetrace using primary key from the local Database. 
 *************************************************************************************/
WTSync.wtemployeetrace.prototype.getAllDetailsByPK = function(successcallback, errorcallback) {
    sync.log.trace("Entering WTSync.wtemployeetrace.prototype.getAllDetailsByPK function");
    var pks = this.getPKTable();
    WTSync.wtemployeetrace.getAllDetailsByPK(pks, successcallback, errorcallback);
};
WTSync.wtemployeetrace.getAllDetailsByPK = function(pks, successcallback, errorcallback) {
    sync.log.trace("Entering WTSync.wtemployeetrace.getAllDetailsByPK-> main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "WTSync.wtemployeetrace.getAllDetailsByPK", "getAllDetailsByPK", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = WTSync.wtemployeetrace.getTableName();
    var wcs = [];
    if (WTSync.wtemployeetrace.pkCheck(pks, wcs, errorcallback, "searching") === false) {
        return;
    }
    twcs = kony.sync.CreateCopy(wcs);
    var query = kony.sync.qb_createQuery();
    kony.sync.qb_select(query, null);
    kony.sync.qb_from(query, tbname);
    kony.sync.qb_where(query, wcs);
    var query_compile = kony.sync.qb_compile(query);
    var sql = query_compile[0];
    var params = query_compile[1];

    function mySuccCallback(res) {
        sync.log.trace("Entering WTSync.wtemployeetrace.getAllDetailsByPK-> success callback function");
        successcallback(WTSync.wtemployeetrace.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
    }
    kony.sync.single_select_execute(dbname, sql, params, mySuccCallback, errorcallback);
};
/************************************************************************************
 * Retrieves wtemployeetrace(s) using where clause from the local Database. 
 * e.g. WTSync.wtemployeetrace.find("where employeeCode like 'A%'", successcallback,errorcallback);
 *************************************************************************************/
WTSync.wtemployeetrace.find = function(wcs, successcallback, errorcallback) {
    sync.log.trace("Entering WTSync.wtemployeetrace.find function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "WTSync.wtemployeetrace.find", "find", errorcallback)) {
        return;
    }
    //wcs will be a string formed by the user.
    var dbname = kony.sync.getDBName();
    var tbname = WTSync.wtemployeetrace.getTableName();
    wcs = kony.sync.validateWhereClause(wcs);
    var sql = "select * from \"" + tbname + "\" " + wcs;

    function mySuccCallback(res) {
        kony.sync.verifyAndCallClosure(successcallback, WTSync.wtemployeetrace.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
    }
    kony.sync.single_select_execute(dbname, sql, null, mySuccCallback, errorcallback);
};
/************************************************************************************
 * Marks instance of wtemployeetrace with given primary key for upload. This will 
 * enable deferred records to merge with the enterprise datasource in the next Sync.
 *************************************************************************************/
WTSync.wtemployeetrace.prototype.markForUploadbyPK = function(successcallback, errorcallback) {
    sync.log.trace("Entering WTSync.wtemployeetrace.prototype.markForUploadbyPK function");
    var pks = this.getPKTable();
    WTSync.wtemployeetrace.markForUploadbyPK(pks, successcallback, errorcallback);
};
WTSync.wtemployeetrace.markForUploadbyPK = function(pks, successcallback, errorcallback) {
    sync.log.trace("Entering WTSync.wtemployeetrace.markForUploadbyPK function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "WTSync.wtemployeetrace.markForUploadbyPK", "markForUploadbyPK", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = WTSync.wtemployeetrace.getTableName();
    var isError = false;
    var recordsFound = false;
    var recordsMarkedForUpload = 0;
    var wcs = [];
    if (WTSync.wtemployeetrace.pkCheck(pks, wcs, errorcallback, "marking for upload by PK") === false) {
        return;
    }

    function markRecordForUpload(tx, record) {
        var versionMapMain = [];
        versionMapMain[kony.sync.mainTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
        var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
        var versionNo = kony.sync.getseqnumber(tx, scopename);
        if (versionNo === false) {
            return false;
        }
        versionMapMain[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
        var query = kony.sync.qb_createQuery();
        kony.sync.qb_update(query, tbname);
        kony.sync.qb_set(query, versionMapMain);
        kony.sync.qb_where(query, wcs);
        var query_compile = kony.sync.qb_compile(query);
        var sql = query_compile[0];
        var params = query_compile[1];
        return kony.sync.executeSql(tx, sql, params);
    }

    function markRecordForUploadHistory(tx, record) {
        var versionMap = [];
        versionMap[kony.sync.historyTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
        var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
        var versionNo = kony.sync.getseqnumber(tx, scopename);
        if (versionNo === false) {
            return false;
        }
        var twcs = [];
        twcs = wcs;
        kony.table.insert(twcs, {
            key: kony.sync.historyTableChangeTypeColumn,
            value: record[kony.sync.historyTableChangeTypeColumn],
            optype: "EQ",
            comptype: "AND"
        });
        versionMap[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
        var query = kony.sync.qb_createQuery();
        kony.sync.qb_update(query, tbname + kony.sync.historyTableName);
        kony.sync.qb_set(query, versionMap);
        kony.sync.qb_where(query, twcs);
        kony.table.remove(twcs);
        var query_compile = kony.sync.qb_compile(query);
        var sql = query_compile[0];
        var params = query_compile[1];
        return kony.sync.executeSql(tx, sql, params);
    }

    function single_transaction_callback(tx) {
        var query = kony.sync.qb_createQuery();
        kony.sync.qb_select(query, [kony.sync.historyTableChangeTypeColumn]);
        kony.sync.qb_from(query, tbname);
        kony.sync.qb_where(query, wcs);
        var query_compile = kony.sync.qb_compile(query);
        var sql = query_compile[0];
        var params = query_compile[1];
        var resultSet = kony.sync.executeSql(tx, sql, params);
        if (resultSet === false) {
            isError = true;
            return;
        }
        var num_records = resultSet.rows.length;
        if (num_records > 0) {
            recordsFound = true;
            var record = kony.db.sqlResultsetRowItem(tx, resultSet, 0);
            var changeType = record[kony.sync.mainTableChangeTypeColumn];
            if (!kony.sync.isNullOrUndefined(changeType) && kony.string.startsWith("" + changeType, "9")) {
                recordsMarkedForUpload = 1;
                if (markRecordForUpload(tx, record) === false) {
                    isError = true;
                    return;
                }
            }
        }
        var query1 = kony.sync.qb_createQuery();
        kony.sync.qb_select(query1, [kony.sync.historyTableChangeTypeColumn]);
        kony.sync.qb_from(query1, tbname + kony.sync.historyTableName);
        kony.sync.qb_where(query1, wcs);
        var query1_compile = kony.sync.qb_compile(query1);
        var sql1 = query1_compile[0];
        var params1 = query1_compile[1];
        var resultSet1 = kony.sync.executeSql(tx, sql1, params1);
        if (resultSet1 !== false) {
            var num_records = resultSet1.rows.length;
            for (var i = 0; i <= num_records - 1; i++) {
                var record = kony.db.sqlResultsetRowItem(tx, resultSet1, i);
                if (markRecordForUploadHistory(tx, record) === false) {
                    isError = true;
                    return;
                }
                recordsFound = true;
            }
        } else {
            isError = true;
        }
    }

    function single_transaction_success_callback() {
        if (recordsFound === true) {
            kony.sync.verifyAndCallClosure(successcallback, {
                count: recordsMarkedForUpload
            });
        } else {
            kony.sync.pkNotFoundErrCallback(errorcallback, tbname);
        }
    }

    function single_transaction_error_callback(res) {
        if (!isError) {
            kony.sync.showTransactionError(errorcallback);
        } else {
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
            kony.sync.errorObject = null;
        }
    }
    var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
    if (connection != null) {
        kony.sync.startTransaction(connection, single_transaction_callback, single_transaction_success_callback, single_transaction_error_callback);
    }
};
/************************************************************************************
 * Marks instance(s) of wtemployeetrace matching given where clause for upload. This will 
 * enable deferred records to merge with the enterprise datasource in the next Sync.
 *************************************************************************************/
WTSync.wtemployeetrace.markForUpload = function(wcs, successcallback, errorcallback) {
    sync.log.trace("Entering WTSync.wtemployeetrace.markForUpload->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "WTSync.wtemployeetrace.markForUpload", "markForUpload", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = WTSync.wtemployeetrace.getTableName();
    var isError = false;
    var num_records_main = 0;
    wcs = kony.sync.validateWhereClause(wcs);
    if (!kony.sync.isNull(wcs) && !kony.sync.isEmptyString(wcs)) {
        wcs = wcs + " and " + kony.sync.historyTableChangeTypeColumn + " like '9%'";
    } else {
        wcs = "where " + kony.sync.historyTableChangeTypeColumn + " like '9%'";
    }

    function markRecordForUpload(tx, record) {
        var versionMapMain = [];
        versionMapMain[kony.sync.mainTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
        var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
        var versionNo = kony.sync.getseqnumber(tx, scopename);
        if (versionNo === false) {
            return false;
        }
        versionMapMain[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
        var query = kony.sync.qb_createQuery();
        kony.sync.qb_update(query, tbname);
        kony.sync.qb_set(query, versionMapMain);
        var query_compile = kony.sync.qb_compile(query);
        var sql = query_compile[0] + " " + wcs;
        var params = query_compile[1];
        if (kony.sync.executeSql(tx, sql, params) === false) {
            return false;
        }
    }

    function markRecordForUploadHistory(tx, record) {
        var versionMap = [];
        versionMap[kony.sync.historyTableChangeTypeColumn] = kony.sync.getChangeTypeForUploadTrue(record[kony.sync.historyTableChangeTypeColumn]);
        var scopename = kony.sync.scopes.syncTableScopeDic[tbname];
        var versionNo = kony.sync.getseqnumber(tx, scopename);
        if (versionNo === false) {
            return false;
        }
        var twcs = "";
        twcs = wcs;
        twcs = twcs + " AND " + kony.sync.historyTableChangeTypeColumn + " = " + record[kony.sync.historyTableChangeTypeColumn];
        versionMap[kony.sync.historyTableSyncVersionColumn] = versionNo.versionnumber;
        var query = kony.sync.qb_createQuery();
        kony.sync.qb_update(query, tbname + kony.sync.historyTableName);
        kony.sync.qb_set(query, versionMap);
        var query_compile = kony.sync.qb_compile(query);
        var sql = query_compile[0] + " " + twcs;
        var params = query_compile[1];
        if (kony.sync.executeSql(tx, sql, params) === false) {
            return false;
        }
    }

    function single_transaction_callback(tx) {
        sync.log.trace("Entering WTSync.wtemployeetrace.markForUpload->single_transaction_callback");
        //updating main table
        var sql = "select " + kony.sync.historyTableChangeTypeColumn + " from \"" + tbname + "\" " + wcs;
        var resultSet = kony.sync.executeSql(tx, sql, null);
        if (resultSet === false) {
            isError = true;
            return;
        }
        num_records_main = resultSet.rows.length;
        for (var i = 0; i < num_records_main; i++) {
            var record = kony.db.sqlResultsetRowItem(tx, resultSet, i);
            if (markRecordForUpload(tx, record) === false) {
                isError = true;
                return;
            }
        }
        //updating history table
        var sql = "select " + kony.sync.historyTableChangeTypeColumn + " from " + tbname + kony.sync.historyTableName + " " + wcs;
        var resultSet = kony.sync.executeSql(tx, sql, null);
        if (resultSet === false) {
            isError = true;
            return;
        }
        var num_records = resultSet.rows.length;
        for (var i = 0; i <= num_records - 1; i++) {
            var record = kony.db.sqlResultsetRowItem(tx, resultSet, i);
            if (markRecordForUploadHistory(tx, record) === false) {
                isError = true;
                return;
            }
        }
    }

    function single_transaction_success_callback() {
        sync.log.trace("Entering WTSync.wtemployeetrace.markForUpload->single_transaction_success_callback");
        kony.sync.verifyAndCallClosure(successcallback, {
            count: num_records_main
        });
    }

    function single_transaction_error_callback() {
        sync.log.error("Entering WTSync.wtemployeetrace.markForUpload->single_transaction_error_callback");
        if (!isError) {
            kony.sync.showTransactionError(errorcallback);
        } else {
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.errorObject);
            kony.sync.errorObject = null;
        }
    }
    var connection = kony.sync.getConnectionOnly(dbname, dbname, errorcallback);
    if (connection != null) {
        kony.sync.startTransaction(connection, single_transaction_callback, single_transaction_success_callback, single_transaction_error_callback);
    }
};
/************************************************************************************
 * Retrieves instance(s) of wtemployeetrace pending for upload. Records are marked for
 * pending upload if they have been updated or created locally and the changes have
 * not been merged with enterprise datasource.
 *************************************************************************************/
WTSync.wtemployeetrace.getPendingUpload = function(wcs, successcallback, errorcallback) {
    sync.log.trace("Entering WTSync.wtemployeetrace.getPendingUpload->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = WTSync.wtemployeetrace.getTableName();
    var currentversion = kony.sync.getCurrentVersionNumber(tbname);
    var sql;
    if (typeof(wcs) === "string" && wcs != null) {
        wcs = kony.sync.validateWhereClause(wcs);
        sql = "select * from \"" + tbname + "\" " + wcs + " and " + kony.sync.mainTableChangeTypeColumn + " is not null AND " + kony.sync.mainTableChangeTypeColumn + " <> -1 AND " + kony.sync.mainTableSyncVersionColumn + " = " + currentversion + " AND " + kony.sync.mainTableChangeTypeColumn + " NOT LIKE '9%'";
    } else {
        errorcallback = successcallback;
        successcallback = wcs;
        sql = "select * from \"" + tbname + "\" WHERE " + kony.sync.mainTableChangeTypeColumn + " is not null AND " + kony.sync.mainTableChangeTypeColumn + " <> -1 AND " + kony.sync.mainTableSyncVersionColumn + " = " + currentversion + " AND " + kony.sync.mainTableChangeTypeColumn + " NOT LIKE '9%'";
    }
    kony.sync.single_select_execute(dbname, sql, null, mySuccesscallback, errorcallback);

    function mySuccesscallback(res) {
        sync.log.trace("Entering WTSync.wtemployeetrace.getPendingUpload->successcallback function");
        kony.sync.verifyAndCallClosure(successcallback, WTSync.wtemployeetrace.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
    }
};
/************************************************************************************
 * Retrieves instance(s) of wtemployeetrace pending for acknowledgement. This is relevant
 * when the SyncObject is part of the SyncScope whose SyncStrategy is PersistentSync.
 * In persistent Sync the  records in the local database are put into a pending 
 * acknowledgement state after an upload.
 *************************************************************************************/
WTSync.wtemployeetrace.getPendingAcknowledgement = function(successcallback, errorcallback) {
    sync.log.trace("Entering WTSync.wtemployeetrace.getPendingAcknowledgement->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = WTSync.wtemployeetrace.getTableName();
    var currentversion = kony.sync.getCurrentVersionNumber(tbname);
    var mysql = "select * from \"" + tbname + "\" WHERE " + kony.sync.mainTableChangeTypeColumn + " is not null AND " + kony.sync.mainTableChangeTypeColumn + " <> -1 AND " + kony.sync.mainTableSyncVersionColumn + " <> " + currentversion + " AND " + kony.sync.mainTableChangeTypeColumn + " NOT LIKE '9%'";
    kony.sync.single_select_execute(dbname, mysql, null, mySuccesscallback, errorcallback);

    function mySuccesscallback(res) {
        sync.log.trace("Entering WTSync.wtemployeetrace.getPendingAcknowledgement success callback function");
        kony.sync.verifyAndCallClosure(successcallback, WTSync.wtemployeetrace.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
    }
};
/************************************************************************************
 * Retrieves instance(s) of wtemployeetrace deferred for upload.
 *************************************************************************************/
WTSync.wtemployeetrace.getDeferredUpload = function(wcs, successcallback, errorcallback) {
    sync.log.trace("Entering WTSync.wtemployeetrace.getDeferredUpload->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = WTSync.wtemployeetrace.getTableName();
    var sql;
    if (typeof(wcs) === "string" && wcs != null) {
        wcs = kony.sync.validateWhereClause(wcs);
        sql = "select * from \"" + tbname + "\" " + wcs + " and " + kony.sync.mainTableChangeTypeColumn + " is not null AND " + kony.sync.mainTableChangeTypeColumn + " <> -1 AND " + kony.sync.mainTableChangeTypeColumn + " LIKE '9%'";
    } else {
        errorcallback = successcallback;
        successcallback = wcs;
        sql = "select * from \"" + tbname + "\" WHERE " + kony.sync.mainTableChangeTypeColumn + " is not null AND " + kony.sync.mainTableChangeTypeColumn + " <> -1 AND " + kony.sync.mainTableChangeTypeColumn + " LIKE '9%'";
    }
    kony.sync.single_select_execute(dbname, sql, null, mySuccesscallback, errorcallback);

    function mySuccesscallback(res) {
        sync.log.trace("Entering WTSync.wtemployeetrace.getDeferredUpload->success callback function");
        kony.sync.verifyAndCallClosure(successcallback, WTSync.wtemployeetrace.convertTableToObject(kony.sync.filterNullsFromSelectResult(res)));
    }
};
/************************************************************************************
 * Rollbacks all changes to wtemployeetrace in local database to last synced state
 *************************************************************************************/
WTSync.wtemployeetrace.rollbackPendingLocalChanges = function(successcallback, errorcallback) {
    sync.log.trace("Entering WTSync.wtemployeetrace.rollbackPendingLocalChanges->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = WTSync.wtemployeetrace.getTableName();
    kony.sync.konySyncRollBackPendingChanges(tbname, dbname, null, mySuccesscallback, errorcallback);

    function mySuccesscallback(res) {
        sync.log.trace("Entering WTSync.wtemployeetrace.rollbackPendingLocalChanges->main function");
        kony.sync.verifyAndCallClosure(successcallback, res);
    }
};
/************************************************************************************
 * Rollbacks changes to wtemployeetrace's record with given primary key in local 
 * database to last synced state
 *************************************************************************************/
WTSync.wtemployeetrace.prototype.rollbackPendingLocalChangesByPK = function(successcallback, errorcallback) {
    sync.log.trace("Entering WTSync.wtemployeetrace.prototype.rollbackPendingLocalChangesByPK function");
    var pks = this.getPKTable();
    WTSync.wtemployeetrace.rollbackPendingLocalChangesByPK(pks, successcallback, errorcallback);
};
WTSync.wtemployeetrace.rollbackPendingLocalChangesByPK = function(pks, successcallback, errorcallback) {
    sync.log.trace("Entering WTSync.wtemployeetrace.rollbackPendingLocalChangesByPK->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "WTSync.wtemployeetrace.rollbackPendingLocalChangesByPK", "rollbackPendingLocalChangesByPK", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = WTSync.wtemployeetrace.getTableName();
    var wcs = [];
    if (WTSync.wtemployeetrace.pkCheck(pks, wcs, errorcallback, "rollbacking") === false) {
        return;
    }
    kony.sync.konySyncRollBackPendingChanges(tbname, dbname, wcs, mySuccesscallback, pkNotFoundErrCallback);

    function mySuccesscallback(res) {
        sync.log.trace("Entering WTSync.wtemployeetrace.rollbackPendingLocalChangesByPK->success callback function");
        kony.sync.verifyAndCallClosure(successcallback, res);
    }

    function pkNotFoundErrCallback() {
        sync.log.error("Entering WTSync.wtemployeetrace.rollbackPendingLocalChangesByPK->PK not found callback");
        kony.sync.pkNotFoundErrCallback(errorcallback, tbname);
    }
};
/************************************************************************************
 * isRecordDeferredForUpload returns true or false depending on whether wtemployeetrace's record  
 * with given primary key got deferred in last sync
 *************************************************************************************/
WTSync.wtemployeetrace.prototype.isRecordDeferredForUpload = function(successcallback, errorcallback) {
    sync.log.trace("Entering  WTSync.wtemployeetrace.prototype.isRecordDeferredForUpload function");
    var pks = this.getPKTable();
    WTSync.wtemployeetrace.isRecordDeferredForUpload(pks, successcallback, errorcallback);
};
WTSync.wtemployeetrace.isRecordDeferredForUpload = function(pks, successcallback, errorcallback) {
    sync.log.trace("Entering WTSync.wtemployeetrace.isRecordDeferredForUpload->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "WTSync.wtemployeetrace.isRecordDeferredForUpload", "isRecordDeferredForUpload", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = WTSync.wtemployeetrace.getTableName();
    var wcs = [];
    var flag;
    if (WTSync.wtemployeetrace.pkCheck(pks, wcs, errorcallback, "selecting") === false) {
        return;
    }
    var twcs = [];
    twcs = kony.sync.CreateCopy(wcs);
    kony.table.insert(twcs, {
        key: kony.sync.mainTableChangeTypeColumn,
        value: "9%",
        optype: "LIKE",
        comptype: "AND"
    });
    var query = kony.sync.qb_createQuery();
    kony.sync.qb_select(query, null);
    kony.sync.qb_from(query, tbname);
    kony.sync.qb_where(query, twcs);
    var query_compile = kony.sync.qb_compile(query);
    var sql = query_compile[0];
    var params = query_compile[1];
    kony.sync.single_select_execute(dbname, sql, params, mySuccesscallback, errorcallback);

    function mySuccesscallback(res) {
        sync.log.trace("Entering WTSync.wtemployeetrace.isRecordDeferredForUpload->successcallback function");
        if (res.length === 1) {
            flag = true;
        } else {
            flag = false;
        }
        kony.sync.verifyAndCallClosure(successcallback, {
            deferred: flag
        });
    }
};
/************************************************************************************
 * isRecordPendingForUpload returns true or false depending on whether wtemployeetrace's record  
 * with given primary key is pending for upload
 *************************************************************************************/
WTSync.wtemployeetrace.prototype.isRecordPendingForUpload = function(successcallback, errorcallback) {
    sync.log.trace("Entering  WTSync.wtemployeetrace.prototype.isRecordPendingForUpload function");
    var pks = this.getPKTable();
    WTSync.wtemployeetrace.isRecordPendingForUpload(pks, successcallback, errorcallback);
};
WTSync.wtemployeetrace.isRecordPendingForUpload = function(pks, successcallback, errorcallback) {
    sync.log.trace("Entering WTSync.wtemployeetrace.isRecordPendingForUpload->main function");
    if (!kony.sync.isSyncInitialized(errorcallback)) {
        return;
    }
    if (!kony.sync.validateInput(arguments, "WTSync.wtemployeetrace.isRecordPendingForUpload", "isRecordPendingForUpload", errorcallback)) {
        return;
    }
    var dbname = kony.sync.getDBName();
    var tbname = WTSync.wtemployeetrace.getTableName();
    var wcs = [];
    var flag;
    if (WTSync.wtemployeetrace.pkCheck(pks, wcs, errorcallback, "selecting") === false) {
        return;
    }
    var twcs = [];
    twcs = kony.sync.CreateCopy(wcs);
    kony.table.insert(twcs, {
        key: kony.sync.mainTableChangeTypeColumn,
        value: "9%",
        optype: "NOT LIKE",
        comptype: "AND"
    });
    var query = kony.sync.qb_createQuery();
    kony.sync.qb_select(query, null);
    kony.sync.qb_from(query, tbname);
    kony.sync.qb_where(query, twcs);
    var query_compile = kony.sync.qb_compile(query);
    var sql = query_compile[0];
    var params = query_compile[1];
    kony.sync.single_select_execute(dbname, sql, params, mySuccesscallback, errorcallback);

    function mySuccesscallback(res) {
        sync.log.trace("Entering WTSync.wtemployeetrace.isRecordPendingForUpload->successcallback function");
        if (res.length === 1) {
            flag = true;
        } else {
            flag = false;
        }
        kony.sync.verifyAndCallClosure(successcallback, {
            pending: flag
        });
    }
};
/************************************************************************************
 * Start of helper functions used internally, not to be used as ORMs
 *************************************************************************************/
//Deletes all the dependant tables in the relationship tables.Need to pass transaction handler as input
WTSync.wtemployeetrace.removeCascade = function(tx, wcs, errorcallback, markForUpload, isCascade, parentTable, isLocal) {
    sync.log.trace("Entering WTSync.wtemployeetrace.removeCascade function");
    var tbname = WTSync.wtemployeetrace.getTableName();
    markForUpload = kony.sync.getUploadStatus(markForUpload);

    function removeCascadeChildren() {}
    if (isCascade) {
        if (removeCascadeChildren() === false) {
            return false;
        }
        if (kony.sync.deleteBatch(tx, tbname, wcs, isLocal, markForUpload, null) === false) {
            return false;
        }
        return true;
    } else {
        var sql = "select * from \"" + tbname + "\" " + wcs;
        var resultSet = kony.sync.executeSql(tx, sql, null);
        if (resultSet === false) {
            return false;
        }
        var num_records = resultSet.rows.length;
        if (num_records === 0) {
            return true;
        } else {
            sync.log.error(kony.sync.getReferetialIntegrityDeleteErrMessg(tbname, tbname, tbname, parentTable));
            errorcallback(kony.sync.getErrorTable(kony.sync.errorCodeReferentialIntegrity, kony.sync.getReferetialIntegrityDeleteErrMessg(tbname, tbname, tbname, parentTable)));
            return false;
        }
    }
};
WTSync.wtemployeetrace.convertTableToObject = function(res) {
    sync.log.trace("Entering WTSync.wtemployeetrace.convertTableToObject function");
    objMap = [];
    if (res !== null) {
        for (var i in res) {
            var obj = new WTSync.wtemployeetrace();
            obj.traceId = res[i].traceId;
            obj.employeeCode = res[i].employeeCode;
            obj.email = res[i].email;
            obj.traceTimestamp = res[i].traceTimestamp;
            obj.traceLocLatitude = res[i].traceLocLatitude;
            obj.traceLocLongitude = res[i].traceLocLongitude;
            obj.traceIsOpen = res[i].traceIsOpen;
            obj.traceComment = res[i].traceComment;
            obj.isDeleted = res[i].isDeleted;
            obj.lastModifiedDate = res[i].lastModifiedDate;
            obj.traceTimestampStr = res[i].traceTimestampStr;
            obj.branchNumber = res[i].branchNumber;
            obj.markForUpload = (Math.floor(res[i].konysyncchangetype / 10) == 9) ? false : true;
            objMap[i] = obj;
        }
    }
    return objMap;
};
WTSync.wtemployeetrace.filterAttributes = function(valuestable, insert) {
    sync.log.trace("Entering WTSync.wtemployeetrace.filterAttributes function");
    var attributeTable = {};
    attributeTable.traceId = "traceId";
    attributeTable.employeeCode = "employeeCode";
    attributeTable.email = "email";
    attributeTable.traceTimestamp = "traceTimestamp";
    attributeTable.traceLocLatitude = "traceLocLatitude";
    attributeTable.traceLocLongitude = "traceLocLongitude";
    attributeTable.traceIsOpen = "traceIsOpen";
    attributeTable.traceComment = "traceComment";
    attributeTable.traceTimestampStr = "traceTimestampStr";
    attributeTable.branchNumber = "branchNumber";
    var PKTable = {};
    PKTable.traceId = {}
    PKTable.traceId.name = "traceId";
    PKTable.traceId.isAutoGen = true;
    var newvaluestable = {};
    for (var k in valuestable) {
        var v = valuestable[k];
        if (kony.sync.isNull(attributeTable[k])) {
            sync.log.warn("Ignoring the attribute " + k + " for the SyncObject wtemployeetrace. " + k + " not defined as an attribute in SyncConfiguration.");
        } else if (!kony.sync.isNull(PKTable[k])) {
            if (insert === false) {
                sync.log.warn("Ignoring the primary key " + k + " for the SyncObject wtemployeetrace. Primary Key should not be the part of the attributes to be updated in the local device database.");
            } else if (PKTable[k].isAutoGen) {
                sync.log.warn("Ignoring the auto-generated primary key " + k + " for the SyncObject wtemployeetrace. Auto-generated Primary Key should not be the part of the attributes to be inserted in the local device database.");
            } else {
                newvaluestable[k] = v;
            }
        } else {
            newvaluestable[k] = v;
        }
    }
    return newvaluestable;
};
WTSync.wtemployeetrace.formOrderByClause = function(orderByMap) {
    sync.log.trace("Entering WTSync.wtemployeetrace.formOrderByClause function");
    if (!kony.sync.isNull(orderByMap)) {
        var valuestable = kony.sync.convertOrderByMapToValuesTable(orderByMap);
        //var filteredValuestable = WTSync.wtemployeetrace.filterAttributes(valuestable, true);
        return kony.sync.convertToValuesTableOrderByMap(orderByMap, valuestable);
    }
    return null;
};
WTSync.wtemployeetrace.prototype.getValuesTable = function(isInsert) {
    sync.log.trace("Entering WTSync.wtemployeetrace.prototype.getValuesTable function");
    var valuesTable = {};
    if (isInsert === true) {
        valuesTable.traceId = this.traceId;
    }
    valuesTable.employeeCode = this.employeeCode;
    valuesTable.email = this.email;
    valuesTable.traceTimestamp = this.traceTimestamp;
    valuesTable.traceLocLatitude = this.traceLocLatitude;
    valuesTable.traceLocLongitude = this.traceLocLongitude;
    valuesTable.traceIsOpen = this.traceIsOpen;
    valuesTable.traceComment = this.traceComment;
    valuesTable.traceTimestampStr = this.traceTimestampStr;
    valuesTable.branchNumber = this.branchNumber;
    return valuesTable;
};
WTSync.wtemployeetrace.prototype.getPKTable = function() {
    sync.log.trace("Entering WTSync.wtemployeetrace.prototype.getPKTable function");
    var pkTable = {};
    pkTable.traceId = {
        key: "traceId",
        value: this.traceId
    };
    return pkTable;
};
WTSync.wtemployeetrace.getPKTable = function() {
    sync.log.trace("Entering WTSync.wtemployeetrace.getPKTable function");
    var pkTable = [];
    pkTable.push("traceId");
    return pkTable;
};
WTSync.wtemployeetrace.pkCheck = function(pks, wcs, errorcallback, opName) {
    sync.log.trace("Entering WTSync.wtemployeetrace.pkCheck function");
    var wc = [];
    if (kony.sync.isNull(pks)) {
        sync.log.error("Primary Key traceId not specified in  " + opName + "  an item in wtemployeetrace");
        kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified, kony.sync.getPrimaryKeyNotSpecifiedMsg("traceId", opName, "wtemployeetrace")));
        return false;
    } else if (kony.sync.isValidJSTable(pks)) {
        if (!kony.sync.isNull(pks.traceId)) {
            if (!kony.sync.isNull(pks.traceId.value)) {
                wc.key = "traceId";
                wc.value = pks.traceId.value;
            } else {
                wc.key = "traceId";
                wc.value = pks.traceId;
            }
        } else {
            sync.log.error("Primary Key traceId not specified in  " + opName + "  an item in wtemployeetrace");
            kony.sync.verifyAndCallClosure(errorcallback, kony.sync.getErrorTable(kony.sync.errorCodePrimaryKeyNotSpecified, kony.sync.getPrimaryKeyNotSpecifiedMsg("traceId", opName, "wtemployeetrace")));
            return false;
        }
    } else {
        wc.key = "traceId";
        wc.value = pks;
    }
    kony.table.insert(wcs, wc);
    return true;
};
WTSync.wtemployeetrace.validateNull = function(valuestable, errorcallback) {
    sync.log.trace("Entering WTSync.wtemployeetrace.validateNull function");
    return true;
};
WTSync.wtemployeetrace.validateNullInsert = function(valuestable, errorcallback) {
    sync.log.trace("Entering WTSync.wtemployeetrace.validateNullInsert function");
    return true;
};
WTSync.wtemployeetrace.getRelationshipMap = function(relationshipMap, valuestable) {
    sync.log.trace("Entering WTSync.wtemployeetrace.getRelationshipMap function");
    var r1 = {};
    return relationshipMap;
};
WTSync.wtemployeetrace.checkPKValueTables = function(valuetables) {
    var checkPksNotNullFlag = true;
    for (var i = 0; i < valuetables.length; i++) {
        if (kony.sync.isNull(valuetables[i])) {
            checkPksNotNullFlag = false;
            break;
        }
    }
    return checkPksNotNullFlag;
};
WTSync.wtemployeetrace.getTableName = function() {
    return "wtemployeetrace";
};
// **********************************End wtemployeetrace's helper methods************************