/**********************************************************************
 *	Author  : RI - CTS363601 
 *	Purpose : Module to refer all constant declarations
 ***********************************************************************/
function WTConstant() {}
WTConstant.APP_VERSION = "1.0";
WTConstant.SERVICE_HTTP_STATUS = 200;
WTConstant.SERVICE_OPSTATUS = 0;
WTConstant.SERVICE_MSG_SUCCESS = "SUCCESS";
WTConstant.LOCATION_UNAVAILABLE = "POSITION_UNAVAILABLE";
WTConstant.LOCATION_PERMSN_DENIED = "PERMISSION_DENIED";
WTConstant.LOCATION_TIMEOUT = "TIMEOUT";
WTConstant.DEF_COUNTRY_CODE = "UK";
WTConstant.DEF_BUSINESS_CODE = "D";
WTConstant.DEF_LANGUAGE_CODE = "ENG";
WTConstant.DEF_LOCALE = "en_GB";
WTConstant.FORM_HOME = "Home";
WTConstant.FORM_CONTENT = "Content";
WTConstant.FORM_LOGIN = "Login";
WTConstant.WT_RECORD_IN_STORE = "WT_RECORD_IN_STORE";
WTConstant.LAST_SYNC_DATE = "LAST_SYNC_DATE";
WTConstant.RADIO_BTN_ON = "radio_on.png";
WTConstant.RADIO_BTN_OFF = "radio_off.png";
WTConstant.SYNC_USER_ID = "";
WTConstant.SYNC_PASSWORD = "";
WTConstant.SYNC_APP_ID = "100004898743b5a1e";
WTConstant.SYNC_SERVER_HOST = "rentokil-initial.sync.konycloud.com";
WTConstant.SYNC_SERVER_PORT = "";
WTConstant.SYNC_IS_SECURE = true;
WTConstant.SYNC_DO_UPLOAD = true;
WTConstant.SYNC_DO_DOWNLOAD = false;
WTConstant.SYNC_UPLOAD_ERROR_POLICY = "continueonerror";
WTConstant.PEST = "Pest";
WTConstant.HYGIENE = "Hygiene";
WTConstant.SELECT = "Select";
var static_content = "<h1 style=\"color:#0e202f; font-family:helvetica; font-size:14px; font-weight:bold;\">About us</h1>";
static_content += "<p style=\"color:#0e202f; font-family:helvetica; font-size:12px; font-weight:bold;\">Rentokil Initial provides services that protect people and enhance lives. We protect people from the dangers of pest-borne disease, the risks of poor hygiene or from injury in the workplace. We enhance lives with services that protect the health and wellbeing of people, and the reputation of our customers' brands. We are one of the largest business services companies in the world providing Pest Control, Hygiene and Workwear services. We operate in over 60 countries. Embedded within our approach is the ability to analyse the progress and performance of the company in three ways - by Region and Category which form part of our Business Model and by Quadrant which is how we execute our Strategy including capital allocation.</p>";
static_content += "<p style=\"color:gray; font-family:helvetica; font-size:12px;\">We are one of the largest business services companies in the world providing Pest Control, Hygiene and Workwear services. We operate in over 60 countries.</p>";
static_content += "<p style=\"color:gray; font-family:helvetica; font-size:12px;\">Embedded within our approach is the ability to analyse the progress and performance of the company in three ways – by Region and Category which form part of our Business Model and by Quadrant which is how we execute our Strategy including capital allocation.</p>";