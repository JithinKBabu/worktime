function addWidgetsContent() {
    Content.setDefaultUnit(kony.flex.DP);
    var flxMainContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxMainContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flxBlueBgSkin",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMainContainer.setDefaultUnit(kony.flex.DP);
    var flxHeaderContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "55dp",
        "id": "flxHeaderContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flxDarkGrayBgSkin",
        "top": "0dp",
        "width": "100%"
    }, {}, {});
    flxHeaderContainer.setDefaultUnit(kony.flex.DP);
    var imgBurgerIcon = new kony.ui.Image2({
        "centerY": "50%",
        "height": "26dp",
        "id": "imgBurgerIcon",
        "isVisible": true,
        "left": "5%",
        "skin": "slImage",
        "src": "burger_icon.png",
        "width": "30dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnMenuOpen = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "btnTransparentSkin",
        "height": "42dp",
        "id": "btnMenuOpen",
        "isVisible": true,
        "left": "2%",
        "onClick": AS_Button_7cbd927421e2489a96d6f2dfbffb1397,
        "skin": "btnTransparentSkin",
        "text": "Button",
        "width": "51dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblTitle = new kony.ui.Label({
        "centerY": "50.00%",
        "id": "lblTitle",
        "isVisible": true,
        "left": "64dp",
        "skin": "lblWhiteTitleSkin",
        "text": "Working Time",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxHeaderContainer.add(imgBurgerIcon, btnMenuOpen, lblTitle);
    var flxContentContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxContentContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "right": "0dp",
        "skin": "slFbox",
        "top": "55dp",
        "width": "100%",
        "zIndex": 2
    }, {}, {});
    flxContentContainer.setDefaultUnit(kony.flex.DP);
    var browserContent = new kony.ui.Browser({
        "bottom": "3%",
        "detectTelNumber": true,
        "enableZoom": false,
        "htmlString": "<p><b>Rentokil Pest Control</b> is a trading division of Rentokil Initial UK Limited, a company registered in England and Wales with registration number 301044 and whose registered office is at Riverbank, Meadows Business Park, Blackwater, Camberley, Surrey GU17 9AB.</p>",
        "id": "browserContent",
        "isVisible": true,
        "left": "4%",
        "right": "4%",
        "top": "3%",
        "width": "92%"
    }, {}, {});
    flxContentContainer.add(browserContent);
    flxMainContainer.add(flxHeaderContainer, flxContentContainer);
    var flxSettingsContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "centerX": "-50%",
        "clipBounds": true,
        "id": "flxSettingsContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "onTouchEnd": AS_FlexContainer_84dd514a3fd7473fbaefae2710f6ed2d,
        "onTouchMove": AS_FlexContainer_87162cfa19c140819ed551ae417095cd,
        "onTouchStart": AS_FlexContainer_aa40600be4804a44ba413387ba1083ed,
        "right": 0,
        "skin": "flxSettingsBgSkin",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxSettingsContainer.setDefaultUnit(kony.flex.DP);
    var flxBurgerLayer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxBurgerLayer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flxWhiteBgSkin",
        "top": "0dp",
        "width": "80%",
        "zIndex": 1
    }, {}, {});
    flxBurgerLayer.setDefaultUnit(kony.flex.DP);
    var flxTopContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "30%",
        "id": "flxTopContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flxDarkGrayBgSkin",
        "top": "0dp",
        "width": "100%"
    }, {}, {});
    flxTopContainer.setDefaultUnit(kony.flex.DP);
    var lblVersion = new kony.ui.Label({
        "id": "lblVersion",
        "isVisible": true,
        "right": "4%",
        "skin": "lbllWhiteTinySkin",
        "text": "V 1.0",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "4%",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var imgLogo = new kony.ui.Image2({
        "height": "55dp",
        "id": "imgLogo",
        "isVisible": true,
        "left": "4%",
        "skin": "slImage",
        "src": "logo_rentokil.png",
        "top": "30%",
        "width": "122dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblUsername = new kony.ui.Label({
        "id": "lblUsername",
        "isVisible": true,
        "left": "5%",
        "skin": "lbllWhiteNormalSkin",
        "text": "SVeetil",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "72%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxTopContainer.add(lblVersion, imgLogo, lblUsername);
    var flxBottomContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxBottomContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "30%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBottomContainer.setDefaultUnit(kony.flex.DP);
    var flxContainerTwo = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "46dp",
        "id": "flxContainerTwo",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onClick": AS_FlexContainer_8ee945d5713b4ea2aa763cc0a5e4ccd4,
        "skin": "slFbox",
        "top": "3%",
        "width": "100%"
    }, {}, {});
    flxContainerTwo.setDefaultUnit(kony.flex.DP);
    var imgIcon2 = new kony.ui.Image2({
        "centerY": "50.00%",
        "height": "40dp",
        "id": "imgIcon2",
        "isVisible": true,
        "left": "4%",
        "skin": "slImage",
        "src": "about_icon.png",
        "width": "40dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblAbout = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblAbout",
        "isVisible": true,
        "left": "60dp",
        "skin": "lblBlackBoldSkin",
        "text": "About",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxContainerTwo.add(imgIcon2, lblAbout);
    var flxContainerThree = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "46dp",
        "id": "flxContainerThree",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onClick": AS_FlexContainer_3b25a33bb23d4a4a858706038c0b0ef6,
        "skin": "slFbox",
        "top": "16%",
        "width": "100%"
    }, {}, {});
    flxContainerThree.setDefaultUnit(kony.flex.DP);
    var imgIcon3 = new kony.ui.Image2({
        "centerY": "50.00%",
        "height": "40dp",
        "id": "imgIcon3",
        "isVisible": true,
        "left": "4%",
        "skin": "slImage",
        "src": "sync_icon.png",
        "width": "40dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblSync = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblSync",
        "isVisible": true,
        "left": "60dp",
        "skin": "lblBlackBoldSkin",
        "text": "Sync",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxContainerThree.add(imgIcon3, lblSync);
    var flxContainerFour = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "46dp",
        "id": "flxContainerFour",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onClick": AS_FlexContainer_eee9853718e142ed919e016f9bec0453,
        "skin": "slFbox",
        "top": "29%",
        "width": "100%"
    }, {}, {});
    flxContainerFour.setDefaultUnit(kony.flex.DP);
    var imgIcon4 = new kony.ui.Image2({
        "centerY": "50.00%",
        "height": "40dp",
        "id": "imgIcon4",
        "isVisible": true,
        "left": "4%",
        "skin": "slImage",
        "src": "logout_icon.png",
        "width": "40dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblLogout = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblLogout",
        "isVisible": true,
        "left": "60dp",
        "skin": "lblBlackBoldSkin",
        "text": "Logout",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxContainerFour.add(imgIcon4, lblLogout);
    var flxContainerVersion = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "50dp",
        "id": "flxContainerVersion",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxContainerVersion.setDefaultUnit(kony.flex.DP);
    var lblLastSync = new kony.ui.Label({
        "id": "lblLastSync",
        "isVisible": true,
        "left": "5%",
        "skin": "lblGrayNormalSkin",
        "text": "Last Sync on:",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblLastSyncDate = new kony.ui.Label({
        "id": "lblLastSyncDate",
        "isVisible": true,
        "left": "5%",
        "skin": "lblGrayNormalSkin",
        "text": "07/04/2016 0:30 AM",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "21dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxContainerVersion.add(lblLastSync, lblLastSyncDate);
    flxBottomContainer.add(flxContainerTwo, flxContainerThree, flxContainerFour, flxContainerVersion);
    flxBurgerLayer.add(flxTopContainer, flxBottomContainer);
    flxSettingsContainer.add(flxBurgerLayer);
    Content.add(flxMainContainer, flxSettingsContainer);
};

function ContentGlobals() {
    Content = new kony.ui.Form2({
        "addWidgets": addWidgetsContent,
        "allowHorizontalBounce": false,
        "enabledForIdleTimeout": false,
        "id": "Content",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "postShow": AS_Form_25267fa367ab4843bad85aae3bb7ac5b,
        "preShow": AS_Form_c51fbd6104a14ea59b9e875276278efd,
        "skin": "BlueBgFormSkin",
        "verticalScrollIndicator": false
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "retainScrollPosition": false,
        "titleBar": false,
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
    Content.info = {
        "kuid": "c8bc0df090704eb28cb189ca6d61b5f4"
    };
};