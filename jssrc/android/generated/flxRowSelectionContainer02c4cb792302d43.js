function initializeflxRowSelectionContainer02c4cb792302d43() {
    flxRowSelectionContainer02c4cb792302d43 = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "40dp",
        "id": "flxRowSelectionContainer02c4cb792302d43",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "flxWhiteBgSkin061c30f6f621844"
    }, {}, {});
    flxRowSelectionContainer02c4cb792302d43.setDefaultUnit(kony.flex.DP);
    var imgSelection = new kony.ui.Image2({
        "centerY": "50%",
        "height": "36dp",
        "id": "imgSelection",
        "isVisible": true,
        "left": "3%",
        "skin": "slImage0a76aef17681f44",
        "src": "imagedrag.png",
        "width": "36dp"
    }, {
        "containerWeight": 100,
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "margin": [0, 0, 0, 0],
        "marginInPixel": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER
    }, {});
    var lblItem = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblItem",
        "isVisible": true,
        "left": "48dp",
        "skin": "lblGrayNormalSkin0386ba7ac75d644",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "containerWeight": 100,
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "hExpand": true,
        "margin": [1, 1, 1, 1],
        "marginInPixel": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false,
        "vExpand": false,
        "widgetAlignment": constants.WIDGET_ALIGN_CENTER
    }, {
        "textCopyable": false
    });
    flxRowSelectionContainer02c4cb792302d43.add(imgSelection, lblItem);
}