function addWidgetsLogin() {
    Login.setDefaultUnit(kony.flex.DP);
    var imgLogo = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "100%",
        "id": "imgLogo",
        "isVisible": true,
        "right": 0,
        "skin": "slImage",
        "src": "splash.png",
        "width": "100%"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flxBottomContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxBottomContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "skin": "slFbox",
        "top": "80%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBottomContainer.setDefaultUnit(kony.flex.DP);
    var imgUserIcon = new kony.ui.Image2({
        "centerX": "50%",
        "height": "60dp",
        "id": "imgUserIcon",
        "isVisible": true,
        "skin": "slImage",
        "src": "user_icon.png",
        "top": "0dp",
        "width": "60dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnLogin = new kony.ui.Button({
        "centerX": "50%",
        "focusSkin": "btnTransparentSkin",
        "height": "60dp",
        "id": "btnLogin",
        "isVisible": true,
        "skin": "btnTransparentSkin",
        "text": "Button",
        "top": 0,
        "width": "60dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblUsername = new kony.ui.Label({
        "bottom": "45dp",
        "centerX": "50%",
        "id": "lblUsername",
        "isVisible": true,
        "skin": "lbllWhiteNormalSkin",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxBottomContainer.add(imgUserIcon, btnLogin, lblUsername);
    var flxPopupOverlayContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxPopupOverlayContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flxOverlaySkin",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxPopupOverlayContainer.setDefaultUnit(kony.flex.DP);
    var flxPopupContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_HEIGHT,
        "centerX": "50%",
        "centerY": "50%",
        "clipBounds": true,
        "id": "flxPopupContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "11%",
        "right": "11%",
        "skin": "flxPopupBgSkin"
    }, {}, {});
    flxPopupContainer.setDefaultUnit(kony.flex.DP);
    var flxPopupTitleContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxPopupTitleContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxPopupTitleContainer.setDefaultUnit(kony.flex.DP);
    var lblPopupTitle = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblPopupTitle",
        "isVisible": true,
        "left": "6%",
        "skin": "lblBlackBoldSkin",
        "text": "Choose User",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxPopupTitleContainer.add(lblPopupTitle);
    var flxPopupContentContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "120dp",
        "id": "flxPopupContentContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "50dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxPopupContentContainer.setDefaultUnit(kony.flex.DP);
    var segEmailList = new kony.ui.SegmentedUI2({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "centerY": "50%",
        "data": [{
            "imgSelection": "",
            "lblItemName": "Label"
        }],
        "groupCells": false,
        "height": "120dp",
        "id": "segEmailList",
        "isVisible": true,
        "needPageIndicator": true,
        "onRowClick": AS_Segment_8e476be8a3d74348981be40a6ad1b00c,
        "pageOffDotImage": "pageoffdot.png",
        "pageOnDotImage": "pageondot.png",
        "retainSelection": false,
        "rowTemplate": flxRowSelectionContainer,
        "scrollingEvents": {},
        "sectionHeaderSkin": "sliPhoneSegmentHeader",
        "selectionBehavior": constants.SEGUI_DEFAULT_BEHAVIOR,
        "separatorRequired": false,
        "showScrollbars": false,
        "viewType": constants.SEGUI_VIEW_TYPE_TABLEVIEW,
        "widgetDataMap": {
            "flxRowSelectionContainer": "flxRowSelectionContainer",
            "imgSelection": "imgSelection",
            "lblItemName": "lblItemName"
        },
        "width": "100%"
    }, {
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxPopupContentContainer.add(segEmailList);
    var flxPopupButtonContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "70dp",
        "id": "flxPopupButtonContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "170dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxPopupButtonContainer.setDefaultUnit(kony.flex.DP);
    var btnCancel = new kony.ui.Button({
        "centerX": "28%",
        "centerY": "50%",
        "focusSkin": "btnNormalSkin",
        "height": "36dp",
        "id": "btnCancel",
        "isVisible": true,
        "onClick": AS_Button_80b6d6450b124006b9f87e5b41517e62,
        "skin": "btnNormalSkin",
        "text": "Cancel",
        "width": "100dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnOk = new kony.ui.Button({
        "centerX": "72%",
        "centerY": "50%",
        "focusSkin": "btnNormalSkin",
        "height": "36dp",
        "id": "btnOk",
        "isVisible": true,
        "onClick": AS_Button_1098877f28b84d1e9d5a563dd6573082,
        "skin": "btnNormalSkin",
        "text": "Ok",
        "width": "100dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxPopupButtonContainer.add(btnCancel, btnOk);
    flxPopupContainer.add(flxPopupTitleContainer, flxPopupContentContainer, flxPopupButtonContainer);
    var flexPopupBranch = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "248dp",
        "id": "flexPopupBranch",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "11%",
        "skin": "flxPopupBgSkin",
        "top": "186dp",
        "width": "79%",
        "zIndex": 1
    }, {}, {});
    flexPopupBranch.setDefaultUnit(kony.flex.DP);
    var flexBranchTitleContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flexBranchTitleContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flexBranchTitleContainer.setDefaultUnit(kony.flex.DP);
    var lblBranchTitle = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblBranchTitle",
        "isVisible": true,
        "left": "6%",
        "skin": "lblBlackBoldSkin",
        "text": "Choose business and branch ",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flexBranchTitleContainer.add(lblBranchTitle);
    var flxBranchContentContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "120dp",
        "id": "flxBranchContentContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "50dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBranchContentContainer.setDefaultUnit(kony.flex.DP);
    var lblCountryDetail = new kony.ui.Label({
        "id": "lblCountryDetail",
        "isVisible": true,
        "left": "37dp",
        "skin": "lblGrayNormalSkin",
        "text": "Country :UK",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "6dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var listBusiness = new kony.ui.ListBox({
        "height": "30%",
        "id": "listBusiness",
        "isVisible": true,
        "left": "37dp",
        "onSelection": AS_ListBox_9d502f47db49495fa55e4a22db9b23f2,
        "skin": "skinDropDowns",
        "top": "36dp",
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "applySkinsToPopup": true,
        "placeholder": "Select Business",
        "popupIcon": "icon_checkbox_on.png",
        "viewType": constants.LISTBOX_VIEW_TYPE_LISTVIEW
    });
    var listbranchnumbers = new kony.ui.ListBox({
        "height": "30%",
        "id": "listbranchnumbers",
        "isVisible": false,
        "left": "38dp",
        "skin": "skinDropDowns",
        "top": "81dp",
        "width": "226dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "applySkinsToPopup": true,
        "placeholder": "Select Branch",
        "viewType": constants.LISTBOX_VIEW_TYPE_LISTVIEW
    });
    flxBranchContentContainer.add(lblCountryDetail, listBusiness, listbranchnumbers);
    var flxBranchButton = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "70dp",
        "id": "flxBranchButton",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "170dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBranchButton.setDefaultUnit(kony.flex.DP);
    var btnBranchCancel = new kony.ui.Button({
        "centerX": "28%",
        "centerY": "50%",
        "focusSkin": "btnNormalSkin",
        "height": "36dp",
        "id": "btnBranchCancel",
        "isVisible": true,
        "onClick": AS_Button_a7cbda6196c74559803c3e5b29e40572,
        "skin": "btnNormalSkin",
        "text": "Cancel",
        "width": "100dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnBranchOk = new kony.ui.Button({
        "centerX": "72%",
        "centerY": "50%",
        "focusSkin": "btnNormalSkin",
        "height": "36dp",
        "id": "btnBranchOk",
        "isVisible": true,
        "onClick": AS_Button_62fa314bfad947ebbfb4ae647629cf73,
        "skin": "btnNormalSkin",
        "text": "Ok",
        "width": "100dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxBranchButton.add(btnBranchCancel, btnBranchOk);
    flexPopupBranch.add(flexBranchTitleContainer, flxBranchContentContainer, flxBranchButton);
    var flxBottomMsgContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxBottomMsgContainer",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onClick": AS_FlexContainer_203b3d8e7e224630b983bef18e1ef209,
        "onTouchStart": AS_FlexContainer_9385470f688649f9886d2f7db9581b3d,
        "skin": "flxOverlaySkin",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBottomMsgContainer.setDefaultUnit(kony.flex.DP);
    var flxWarningMsgContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "12%",
        "id": "flxWarningMsgContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flxBlueBgSkin",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxWarningMsgContainer.setDefaultUnit(kony.flex.DP);
    var lblWarningMsg = new kony.ui.Label({
        "centerY": "32%",
        "height": "27dp",
        "id": "lblWarningMsg",
        "isVisible": true,
        "left": "3.20%",
        "maxNumberOfLines": 2,
        "skin": "lblWhiteBoldNormalSkin",
        "text": "Business and Branch are mandatory",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": "96.17%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var btnYes = new kony.ui.Button({
        "centerY": "62%",
        "focusSkin": "slButtonGlossRed06e422d20a06149",
        "height": "40dp",
        "id": "btnYes",
        "isVisible": true,
        "onClick": AS_Button_96e2680a4e5c41df818b0e1ceb810f2e,
        "right": "27%",
        "skin": "btnTransparentSkinYellow0e00a3ee7c2444c",
        "text": "Ok",
        "top": "13dp",
        "width": "40dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxWarningMsgContainer.add(lblWarningMsg, btnYes);
    flxBottomMsgContainer.add(flxWarningMsgContainer);
    flxPopupOverlayContainer.add(flxPopupContainer, flexPopupBranch, flxBottomMsgContainer);
    Login.add(imgLogo, flxBottomContainer, flxPopupOverlayContainer);
};

function LoginGlobals() {
    Login = new kony.ui.Form2({
        "addWidgets": addWidgetsLogin,
        "allowHorizontalBounce": false,
        "enabledForIdleTimeout": false,
        "id": "Login",
        "init": AS_Form_1f9ec10f630e4ee18048ea4f7f06f864,
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "postShow": AS_Form_e7d9850118d0451aacd477132d6229be,
        "preShow": AS_Form_8b2dd570ab984a1bb2b75b91a5eaddfa,
        "skin": "BgFormSkiLogin",
        "verticalScrollIndicator": false
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceBack": AS_Form_2471fa04c0ee4c05a6f776beba46bf03,
        "retainScrollPosition": false,
        "titleBar": false,
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
    Login.info = {
        "kuid": "1497982e9c954c9f9afbcc1b120345f3"
    };
};