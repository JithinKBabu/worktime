function addWidgetsHome() {
    Home.setDefaultUnit(kony.flex.DP);
    var flxMainContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxMainContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flxBlueBgSkin",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMainContainer.setDefaultUnit(kony.flex.DP);
    var flxHeaderContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "55dp",
        "id": "flxHeaderContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flxDarkGrayBgSkin",
        "top": "0dp",
        "width": "100%"
    }, {}, {});
    flxHeaderContainer.setDefaultUnit(kony.flex.DP);
    var imgBurgerIcon = new kony.ui.Image2({
        "centerY": "50%",
        "height": "26dp",
        "id": "imgBurgerIcon",
        "isVisible": true,
        "left": "5%",
        "skin": "slImage",
        "src": "burger_icon.png",
        "width": "30dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnMenuOpen = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "btnTransparentSkin",
        "height": "42dp",
        "id": "btnMenuOpen",
        "isVisible": true,
        "left": "2%",
        "onClick": AS_Button_7cbd927421e2489a96d6f2dfbffb1397,
        "skin": "btnTransparentSkin",
        "text": "Button",
        "width": "51dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblTitle = new kony.ui.Label({
        "centerY": "50.00%",
        "id": "lblTitle",
        "isVisible": true,
        "left": "64dp",
        "skin": "lblWhiteTitleSkin",
        "text": "Working Time",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxHeaderContainer.add(imgBurgerIcon, btnMenuOpen, lblTitle);
    var flxContentContiner = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxContentContiner",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "55dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxContentContiner.setDefaultUnit(kony.flex.DP);
    var flxTimerContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "50%",
        "centerY": "50%",
        "clipBounds": true,
        "height": "120dp",
        "id": "flxTimerContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "flxRedBgSkin",
        "width": "120dp",
        "zIndex": 1
    }, {}, {});
    flxTimerContainer.setDefaultUnit(kony.flex.DP);
    var lblTimer = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblTimer",
        "isVisible": true,
        "skin": "lblWhiteBoldNormalSkin",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxTimerContainer.add(lblTimer);
    var flxStartWorkContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50%",
        "id": "flxStartWorkContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "50%"
    }, {}, {});
    flxStartWorkContainer.setDefaultUnit(kony.flex.DP);
    var flxStartWorkInnerContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "53%",
        "centerY": "56%",
        "clipBounds": true,
        "height": "180dp",
        "id": "flxStartWorkInnerContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "onClick": AS_FlexContainer_7f7c0832f9684279a4dec7bac00ef0bf,
        "skin": "flxItemEnabledSkin",
        "width": "150dp"
    }, {}, {});
    flxStartWorkInnerContainer.setDefaultUnit(kony.flex.DP);
    var lblStartWork = new kony.ui.Label({
        "centerX": "50.08%",
        "id": "lblStartWork",
        "isVisible": true,
        "skin": "lblGreenBoldSkin",
        "text": "Start Work",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var imgStartWork = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "104dp",
        "id": "imgStartWork",
        "isVisible": true,
        "skin": "slImage",
        "src": "start_work.png",
        "width": "104dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblStartTimeValue = new kony.ui.Label({
        "bottom": "10dp",
        "centerX": "50%",
        "id": "lblStartTimeValue",
        "isVisible": true,
        "skin": "lblWhiteBoldNormalSkin",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxStartWorkInnerContainer.add(lblStartWork, imgStartWork, lblStartTimeValue);
    flxStartWorkContainer.add(flxStartWorkInnerContainer);
    var flxStopWorkContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50%",
        "id": "flxStopWorkContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "50%",
        "skin": "slFbox",
        "top": "0dp",
        "width": "50%"
    }, {}, {});
    flxStopWorkContainer.setDefaultUnit(kony.flex.DP);
    var flxStopWorkInnerContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "48%",
        "centerY": "56%",
        "clipBounds": true,
        "height": "180dp",
        "id": "flxStopWorkInnerContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "onClick": AS_FlexContainer_a10cb0d9a9ee408598c58c3f7ab32876,
        "skin": "flxItemDisabledSkin",
        "width": "150dp"
    }, {}, {});
    flxStopWorkInnerContainer.setDefaultUnit(kony.flex.DP);
    var lblStopWork = new kony.ui.Label({
        "centerX": "50.00%",
        "id": "lblStopWork",
        "isVisible": true,
        "skin": "lblRedBoldSkin",
        "text": "Stop Work",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var imgSopWork = new kony.ui.Image2({
        "bottom": "30dp",
        "centerX": "50%",
        "centerY": "50%",
        "height": "104dp",
        "id": "imgSopWork",
        "isVisible": true,
        "skin": "slImage",
        "src": "stop_work.png",
        "width": "104dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblStopTimeValue = new kony.ui.Label({
        "bottom": "10dp",
        "centerX": "50%",
        "id": "lblStopTimeValue",
        "isVisible": true,
        "skin": "lblWhiteBoldNormalSkin",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxStopWorkInnerContainer.add(lblStopWork, imgSopWork, lblStopTimeValue);
    flxStopWorkContainer.add(flxStopWorkInnerContainer);
    var flxStartOverTimeContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "50%",
        "id": "flxStartOverTimeContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "width": "50%"
    }, {}, {});
    flxStartOverTimeContainer.setDefaultUnit(kony.flex.DP);
    var flxStartOverTimeInnerContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "53%",
        "centerY": "44%",
        "clipBounds": true,
        "height": "180dp",
        "id": "flxStartOverTimeInnerContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "onClick": AS_FlexContainer_30d9a88915f7461788f7f907e3f36b37,
        "skin": "flxItemEnabledSkin",
        "width": "150dp"
    }, {}, {});
    flxStartOverTimeInnerContainer.setDefaultUnit(kony.flex.DP);
    var lblStartOvertime = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblStartOvertime",
        "isVisible": true,
        "skin": "lblGreenBoldSkin",
        "text": "Start Overtime",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var imgStartOvertime = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "104dp",
        "id": "imgStartOvertime",
        "isVisible": true,
        "skin": "slImage",
        "src": "start_overtime.png",
        "width": "104dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblStartOvertimeTimeValue = new kony.ui.Label({
        "bottom": "10dp",
        "centerX": "50%",
        "id": "lblStartOvertimeTimeValue",
        "isVisible": true,
        "skin": "lblWhiteBoldNormalSkin",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxStartOverTimeInnerContainer.add(lblStartOvertime, imgStartOvertime, lblStartOvertimeTimeValue);
    flxStartOverTimeContainer.add(flxStartOverTimeInnerContainer);
    var flxStopOverTimeContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "50%",
        "id": "flxStopOverTimeContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "50%",
        "skin": "slFbox",
        "width": "50%"
    }, {}, {});
    flxStopOverTimeContainer.setDefaultUnit(kony.flex.DP);
    var flxStopOverTimeInnerContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerX": "48%",
        "centerY": "44%",
        "clipBounds": true,
        "height": "180dp",
        "id": "flxStopOverTimeInnerContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "onClick": AS_FlexContainer_34b052540d764153825abd5c40691a8e,
        "skin": "flxItemDisabledSkin",
        "width": "150dp"
    }, {}, {});
    flxStopOverTimeInnerContainer.setDefaultUnit(kony.flex.DP);
    var lblStopOvertime = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblStopOvertime",
        "isVisible": true,
        "skin": "lblRedBoldSkin",
        "text": "Stop Overtime",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "10dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var imgStopOvertime = new kony.ui.Image2({
        "centerX": "50%",
        "centerY": "50%",
        "height": "104dp",
        "id": "imgStopOvertime",
        "isVisible": true,
        "skin": "slImage",
        "src": "stop_overtime.png",
        "width": "104dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblStopOverTimeValue = new kony.ui.Label({
        "bottom": "10dp",
        "centerX": "50%",
        "id": "lblStopOverTimeValue",
        "isVisible": true,
        "skin": "lblWhiteBoldNormalSkin",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxStopOverTimeInnerContainer.add(lblStopOvertime, imgStopOvertime, lblStopOverTimeValue);
    flxStopOverTimeContainer.add(flxStopOverTimeInnerContainer);
    flxContentContiner.add(flxTimerContainer, flxStartWorkContainer, flxStopWorkContainer, flxStartOverTimeContainer, flxStopOverTimeContainer);
    var flxPopupOverlay = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxPopupOverlay",
        "isVisible": false,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onClick": AS_NamedActions_46f494a1e0c8432ab42a37c87952451e,
        "skin": "flxOverlaySkin",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxPopupOverlay.setDefaultUnit(kony.flex.DP);
    var flxPopupContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "centerY": "50%",
        "clipBounds": true,
        "height": "204dp",
        "id": "flxPopupContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "11%",
        "right": "11%",
        "skin": "flxPopupBgSkin",
        "top": "156dp"
    }, {}, {});
    flxPopupContainer.setDefaultUnit(kony.flex.DP);
    var flxPopupTitleContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "40dp",
        "id": "flxPopupTitleContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxPopupTitleContainer.setDefaultUnit(kony.flex.DP);
    var lblPopupTitle = new kony.ui.Label({
        "centerX": "50%",
        "centerY": "50%",
        "id": "lblPopupTitle",
        "isVisible": true,
        "left": "4%",
        "skin": "lblBlackBoldBigSkin",
        "text": "Work summary for the Day",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxPopupTitleContainer.add(lblPopupTitle);
    var flxPopupContentContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "104dp",
        "id": "flxPopupContentContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "40dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxPopupContentContainer.setDefaultUnit(kony.flex.DP);
    var lblDate = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblDate",
        "isVisible": true,
        "skin": "lblGrayBoldSkin",
        "text": "Date: 15 April 2016",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "9dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblTotalWorkTime = new kony.ui.Label({
        "centerX": "50.00%",
        "id": "lblTotalWorkTime",
        "isVisible": true,
        "skin": "lblGreenBoldNormalSkin",
        "text": "Total work time: 8:9:10",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "32dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblTotalOvertime = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblTotalOvertime",
        "isVisible": true,
        "skin": "lblRedBoldNormalSkin",
        "text": "Total overtime: 1:9:0",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "55dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblProgress = new kony.ui.Label({
        "centerX": "50%",
        "id": "lblProgress",
        "isVisible": true,
        "maxNumberOfLines": 2,
        "skin": "lblRedProgressSkin",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "85dp",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxPopupContentContainer.add(lblDate, lblTotalWorkTime, lblTotalOvertime, lblProgress);
    var flxPopupButtonContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": 60,
        "id": "flxPopupButtonContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "144dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxPopupButtonContainer.setDefaultUnit(kony.flex.DP);
    var btnOk = new kony.ui.Button({
        "centerX": "50%",
        "centerY": "50%",
        "focusSkin": "btnNormalSkin",
        "height": "36dp",
        "id": "btnOk",
        "isVisible": true,
        "onClick": AS_Button_ce77351aa09149329ddc7757e0595702,
        "skin": "btnNormalDisabledSkin",
        "text": "Ok",
        "width": "100dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxPopupButtonContainer.add(btnOk);
    flxPopupContainer.add(flxPopupTitleContainer, flxPopupContentContainer, flxPopupButtonContainer);
    flxPopupOverlay.add(flxPopupContainer);
    flxMainContainer.add(flxHeaderContainer, flxContentContiner, flxPopupOverlay);
    var flxSettingsContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "centerX": "-50%",
        "clipBounds": true,
        "id": "flxSettingsContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "onTouchEnd": AS_FlexContainer_84dd514a3fd7473fbaefae2710f6ed2d,
        "onTouchMove": AS_FlexContainer_87162cfa19c140819ed551ae417095cd,
        "onTouchStart": AS_FlexContainer_aa40600be4804a44ba413387ba1083ed,
        "right": 0,
        "skin": "flxSettingsBgSkin",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxSettingsContainer.setDefaultUnit(kony.flex.DP);
    var flxBurgerLayer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxBurgerLayer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flxWhiteBgSkin",
        "top": "0dp",
        "width": "80%",
        "zIndex": 1
    }, {}, {});
    flxBurgerLayer.setDefaultUnit(kony.flex.DP);
    var flxTopContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "30%",
        "id": "flxTopContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flxDarkGrayBgSkin",
        "top": "0dp",
        "width": "100%"
    }, {}, {});
    flxTopContainer.setDefaultUnit(kony.flex.DP);
    var lblVersion = new kony.ui.Label({
        "id": "lblVersion",
        "isVisible": true,
        "right": "4%",
        "skin": "lbllWhiteTinySkin",
        "text": "V 1.0",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "4%",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var imgLogo = new kony.ui.Image2({
        "height": "55dp",
        "id": "imgLogo",
        "isVisible": true,
        "left": "4%",
        "skin": "slImage",
        "src": "logo_rentokil.png",
        "top": "30%",
        "width": "122dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblUsername = new kony.ui.Label({
        "id": "lblUsername",
        "isVisible": true,
        "left": "5%",
        "skin": "lbllWhiteNormalSkin",
        "text": "SVeetil",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "72%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxTopContainer.add(lblVersion, imgLogo, lblUsername);
    var flxBottomContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxBottomContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "30%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBottomContainer.setDefaultUnit(kony.flex.DP);
    var flxContainerTwo = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "46dp",
        "id": "flxContainerTwo",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onClick": AS_FlexContainer_8ee945d5713b4ea2aa763cc0a5e4ccd4,
        "skin": "slFbox",
        "top": "3%",
        "width": "100%"
    }, {}, {});
    flxContainerTwo.setDefaultUnit(kony.flex.DP);
    var imgIcon2 = new kony.ui.Image2({
        "centerY": "50.00%",
        "height": "40dp",
        "id": "imgIcon2",
        "isVisible": true,
        "left": "4%",
        "skin": "slImage",
        "src": "about_icon.png",
        "width": "40dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblAbout = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblAbout",
        "isVisible": true,
        "left": "60dp",
        "skin": "lblBlackBoldSkin",
        "text": "About",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxContainerTwo.add(imgIcon2, lblAbout);
    var flxContainerThree = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "46dp",
        "id": "flxContainerThree",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onClick": AS_FlexContainer_3b25a33bb23d4a4a858706038c0b0ef6,
        "skin": "slFbox",
        "top": "16%",
        "width": "100%"
    }, {}, {});
    flxContainerThree.setDefaultUnit(kony.flex.DP);
    var imgIcon3 = new kony.ui.Image2({
        "centerY": "50.00%",
        "height": "40dp",
        "id": "imgIcon3",
        "isVisible": true,
        "left": "4%",
        "skin": "slImage",
        "src": "sync_icon.png",
        "width": "40dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblSync = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblSync",
        "isVisible": true,
        "left": "60dp",
        "skin": "lblBlackBoldSkin",
        "text": "Sync",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxContainerThree.add(imgIcon3, lblSync);
    var flxContainerFour = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "46dp",
        "id": "flxContainerFour",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "onClick": AS_FlexContainer_eee9853718e142ed919e016f9bec0453,
        "skin": "slFbox",
        "top": "29%",
        "width": "100%"
    }, {}, {});
    flxContainerFour.setDefaultUnit(kony.flex.DP);
    var imgIcon4 = new kony.ui.Image2({
        "centerY": "50.00%",
        "height": "40dp",
        "id": "imgIcon4",
        "isVisible": true,
        "left": "4%",
        "skin": "slImage",
        "src": "logout_icon.png",
        "width": "40dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblLogout = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblLogout",
        "isVisible": true,
        "left": "60dp",
        "skin": "lblBlackBoldSkin",
        "text": "Logout",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxContainerFour.add(imgIcon4, lblLogout);
    var flxContainerVersion = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "50dp",
        "id": "flxContainerVersion",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxContainerVersion.setDefaultUnit(kony.flex.DP);
    var lblLastSync = new kony.ui.Label({
        "id": "lblLastSync",
        "isVisible": true,
        "left": "5%",
        "skin": "lblGrayNormalSkin",
        "text": "Last Sync on:",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblLastSyncDate = new kony.ui.Label({
        "id": "lblLastSyncDate",
        "isVisible": true,
        "left": "5%",
        "skin": "lblGrayNormalSkin",
        "text": "07/04/2016 0:30 AM",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "top": "21dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxContainerVersion.add(lblLastSync, lblLastSyncDate);
    flxBottomContainer.add(flxContainerTwo, flxContainerThree, flxContainerFour, flxContainerVersion);
    flxBurgerLayer.add(flxTopContainer, flxBottomContainer);
    flxSettingsContainer.add(flxBurgerLayer);
    Home.add(flxMainContainer, flxSettingsContainer);
};

function HomeGlobals() {
    Home = new kony.ui.Form2({
        "addWidgets": addWidgetsHome,
        "allowHorizontalBounce": false,
        "enabledForIdleTimeout": false,
        "id": "Home",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "postShow": AS_Form_7e9bb2d8123449118638f96221faacf5,
        "preShow": AS_Form_c321a2e940374a828ee2c71acc2d2957,
        "skin": "BlueBgFormSkin",
        "verticalScrollIndicator": false
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "footerOverlap": false,
        "headerOverlap": false,
        "menuPosition": constants.FORM_MENU_POSITION_AFTER_APPMENU,
        "onDeviceBack": AS_Form_f2d890fdf8574df4bec315488e4586cd,
        "retainScrollPosition": false,
        "titleBar": false,
        "windowSoftInputMode": constants.FORM_ADJUST_PAN
    });
    Home.info = {
        "kuid": "f2a1d30b0ab54c9fb9b5b2a5e66cc500"
    };
};