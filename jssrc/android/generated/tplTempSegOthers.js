function initializetplTempSegOthers() {
    flxSegOtherContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "50dp",
        "id": "flxSegOtherContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "flxWhiteBgSkin"
    }, {}, {});
    flxSegOtherContainer.setDefaultUnit(kony.flex.DP);
    var lblItem1 = new kony.ui.Label({
        "centerY": "30%",
        "id": "lblItem1",
        "isVisible": true,
        "left": "1%",
        "maxNumberOfLines": 2,
        "skin": "lblGreenTinySkin",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": "18%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblItem2 = new kony.ui.Label({
        "centerY": "70%",
        "id": "lblItem2",
        "isVisible": true,
        "left": "1%",
        "maxNumberOfLines": 2,
        "skin": "lblGrayTinySkin",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": "18%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblItem3Top = new kony.ui.Label({
        "centerY": "30%",
        "id": "lblItem3Top",
        "isVisible": true,
        "left": "19%",
        "maxNumberOfLines": 1,
        "skin": "lblGrayTinySkin",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": "30%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblItem3Bottom = new kony.ui.Label({
        "centerY": "70%",
        "id": "lblItem3Bottom",
        "isVisible": true,
        "left": "19%",
        "maxNumberOfLines": 1,
        "skin": "lblGrayTinySkin",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": "30%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblItem4 = new kony.ui.Label({
        "centerY": "30%",
        "id": "lblItem4",
        "isVisible": true,
        "left": "49%",
        "maxNumberOfLines": 2,
        "skin": "lblGrayTinySkin",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": "18%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblItem5 = new kony.ui.Label({
        "centerY": "70%",
        "id": "lblItem5",
        "isVisible": true,
        "left": "49%",
        "maxNumberOfLines": 2,
        "skin": "lblGrayTinySkin",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": "18%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblItem6Top = new kony.ui.Label({
        "centerY": "30%",
        "id": "lblItem6Top",
        "isVisible": true,
        "left": "68%",
        "maxNumberOfLines": 1,
        "skin": "lblGrayTinySkin",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": "30%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblItem6Bottom = new kony.ui.Label({
        "centerY": "70%",
        "id": "lblItem6Bottom",
        "isVisible": true,
        "left": "68%",
        "maxNumberOfLines": 1,
        "skin": "lblGrayTinySkin",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": "30%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxSegOtherContainer.add(lblItem1, lblItem2, lblItem3Top, lblItem3Bottom, lblItem4, lblItem5, lblItem6Top, lblItem6Bottom);
}