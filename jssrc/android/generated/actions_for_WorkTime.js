//actions.js file 
function ActionApplicationBecomesForeground() {
    return AS_NamedActions_2c6bd9f7f9214b4db0704777835b0195();
}

function AS_NamedActions_2c6bd9f7f9214b4db0704777835b0195() {
    return onAppBecomesForeground.call(this);
}

function ActionApplicationWentBackground() {
    return AS_NamedActions_851befcdc2b54516b9b499d5d90a45e1();
}

function AS_NamedActions_851befcdc2b54516b9b499d5d90a45e1() {
    return onAppWentBackground.call(this);
}

function ActionConfirmLogoutPopupYesBtn() {
    return AS__7c907c19cf9b4c1286668f5d318d8d50();
}

function AS__7c907c19cf9b4c1286668f5d318d8d50() {
    onClickConfirmPopupYesBtn.call(this);
    onClickLogoutConfirmPopupYesBtn.call(this);
}

function ActionConfirmPopupNoBtn() {
    return AS_NamedActions_ce4db49d475f49dfa9d1dc5a18c42026();
}

function AS_NamedActions_ce4db49d475f49dfa9d1dc5a18c42026() {
    return onClickConfirmPopupNoBtn.call(this);
}

function ActionConfirmPopupYesBtn() {
    return AS_NamedActions_180ef229a11f459e9a95e24594d48b0e();
}

function AS_NamedActions_180ef229a11f459e9a95e24594d48b0e() {
    return onClickConfirmPopupYesBtn.call(this);
}

function ActionConfirmSyncStopYesBtn() {
    return AS_NamedActions_32d036a1464b4625b2d68f2412668a2f();
}

function AS_NamedActions_32d036a1464b4625b2d68f2412668a2f() {
    return onClickSyncStopConfirmYesBtn.call(this);
}

function ActionContentPostShow(eventobject) {
    return AS_Form_25267fa367ab4843bad85aae3bb7ac5b(eventobject);
}

function AS_Form_25267fa367ab4843bad85aae3bb7ac5b(eventobject) {
    return onPostShowContentScreen.call(this);
}

function ActionContentPreSow(eventobject) {
    return AS_Form_c51fbd6104a14ea59b9e875276278efd(eventobject);
}

function AS_Form_c51fbd6104a14ea59b9e875276278efd(eventobject) {
    return onPreShowContentScreen.call(this);
}

function ActionDeviceBackFromHome(eventobject) {
    return AS_Form_f2d890fdf8574df4bec315488e4586cd(eventobject);
}

function AS_Form_f2d890fdf8574df4bec315488e4586cd(eventobject) {
    return onDeviceBackButtonCall.call(this);
}

function ActionDeviceBackFromLogin(eventobject) {
    return AS_Form_2471fa04c0ee4c05a6f776beba46bf03(eventobject);
}

function AS_Form_2471fa04c0ee4c05a6f776beba46bf03(eventobject) {
    return onDeviceBackButtonCall.call(this);
}

function ActionDismissPopup() {
    return AS_NamedActions_493adb08cbc3458b9a9952df1976c995();
}

function AS_NamedActions_493adb08cbc3458b9a9952df1976c995() {
    return dismissMessagePopup.call(this);
}

function ActionEmailPopupCancel(eventobject) {
    return AS_Button_80b6d6450b124006b9f87e5b41517e62(eventobject);
}

function AS_Button_80b6d6450b124006b9f87e5b41517e62(eventobject) {
    return onClickEmailsPopupCancelButton.call(this);
}

function ActionEmailPopupOk(eventobject) {
    return AS_Button_1098877f28b84d1e9d5a563dd6573082(eventobject);
}

function AS_Button_1098877f28b84d1e9d5a563dd6573082(eventobject) {
    return onClickEmailsPopupOkButton.call(this);
}

function ActionEmailSegRowClick(eventobject, sectionNumber, rowNumber) {
    return AS_Segment_8e476be8a3d74348981be40a6ad1b00c(eventobject, sectionNumber, rowNumber);
}

function AS_Segment_8e476be8a3d74348981be40a6ad1b00c(eventobject, sectionNumber, rowNumber) {
    return onClickSegEmailRow.call(this);
}

function ActionEndDay(eventobject) {
    return AS_FlexContainer_a10cb0d9a9ee408598c58c3f7ab32876(eventobject);
}

function AS_FlexContainer_a10cb0d9a9ee408598c58c3f7ab32876(eventobject) {
    return onClickEndDay.call(this);
}

function ActionHomePostShow(eventobject) {
    return AS_Form_7e9bb2d8123449118638f96221faacf5(eventobject);
}

function AS_Form_7e9bb2d8123449118638f96221faacf5(eventobject) {
    return onPostShowHomeScreen.call(this);
}

function ActionHomePreShow(eventobject) {
    return AS_Form_c321a2e940374a828ee2c71acc2d2957(eventobject);
}

function AS_Form_c321a2e940374a828ee2c71acc2d2957(eventobject) {
    return onPreShowHomeScreen.call(this);
}

function ActionLoginPostShow(eventobject) {
    return AS_Form_e7d9850118d0451aacd477132d6229be(eventobject);
}

function AS_Form_e7d9850118d0451aacd477132d6229be(eventobject) {
    return onPostShowLoginScreen.call(this);
}

function ActionLoginPreShow(eventobject) {
    return AS_Form_8b2dd570ab984a1bb2b75b91a5eaddfa(eventobject);
}

function AS_Form_8b2dd570ab984a1bb2b75b91a5eaddfa(eventobject) {
    return onPreShowLoginScreen.call(this);
}

function ActionLogoutConfirmPopupYesBtn() {
    return AS_NamedActions_ec8bf58dfe0c4ebda7d0ea2cc8b33194();
}

function AS_NamedActions_ec8bf58dfe0c4ebda7d0ea2cc8b33194() {
    return onClickLogoutConfirmPopupYesBtn.call(this);
}

function ActionMessagePopupDismiss() {
    return AS_NamedActions_ee5f69019a5945dfb66a5b8a84be7334();
}

function AS_NamedActions_ee5f69019a5945dfb66a5b8a84be7334() {
    return hideMessagePopup.call(this);
}

function ActionOkBranchSelection(eventobject) {
    return AS_Button_62fa314bfad947ebbfb4ae647629cf73(eventobject);
}

function AS_Button_62fa314bfad947ebbfb4ae647629cf73(eventobject) {
    return onClickBranchPopupOkButton.call(this);
}

function ActionOnClickAbout(eventobject) {
    return AS_FlexContainer_8ee945d5713b4ea2aa763cc0a5e4ccd4(eventobject);
}

function AS_FlexContainer_8ee945d5713b4ea2aa763cc0a5e4ccd4(eventobject) {
    return onClickAbout.call(this);
}

function ActionOnClickBurgerMenu(eventobject) {
    return AS_Button_7cbd927421e2489a96d6f2dfbffb1397(eventobject);
}

function AS_Button_7cbd927421e2489a96d6f2dfbffb1397(eventobject) {
    return onClickOpenMenuButton.call(this);
}

function ActionOnClickEmailCancel(eventobject) {
    return AS_Button_a7cbda6196c74559803c3e5b29e40572(eventobject);
}

function AS_Button_a7cbda6196c74559803c3e5b29e40572(eventobject) {
    return onClickEmailsPopupCancelButton.call(this);
}

function ActionOnClickEmailPopupCancel(eventobject) {
    return AS_Button_7f5d1ba38e514512a786ee6373e33ea7(eventobject);
}

function AS_Button_7f5d1ba38e514512a786ee6373e33ea7(eventobject) {}

function ActionOnClickLogout(eventobject) {
    return AS_FlexContainer_eee9853718e142ed919e016f9bec0453(eventobject);
}

function AS_FlexContainer_eee9853718e142ed919e016f9bec0453(eventobject) {
    return onClickLogout.call(this);
}

function ActionOnClickSettingsContainer(eventobject) {
    return AS_FlexContainer_e865e39c1cde4d28b9529c0e68d7578c(eventobject);
}

function AS_FlexContainer_e865e39c1cde4d28b9529c0e68d7578c(eventobject) {
    return closeSettingsMenu.call(this);
}

function ActionOnClickSync(eventobject) {
    return AS_FlexContainer_3b25a33bb23d4a4a858706038c0b0ef6(eventobject);
}

function AS_FlexContainer_3b25a33bb23d4a4a858706038c0b0ef6(eventobject) {
    return onClickSync.call(this);
}

function ActionOnMessagePopupOkBtn() {
    return AS_NamedActions_44fb94bbb73e4ce4ad782ec96fbec40a();
}

function AS_NamedActions_44fb94bbb73e4ce4ad782ec96fbec40a() {
    return onClickMessagePopupOkButton.call(this);
}

function ActionOnTouchEndSettingsContainer(eventobject, x, y) {
    return AS_FlexContainer_84dd514a3fd7473fbaefae2710f6ed2d(eventobject, x, y);
}

function AS_FlexContainer_84dd514a3fd7473fbaefae2710f6ed2d(eventobject, x, y) {
    return handleTouchEndOnSettings.call(this, eventobject, x, y);
}

function ActionOnTouchMoveSettingsContainer(eventobject, x, y) {
    return AS_FlexContainer_87162cfa19c140819ed551ae417095cd(eventobject, x, y);
}

function AS_FlexContainer_87162cfa19c140819ed551ae417095cd(eventobject, x, y) {
    return handleTouchMoveOnSettings.call(this, eventobject, x, y);
}

function ActionOnTouchStartSettingsContainer(eventobject, x, y) {
    return AS_FlexContainer_aa40600be4804a44ba413387ba1083ed(eventobject, x, y);
}

function AS_FlexContainer_aa40600be4804a44ba413387ba1083ed(eventobject, x, y) {
    return handleTouchStartOnSettings.call(this, eventobject, x, y);
}

function ActionPopupConfirmCancel(eventobject) {
    return AS_Button_4971bc3fbdb44fcc8d2d35b8d5198fcc(eventobject);
}

function AS_Button_4971bc3fbdb44fcc8d2d35b8d5198fcc(eventobject) {
    return onClickEmailsPopupCancelButton.call(this);
}

function ActionPopupConfirmOk(eventobject) {
    return AS_Button_118f150970c14159a9912c46974bf723(eventobject);
}

function AS_Button_118f150970c14159a9912c46974bf723(eventobject) {
    return onClickEmailsPopupOkButton.call(this);
}

function ActionPopupMessageOk(eventobject) {
    return AS_Button_6253f731806141048fc89f31f10ca6ac(eventobject);
}

function AS_Button_6253f731806141048fc89f31f10ca6ac(eventobject) {
    return onClickMessagePopupOkButton.call(this);
}

function ActionPostAppInit(eventobject) {
    return AS_AppEvents_24da5bfb113946e699199d551f892dbc(eventobject);
}

function AS_AppEvents_24da5bfb113946e699199d551f892dbc(eventobject) {
    return onPostAppInit.call(this);
}

function ActionPreAppInit(eventobject) {
    return AS_AppEvents_d7c985570eec418990647017250e5a1a(eventobject);
}

function AS_AppEvents_d7c985570eec418990647017250e5a1a(eventobject) {
    return onPreAppInit.call(this);
}

function ActionStartDay(eventobject) {
    return AS_FlexContainer_7f7c0832f9684279a4dec7bac00ef0bf(eventobject);
}

function AS_FlexContainer_7f7c0832f9684279a4dec7bac00ef0bf(eventobject) {
    return onClickStartDay.call(this);
}

function ActionStartOverTime(eventobject) {
    return AS_FlexContainer_30d9a88915f7461788f7f907e3f36b37(eventobject);
}

function AS_FlexContainer_30d9a88915f7461788f7f907e3f36b37(eventobject) {
    return onClickStartOvertime.call(this);
}

function ActionStopOverTime(eventobject) {
    return AS_FlexContainer_34b052540d764153825abd5c40691a8e(eventobject);
}

function AS_FlexContainer_34b052540d764153825abd5c40691a8e(eventobject) {
    return onClickEndOvertime.call(this);
}

function ActionVoidClick() {
    return AS_NamedActions_46f494a1e0c8432ab42a37c87952451e();
}

function AS_NamedActions_46f494a1e0c8432ab42a37c87952451e() {
    return voidClick.call(this);
}

function ActionWorkSummaryPopupOk(eventobject) {
    return AS_Button_ce77351aa09149329ddc7757e0595702(eventobject);
}

function AS_Button_ce77351aa09149329ddc7757e0595702(eventobject) {
    return onClickPopupOk.call(this);
}

function AS_Button_09679ff871e64d13b995911213874533(eventobject) {
    return onClickOpenMenuButton.call(this);
}

function AS_Button_2d3152a0e67b478a99bd61feb046c893(eventobject) {
    return onClickEmailsPopupOkButton.call(this);
}

function AS_Button_96e2680a4e5c41df818b0e1ceb810f2e(eventobject) {
    function SHOW_ALERT__520ccc051d92410fb1ca374852202759_True() {
        onClickLogout.call(this);
    }

    function SHOW_ALERT__520ccc051d92410fb1ca374852202759_False() {
        dismissMessagePopup.call(this);
    }

    function SHOW_ALERT__520ccc051d92410fb1ca374852202759_Callback(response) {
        if (response == true) {
            SHOW_ALERT__520ccc051d92410fb1ca374852202759_True()
        } else {
            SHOW_ALERT__520ccc051d92410fb1ca374852202759_False()
        };
    }
    kony.ui.Alert({
        "alertType": constants.ALERT_TYPE_CONFIRMATION,
        "alertTitle": null,
        "yesLabel": "Yes",
        "noLabel": "No",
        "alertIcon": "logout_icon.png",
        "message": "Are you sure want to log out the application?",
        "alertHandler": SHOW_ALERT__520ccc051d92410fb1ca374852202759_Callback
    }, {
        "iconPosition": constants.ALERT_ICON_POSITION_LEFT
    })
}

function AS_Button_a197ad7c08604606ac3333050595c55e(eventobject) {
    return onClickEmailsPopupCancelButton.call(this);
}

function AS_FlexContainer_203b3d8e7e224630b983bef18e1ef209(eventobject) {
    return onClickBranchOverlayLayer.call(this);
}

function AS_FlexContainer_9385470f688649f9886d2f7db9581b3d(eventobject, x, y) {
    return onClickBranchOverlayLayer.call(this);
}

function AS_FlexContainer_d34c059d324a4e93856e8bd3bd8e9494(eventobject) {
    return onClickSettings.call(this);
}

function AS_Form_1f9ec10f630e4ee18048ea4f7f06f864(eventobject) {}

function AS_ListBox_9d502f47db49495fa55e4a22db9b23f2(eventobject) {
    return updateBranchObj.call(this, null);
}