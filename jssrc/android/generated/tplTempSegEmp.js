function initializetplTempSegEmp() {
    flxSegEmpContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "30dp",
        "id": "flxSegEmpContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "flxWhiteBgSkin"
    }, {}, {});
    flxSegEmpContainer.setDefaultUnit(kony.flex.DP);
    var lblItem1 = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblItem1",
        "isVisible": true,
        "left": "2%",
        "maxNumberOfLines": 2,
        "skin": "lblGrayTinySkin",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": "50%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblItem2 = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblItem2",
        "isVisible": true,
        "left": "54%",
        "maxNumberOfLines": 2,
        "skin": "lblGrayTinySkin",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": "20%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblItem3 = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblItem3",
        "isVisible": true,
        "left": "75%",
        "maxNumberOfLines": 2,
        "skin": "lblGrayTinySkin",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": "8%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblItem4 = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblItem4",
        "isVisible": true,
        "left": "83%",
        "maxNumberOfLines": 2,
        "skin": "lblGrayTinySkin",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": "8%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    var lblItem5 = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblItem5",
        "isVisible": true,
        "left": "91%",
        "maxNumberOfLines": 2,
        "skin": "lblGrayTinySkin",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": "8%"
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxSegEmpContainer.add(lblItem1, lblItem2, lblItem3, lblItem4, lblItem5);
}