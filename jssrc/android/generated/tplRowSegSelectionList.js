function initializetplRowSegSelectionList() {
    flxRowSelectionContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "40dp",
        "id": "flxRowSelectionContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "skin": "flxWhiteBgSkin"
    }, {}, {});
    flxRowSelectionContainer.setDefaultUnit(kony.flex.DP);
    var imgSelection = new kony.ui.Image2({
        "centerY": "50%",
        "height": "36dp",
        "id": "imgSelection",
        "isVisible": true,
        "left": "3%",
        "skin": "slImage",
        "src": "imagedrag.png",
        "width": "36dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblItemName = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblItemName",
        "isVisible": true,
        "left": "16%",
        "maxNumberOfLines": 2,
        "skin": "lblGrayNormalSkin",
        "text": "Label",
        "textStyle": {
            "letterSpacing": 0,
            "strikeThrough": false
        },
        "textTruncatePosition": constants.TEXT_TRUNCATE_END,
        "width": "80%",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "textCopyable": false
    });
    flxRowSelectionContainer.add(imgSelection, lblItemName);
}