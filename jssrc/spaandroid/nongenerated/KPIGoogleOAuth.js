konyObject = null;
authClient = null;
/** Kony SDK initialization call **/
function initKonySDKObject() {
    konyObject = new kony.sdk();
    konyObject.init("3303352c6ab89669c6e47f4836de69cc", "4551e387ae48244432d455c9c45d1b1", "https://100004898.auth.konycloud.com/appconfig", initSDKSuccessCallback, initSDKFailureCallback);
}
/** Success callback - SDK initialization  **/
function initSDKSuccessCallback(response) {
    //alert("success");
    authClient = konyObject.getIdentityService("Google");
    authClient.login({}, identityServiceSuccessCallback, identityServiceFailureCallback);
}
/** Failure callback - SDK initialization  **/
function initSDKFailureCallback(response) {
    //alert("failure");
}
/** Success callback - Identity service  **/
function identityServiceSuccessCallback(response) {
    //alert("success");
}
/** Failure callback - Identity service  **/
function identityServiceFailureCallback(response) {
    //alert("failure");
}