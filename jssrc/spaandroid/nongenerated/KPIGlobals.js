/** Initializing global variables **/
function initializeGlobals() {
    gblKPIObjects = [];
    gblLoggedUser = null;
    getCurrentDevLocale();
}
/** Device back button action **/
function onDeviceBackButtonCall() {
    var frmObject = kony.application.getCurrentForm();
    if (frmObject.id == "Login" || frmObject.id == "Splash Screen") {
        /* Nothing to do */
    } else if (frmObject.id == "Home") {
        LoginScreen.show();
    } else if (frmObject.id == "Country") {
        HomeScreen.show();
    } else if (frmObject.id == "Branch") {
        CountryScreen.show();
    } else if (frmObject.id == "LanguagePopup") {
        LanguagePopup.destroy();
    }
}
/** To fetch current device locale  **/
function getCurrentDevLocale() {
    var curDevLocale = kony.i18n.getCurrentDeviceLocale();
    var language = curDevLocale.language;
    var country = curDevLocale.country;
    var devLocale = (!isEmpty(language) && !isEmpty(country)) ? (language + "_" + toUpperCase(country)) : "en_GB";
}
/** To change current device locale  **/
function changeDeviceLocale(selLocale) {
    if (kony.i18n.isLocaleSupportedByDevice(selLocale) && kony.i18n.isResourceBundlePresent(selLocale)) {
        kony.i18n.setCurrentLocaleAsync(selLocale, setLocaleSuccessCallback, setLocaleFailureCallback, {});
    }
}
/** Success callback - change device locale  **/
function setLocaleSuccessCallback() {
    //alert("success");
}
/** Failure callback - change device locale  **/
function setLocaleFailureCallback() {
    //alert("failure");
}