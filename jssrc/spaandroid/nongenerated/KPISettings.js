/** Settings Menu (Burger Menu)- UI refreshing like i18n updates, default visibility and all **/
function refreshUI() {
    var frmObj = kony.application.getCurrentForm();
    frmObj.flxMainContainer.left = 0;
    frmObj.flxMainContainer.top = 0;
    frmObj.lblUsername.text = "Welcome" + " " + "SVeetil";
    frmObj.lblChange.text = "Change Language";
    frmObj.lblAbout.text = "About";
    frmObj.lblLastSync.text = "Last sync on:";
    frmObj.lblLastSyncDate.text = "06/04/2016 10:30 AM";
    frmObj.lblVersion.text = "Version:" + " " + "1.0";
}
/** Settings Menu (Burger Menu)- Open menu button action **/
function onClickOpenMenuButton() {
    var frmObj = kony.application.getCurrentForm();
    frmObj.btnOpenBurgerMenu.setVisibility(false, {});
    frmObj.btnCloseBurgerMenu.setVisibility(true, {});

    function openMenu_Callback() {}
    frmObj.flxMainContainer.animate(
    kony.ui.createAnimation({
        "100": {
            "left": "80%",
            "stepConfig": {
                "timingFunction": kony.anim.LINEAR
            }
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.25
    }, {
        "animationEnd": openMenu_Callback
    });
}
/** Settings Menu (Burger Menu)- Close menu button action **/
function onClickCloseMenuButton() {
    var frmObj = kony.application.getCurrentForm();
    frmObj.btnCloseBurgerMenu.setVisibility(false, {});
    frmObj.btnOpenBurgerMenu.setVisibility(true, {});

    function closeMenu_Callback() {}
    frmObj.flxMainContainer.animate(
    kony.ui.createAnimation({
        "100": {
            "left": "0px",
            "stepConfig": {
                "timingFunction": kony.anim.LINEAR
            }
        }
    }), {
        "delay": 0,
        "iterationCount": 1,
        "fillMode": kony.anim.FILL_MODE_FORWARDS,
        "duration": 0.25
    }, {
        "animationEnd": closeMenu_Callback
    });
}
/** Settings Menu (Burger Menu)- Change Language button action **/
function onClickChangeLanguage() {}
/** Settings Menu (Burger Menu)- About button action **/
function onClickAbout() {
    var frmObj = kony.application.getCurrentForm();
    frmObj.FlexOverlayContainer.setVisibility(true);
    frmObj.listBoxLanguages.setVisibility(false);
    frmObj.browserAbout.setVisibility(true);
    frmObj.btnUpdate.text = "Close";
}
/** Settings Menu (Burger Menu)- Logout button action **/
function onClickLogout() {}