function addWidgetsLanguagePopup() {
    LanguagePopup.setDefaultUnit(kony.flex.DP);
    var flxPopupContainer = new kony.ui.FlexContainer({
        "centerY": "50%",
        "clipBounds": true,
        "height": "220dp",
        "id": "flxPopupContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "11%",
        "right": "11%",
        "skin": "flxPopupBgSkin",
        "top": "156dp"
    }, {}, {});
    flxPopupContainer.setDefaultUnit(kony.flex.DP);
    var lblPopupTitle = new kony.ui.Label({
        "id": "lblPopupTitle",
        "isVisible": true,
        "left": "8%",
        "skin": "NormalBlackBoldSkin",
        "text": "Language",
        "top": "8%",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    var rbtLanguages = new kony.ui.RadioButtonGroup({
        "height": "113dp",
        "id": "rbtLanguages",
        "isVisible": true,
        "left": "8%",
        "masterData": [
            ["rbg1", "Radiobutton One"],
            ["rbg2", "Radiobutton Two"],
            ["rbg3", "Radiobutton Three"]
        ],
        "right": "8%",
        "skin": "rbtNormalSkin",
        "top": "50dp",
        "zIndex": 1
    }, {
        "itemOrientation": constants.RADIOGROUP_ITEM_ORIENTATION_VERTICAL,
        "padding": [0, 2, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnOk = new kony.ui.Button({
        "bottom": "8%",
        "focusSkin": "slButtonGlossRed",
        "height": "36dp",
        "id": "btnOk",
        "isVisible": true,
        "right": "8%",
        "skin": "btnNormalSkin",
        "text": "Ok",
        "width": "100dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var CopybtnOk0ac830daedffe46 = new kony.ui.Button({
        "bottom": "8%",
        "focusSkin": "slButtonGlossRed",
        "height": "36dp",
        "id": "CopybtnOk0ac830daedffe46",
        "isVisible": true,
        "right": "48%",
        "skin": "btnNormalSkin",
        "text": "Cancel",
        "width": "100dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxPopupContainer.add(
    lblPopupTitle, rbtLanguages, btnOk, CopybtnOk0ac830daedffe46);
    LanguagePopup.add(
    flxPopupContainer);
};

function LanguagePopupGlobals() {
    LanguagePopup = new kony.ui.Form2({
        "addWidgets": addWidgetsLanguagePopup,
        "allowHorizontalBounce": false,
        "enabledForIdleTimeout": false,
        "id": "LanguagePopup",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "SemiTransparentBgSkin",
        "verticalScrollIndicator": false
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "retainScrollPosition": false
    });
};