function addWidgetsHome() {
    Home.setDefaultUnit(kony.flex.DP);
    var flxBurgerLayer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxBurgerLayer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "WhiteBgFlexSkin",
        "top": "0dp",
        "width": "100%"
    }, {}, {});
    flxBurgerLayer.setDefaultUnit(kony.flex.DP);
    var flxTopContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "30%",
        "id": "flxTopContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "DarkGrayBgFlexSkin",
        "top": "0dp",
        "width": "100%"
    }, {}, {});
    flxTopContainer.setDefaultUnit(kony.flex.DP);
    var imgLogo = new kony.ui.Image2({
        "height": "29dp",
        "id": "imgLogo",
        "isVisible": true,
        "left": "5%",
        "skin": "slImage",
        "src": "logo_rentokil.png",
        "top": "40%",
        "width": "138dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblUsername = new kony.ui.Label({
        "id": "lblUsername",
        "isVisible": true,
        "left": "5%",
        "skin": "NormalWhiteLabelSkin",
        "text": "SVeetil",
        "top": "72%",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    flxTopContainer.add(
    imgLogo, lblUsername);
    var flxBottomContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxBottomContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "30%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBottomContainer.setDefaultUnit(kony.flex.DP);
    var flxContainerOne = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "46dp",
        "id": "flxContainerOne",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "5%",
        "width": "100%"
    }, {}, {});
    flxContainerOne.setDefaultUnit(kony.flex.DP);
    var imgIcon1 = new kony.ui.Image2({
        "centerY": "50.00%",
        "height": "40dp",
        "id": "imgIcon1",
        "isVisible": true,
        "left": "5%",
        "skin": "slImage",
        "src": "settings_icon.png",
        "width": "40dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblChange = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblChange",
        "isVisible": true,
        "left": "70dp",
        "skin": "NormalBlackBoldSkin",
        "text": "Change Language",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    flxContainerOne.add(
    imgIcon1, lblChange);
    var flxContainerTwo = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "46dp",
        "id": "flxContainerTwo",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "top": "18%",
        "width": "100%"
    }, {}, {});
    flxContainerTwo.setDefaultUnit(kony.flex.DP);
    var imgIcon2 = new kony.ui.Image2({
        "centerY": "50.00%",
        "height": "40dp",
        "id": "imgIcon2",
        "isVisible": true,
        "left": "5%",
        "skin": "slImage",
        "src": "about_icon.png",
        "width": "40dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblAbout = new kony.ui.Label({
        "centerY": "50%",
        "id": "lblAbout",
        "isVisible": true,
        "left": "70dp",
        "skin": "NormalBlackBoldSkin",
        "text": "About",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    flxContainerTwo.add(
    imgIcon2, lblAbout);
    var flxContainerVersion = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "height": "80dp",
        "id": "flxContainerVersion",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "slFbox",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxContainerVersion.setDefaultUnit(kony.flex.DP);
    var lblLastSync = new kony.ui.Label({
        "id": "lblLastSync",
        "isVisible": true,
        "left": "5%",
        "skin": "NormalBlackLabelSkin",
        "text": "Last Sync on:",
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    var lblLastSyncDate = new kony.ui.Label({
        "id": "lblLastSyncDate",
        "isVisible": true,
        "left": "5%",
        "skin": "NormalBlackLabelSkin",
        "text": "07/04/2016 0:30 AM",
        "top": "21dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    var lblVersion = new kony.ui.Label({
        "bottom": "5dp",
        "id": "lblVersion",
        "isVisible": true,
        "left": "5%",
        "skin": "NormalBlackLabelSkin",
        "text": "Version: 1.0",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_MIDDLE_LEFT,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    flxContainerVersion.add(
    lblLastSync, lblLastSyncDate, lblVersion);
    flxBottomContainer.add(
    flxContainerOne, flxContainerTwo, flxContainerVersion);
    flxBurgerLayer.add(
    flxTopContainer, flxBottomContainer);
    var flxMainContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxMainContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flxMainContainerBGSkin",
        "top": "0dp",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxMainContainer.setDefaultUnit(kony.flex.DP);
    var flxHeaderContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "clipBounds": true,
        "height": "55dp",
        "id": "flxHeaderContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": "0dp",
        "skin": "flxHeaderBgSkin",
        "top": "0dp",
        "width": "100%"
    }, {}, {});
    flxHeaderContainer.setDefaultUnit(kony.flex.DP);
    var imgBurgerIcon = new kony.ui.Image2({
        "centerY": "50%",
        "height": "26dp",
        "id": "imgBurgerIcon",
        "isVisible": true,
        "left": "5%",
        "skin": "slImage",
        "src": "burger_icon.png",
        "width": "43dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnMenuClose = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "slButtonGlossRed",
        "height": "42dp",
        "id": "btnMenuClose",
        "isVisible": true,
        "left": "4%",
        "skin": "btnTransparentSkin",
        "text": "Button",
        "width": "51dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var btnMenuOpen = new kony.ui.Button({
        "centerY": "50%",
        "focusSkin": "slButtonGlossRed",
        "height": "42dp",
        "id": "btnMenuOpen",
        "isVisible": true,
        "left": "4%",
        "skin": "btnTransparentSkin",
        "text": "Button",
        "width": "51dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": false,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var imgBusinessLogo = new kony.ui.Image2({
        "centerY": "50%",
        "height": "29dp",
        "id": "imgBusinessLogo",
        "isVisible": true,
        "left": "21%",
        "skin": "slImage",
        "src": "logo_rentokil.png",
        "width": "138dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxHeaderContainer.add(
    imgBurgerIcon, btnMenuClose, btnMenuOpen, imgBusinessLogo);
    flxMainContainer.add(
    flxHeaderContainer);
    Home.add(
    flxBurgerLayer, flxMainContainer);
};

function HomeGlobals() {
    Home = new kony.ui.Form2({
        "addWidgets": addWidgetsHome,
        "allowHorizontalBounce": false,
        "enabledForIdleTimeout": false,
        "id": "Home",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "GrayBgFormSkin",
        "verticalScrollIndicator": false
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "retainScrollPosition": false
    });
};