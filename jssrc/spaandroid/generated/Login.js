function addWidgetsLogin() {
    Login.setDefaultUnit(kony.flex.DP);
    var imgLogo = new kony.ui.Image2({
        "id": "imgLogo",
        "isVisible": true,
        "left": "0dp",
        "skin": "slImage",
        "src": "logo.png",
        "top": "0dp",
        "width": kony.flex.USE_PREFFERED_SIZE
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var flxBottomContainer = new kony.ui.FlexContainer({
        "autogrowMode": kony.flex.AUTOGROW_NONE,
        "bottom": "0dp",
        "clipBounds": true,
        "id": "flxBottomContainer",
        "isVisible": true,
        "layoutType": kony.flex.FREE_FORM,
        "left": 0,
        "skin": "slFbox",
        "top": "80%",
        "width": "100%",
        "zIndex": 1
    }, {}, {});
    flxBottomContainer.setDefaultUnit(kony.flex.DP);
    var imgUserIcon = new kony.ui.Image2({
        "centerX": "50%",
        "height": "60dp",
        "id": "imgUserIcon",
        "isVisible": true,
        "skin": "slImage",
        "src": "user_icon.png",
        "top": "0dp",
        "width": "60dp"
    }, {
        "imageScaleMode": constants.IMAGE_SCALE_MODE_MAINTAIN_ASPECT_RATIO,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    var lblUsername = new kony.ui.Label({
        "bottom": "28dp",
        "centerX": "50%",
        "id": "lblUsername",
        "isVisible": true,
        "skin": "NormalWhiteLabelSkin",
        "text": "Welcome SVeetil",
        "width": kony.flex.USE_PREFFERED_SIZE,
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "renderAsAnchor": false,
        "textCopyable": false
    });
    var Button02c90bb624ff24a = new kony.ui.Button({
        "focusSkin": "slButtonGlossRed",
        "height": "50dp",
        "id": "Button02c90bb624ff24a",
        "isVisible": true,
        "left": "138dp",
        "onClick": AS_Button_af710ab81e8d407ea0770cc8ab8295b7,
        "skin": "slButtonGlossBlue",
        "text": "Button",
        "top": "14dp",
        "width": "103dp",
        "zIndex": 1
    }, {
        "contentAlignment": constants.CONTENT_ALIGN_CENTER,
        "displayText": true,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {});
    flxBottomContainer.add(
    imgUserIcon, lblUsername, Button02c90bb624ff24a);
    Login.add(
    imgLogo, flxBottomContainer);
};

function LoginGlobals() {
    Login = new kony.ui.Form2({
        "addWidgets": addWidgetsLogin,
        "allowHorizontalBounce": false,
        "enabledForIdleTimeout": false,
        "id": "Login",
        "layoutType": kony.flex.FREE_FORM,
        "needAppMenu": false,
        "skin": "GrayBgFormSkin",
        "verticalScrollIndicator": false
    }, {
        "displayOrientation": constants.FORM_DISPLAY_ORIENTATION_PORTRAIT,
        "layoutType": kony.flex.FREE_FORM,
        "padding": [0, 0, 0, 0],
        "paddingInPixel": false
    }, {
        "retainScrollPosition": false
    });
};